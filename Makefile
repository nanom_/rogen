CC		= /opt/gbdk/bin/lcc
OBJS	= src/main.o \
		  src/game.o \
		  src/player.o \
		  src/sprites.o \
		  src/hud.o \
		  src/tiles.o \
		  src/level.o \
		  src/coin.o \
		  src/math.o \
		  src/title.o \
		  src/logo.o \
		  src/bullet.o \
		  src/drone.o \
		  src/beam.o \
		  src/warning.o \
		  src/splitter.o \
		  src/skulltron.o \
		  src/turret.o \
		  src/heatball.o \
		  src/spike.o \
		  src/item.o
BIN	= build/rogen.gb

all: rom
	cd src

rom: $(OBJS)
	cd ..
	[ -d build ] || mkdir build
	$(CC) -o $(BIN) $(OBJS)

main.o: main.c main.h game.h title.h warning.h
	$(CC) -c main.c

game.o: game.c main.h sprites.h tiles.h math.h coin.h bullet.h drone.h splitter.h skulltron.h turret.h heatball.h spike.h item.h level.h game.h player.h hud.h beam.h
	$(CC) -c game.c

player.o: player.c player.h main.h game.h coin.h bullet.h item.h drone.h splitter.h skulltron.h turret.h heatball.h spike.h hud.h 
	$(CC) -c player.c

drone.o: drone.c drone.h main.h math.h game.h coin.h bullet.h
	$(CC) -c drone.c

splitter.o: splitter.c splitter.h math.h game.h coin.h bullet.h
	$(CC) -c splitter.c 

skulltron.o: skulltron.c skulltron.h main.h game.h coin.h bullet.h
	$(CC) -c skulltron.c

turret.o: turret.c turret.h main.h game.h coin.h bullet.h
	$(CC) -c turret.c

heatball.o: heatball.c heatball.h main.h game.h coin.h
	$(CC) -c heatball.c

spike.o: spike.c spike.h main.h game.h coin.h bullet.h
	$(CC) -c spike.c

coin.o: coin.c coin.h main.h math.h 
	$(CC) -c coin.c

sprites.o: sprites.c sprites.h
	$(CC) -c sprites.c

tiles.o: tiles.c tiles.h
	$(CC) -c tiles.c

level.o: level.c level.h
	$(CC) -c level.c

logo.o: logo.c logo.h
	$(CC) -c logo.c

hud.o: hud.c hud.h main.h
	$(CC) -c hud.c

math.o: math.c math.h main.h
	$(CC) -c math.c

title.o: title.c title.h main.h game.h tiles.h logo.h
	$(CC) -c title.c

bullet.o: bullet.c bullet.h main.h
	$(CC) -c bullet.c

beam.o: beam.c beam.h player.h drone.h splitter.h skulltron.h turret.h heatball.h spike.h game.h math.h
	$(CC) -c beam.c

item.o: item.c item.h game.h main.h math.h
	$(CC) -c item.c

warning.o: warning.c warning.h main.h
	$(CC) -c warning.c

clean:
	rm -f src/*.o
