/*

 LOGO.H

 Map Include File.

 Info:
   Section       : 
   Bank          : 0
   Map size      : 20 x 17
   Tile set      : Z:\home\nokoru\rogen\assets\tiles.gbr
   Plane count   : 1 plane (8 bits)
   Plane order   : Tiles are continues
   Tile offset   : 0
   Split data    : No

 This file was generated by GBMB v1.8

*/

#define logoWidth 20
#define logoHeight 17
#define logoBank 0

extern unsigned char logo[];

/* End of LOGO.H */
