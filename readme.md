Rogen
===
Rogen is a bullet hell homebrew game for the Game Boy based on an Atari
homebrew game I played a few years ago. It's written in C and requires the GBDK
to be compiled. I don't maintain the game anymore and I don't know whether it
compiles correctly or not. Anyway, you're free to modify it since it's released
in the public domain. And there's a compiled ROM in the `build/` directory if
you want to play the latest build of the game.

__The game contains flashing images, so please be careful! :3__
![screenshot here](screenshot.png)

Gameplay
---
Avoid the bullets, kill enemies and collect the coins to progress in the game.

Controls
---
You move using the joypad and you can shoot by pressing A.

How to compile
---
Install git, GNU Make and the GBDK, then clone the repo and run "make" to
compile. As I've said above, I'm not sure it will compile since I made some
modifications to the source code that may cause errors when compiling.
Feel free to fork the game and correct those errors if you want.

If you use Windows you need to install an Unix environment to compile the game.

