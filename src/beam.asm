;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module beam
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _beam_spawn
	.globl _beam_update
	.globl _beam_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/beam.c:15: void beam_spawn(beam_t *beam) {
;	---------------------------------
; Function beam_spawn
; ---------------------------------
_beam_spawn::
;src/beam.c:16: beam_draw(beam);
	pop	bc
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_beam_draw
	add	sp, #2
;src/beam.c:17: }
	ret
_fps:
	.db #0x3c	; 60
;src/beam.c:19: void beam_update(beam_t *beam, player_t *player, drone_t *drone, splitter_t *splitter,
;	---------------------------------
; Function beam_update
; ---------------------------------
_beam_update::
	add	sp, #-14
;src/beam.c:23: if (player->is_dead) return;
	ldhl	sp,	#18
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	hl, #0x0015
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#13
	ld	(hl), a
	or	a, a
	jp	NZ,00127$
;src/beam.c:24: if (beam->pos.y > SCREENHEIGHT + 16)
	ldhl	sp,	#16
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#4
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#11
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	inc	hl
	ld	(hl), a
	ld	a, #0xa0
	sub	a, (hl)
	jr	NC, 00104$
;src/beam.c:25: beam->pos.y = 24;
	dec	hl
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x18
	jr	00105$
00104$:
;src/beam.c:27: beam->pos.y += beam->spd;
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0003
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#9
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#13
	add	a, (hl)
	ld	c, a
	dec	hl
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
00105$:
;src/beam.c:29: if (player->pos.x >= beam->pos.x - 2 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#13
	ld	(hl), a
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	inc	hl
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	dec	bc
	dec	bc
	ldhl	sp,	#13
	ld	a, (hl)
	ldhl	sp,	#8
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00199$
	bit	7, d
	jr	NZ, 00200$
	cp	a, a
	jr	00200$
00199$:
	bit	7, d
	jr	Z, 00200$
	scf
00200$:
	jp	C, 00123$
;src/beam.c:30: player->pos.y >= beam->pos.y - 4 &&
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#13
	ld	(hl), a
;src/beam.c:24: if (beam->pos.y > SCREENHEIGHT + 16)
	ldhl	sp,#11
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
;src/beam.c:30: player->pos.y >= beam->pos.y - 4 &&
	dec	hl
	dec	hl
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#13
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00201$
	bit	7, d
	jr	NZ, 00202$
	cp	a, a
	jr	00202$
00201$:
	bit	7, d
	jr	Z, 00202$
	scf
00202$:
	jp	C, 00123$
;src/beam.c:31: player->pos.x <= beam->pos.x + 6 &&
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0006
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#8
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00203$
	bit	7, d
	jr	NZ, 00204$
	cp	a, a
	jr	00204$
00203$:
	bit	7, d
	jr	Z, 00204$
	scf
00204$:
	jp	C, 00123$
;src/beam.c:32: player->pos.y <= beam->pos.y + 6
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0006
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#12
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00205$
	bit	7, d
	jr	NZ, 00206$
	cp	a, a
	jr	00206$
00205$:
	bit	7, d
	jr	Z, 00206$
	scf
00206$:
	jp	C, 00123$
;src/beam.c:34: player->lives--;
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	dec	a
	ld	(bc), a
;src/beam.c:35: if (!drone->entity.is_dead) drone->can_move = false;
	ldhl	sp,#20
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00107$
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x00
00107$:
;src/beam.c:36: if (!splitter->entity.is_dead) splitter->can_move = false;
	ldhl	sp,#22
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00109$
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x00
00109$:
;src/beam.c:37: if (!skulltron->entity.is_dead) skulltron->can_move = false;
	ldhl	sp,#24
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00111$
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x00
00111$:
;src/beam.c:38: if (!skullperior->entity.is_dead) skullperior->can_move = false;
	ldhl	sp,#26
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00113$
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x00
00113$:
;src/beam.c:39: if (!turret->entity.is_dead) {
	ldhl	sp,	#28
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#12
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jr	NZ, 00115$
;src/beam.c:40: turret->entity.can_shoot = false;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000d
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
;src/beam.c:41: turret->can_move = false;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
00115$:
;src/beam.c:43: if (!turret2->entity.is_dead) {
	ldhl	sp,	#30
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#12
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jr	NZ, 00117$
;src/beam.c:44: turret2->entity.can_shoot = false;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000d
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
;src/beam.c:45: turret2->can_move = false;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
00117$:
;src/beam.c:47: if (!heatball->entity.is_dead) heatball->can_move = false;
	ldhl	sp,#32
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00119$
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x00
00119$:
;src/beam.c:48: if (!spike->entity.is_dead) spike->can_move = false;
	ldhl	sp,#34
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00121$
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x00
00121$:
;src/beam.c:49: player->is_dead = true;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
00123$:
;src/beam.c:52: beam_draw(beam);
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_beam_draw
	add	sp, #2
00127$:
;src/beam.c:53: }
	add	sp, #14
	ret
;src/beam.c:55: void beam_draw(beam_t *beam) {
;	---------------------------------
; Function beam_draw
; ---------------------------------
_beam_draw::
	add	sp, #-4
;src/beam.c:56: set_sprite_tile(beam->sprite, S_BEAM);
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	bc
	push	bc
	inc	bc
	inc	bc
	ld	a, (bc)
	ld	e, a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	ld	de, #_shadow_OAM
	add	hl, de
	inc	hl
	inc	hl
	ld	(hl), #0x18
;src/beam.c:57: move_sprite(beam->sprite, beam->pos.x, beam->pos.y);
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#2
	ld	(hl), a
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#3
	ld	(hl), a
	ld	a, (bc)
	ld	e, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	bc, #_shadow_OAM+0
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, bc
	ld	c, l
	ld	b, h
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	(bc), a
	inc	bc
	ld	a, (hl)
	ld	(bc), a
;src/beam.c:57: move_sprite(beam->sprite, beam->pos.x, beam->pos.y);
;src/beam.c:58: }
	add	sp, #4
	ret
	.area _CODE
	.area _CABS (ABS)
