#include <gb/gb.h>
#include <rand.h>
#include "main.h"
#include "game.h"
#include "player.h"
#include "drone.h"
#include "splitter.h"
#include "skulltron.h"
#include "turret.h"
#include "heatball.h"
#include "spike.h"
#include "math.h"
#include "beam.h"

void beam_spawn(beam_t *beam) {
    beam_draw(beam);
}

void beam_update(beam_t *beam, player_t *player, drone_t *drone, splitter_t *splitter,
    skulltron_t *skulltron, skulltron_t *skullperior, turret_t *turret, turret_t *turret2,
    heatball_t *heatball, spike_t *spike
) {
    if (player->is_dead) return;
    if (beam->pos.y > SCREENHEIGHT + 16)
        beam->pos.y = 24;
    else
        beam->pos.y += beam->spd;

    if (player->pos.x >= beam->pos.x - 2 &&
        player->pos.y >= beam->pos.y - 4 &&
        player->pos.x <= beam->pos.x + 6 &&
        player->pos.y <= beam->pos.y + 6
    ) {
        player->lives--;
        if (!drone->entity.is_dead) drone->can_move = false;
        if (!splitter->entity.is_dead) splitter->can_move = false;
        if (!skulltron->entity.is_dead) skulltron->can_move = false;
        if (!skullperior->entity.is_dead) skullperior->can_move = false;
        if (!turret->entity.is_dead) {
            turret->entity.can_shoot = false;
            turret->can_move = false;
        }
        if (!turret2->entity.is_dead) {
            turret2->entity.can_shoot = false;
            turret2->can_move = false;
        }
        if (!heatball->entity.is_dead) heatball->can_move = false;
        if (!spike->entity.is_dead) spike->can_move = false;
        player->is_dead = true;
    }

    beam_draw(beam);
}

void beam_draw(beam_t *beam) {
    set_sprite_tile(beam->sprite, S_BEAM);
    move_sprite(beam->sprite, beam->pos.x, beam->pos.y);
}

