#include <gb/gb.h>
#include "main.h"
#include "player.h"
#include "drone.h"
#include "splitter.h"
#include "skulltron.h"
#include "turret.h"
#include "heatball.h"
#include "spike.h"
#ifndef _BEAM_H_
#define _BEAM_H_

typedef struct beam_t {
    POINT pos;
    UINT8 sprite, spd;
} beam_t;

void beam_spawn(beam_t *beam);
void beam_update(beam_t *beam, player_t *player, drone_t *drone, splitter_t *splitter,
    skulltron_t *skulltron, skulltron_t *skullperior, turret_t *turret, turret_t *turret2,
    heatball_t *heatball, spike_t *spike);
void beam_draw(beam_t *beam);

#endif
