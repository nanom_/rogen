;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module bullet
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _bullet_spawn
	.globl _bullet_update
	.globl _bullet_draw
	.globl _bullet_destroy
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/bullet.c:6: void bullet_spawn(bullet_t *bullet) {
;	---------------------------------
; Function bullet_spawn
; ---------------------------------
_bullet_spawn::
;src/bullet.c:7: bullet_draw(bullet);
	pop	bc
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_bullet_draw
	add	sp, #2
;src/bullet.c:8: }
	ret
;src/bullet.c:10: void bullet_update(bullet_t *bullet) {
;	---------------------------------
; Function bullet_update
; ---------------------------------
_bullet_update::
	add	sp, #-3
;src/bullet.c:11: if (bullet->disabled) return;
	ldhl	sp,#5
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0005
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00106$
;src/bullet.c:12: if (bullet->pos.x < 16 || bullet->pos.x > SCREENWIDTH - 8) {
	ld	a, (bc)
	ld	e, a
	sub	a, #0x10
	jr	C, 00103$
	ld	a, #0x98
	sub	a, e
	jr	NC, 00104$
00103$:
;src/bullet.c:13: bullet_destroy(bullet);
	push	bc
	call	_bullet_destroy
	add	sp, #2
;src/bullet.c:14: return;
	jr	00106$
00104$:
;src/bullet.c:16: bullet->pos.x += (bullet->direction == fa_left ? -bullet->spd : bullet->spd);
	inc	sp
	inc	sp
	push	bc
	ld	hl, #0x0004
	add	hl, bc
	ld	a, (hl)
	ldhl	sp,	#2
	ld	(hl), a
	ld	l, c
	ld	h, b
	inc	hl
	inc	hl
	inc	hl
	ld	d, (hl)
	ldhl	sp,	#2
	ld	a, (hl)
	or	a, a
	jr	NZ, 00108$
	xor	a, a
	sub	a, d
	ld	(hl), a
	jr	00109$
00108$:
	ldhl	sp,	#2
	ld	(hl), d
00109$:
	ldhl	sp,	#2
	ld	a, (hl)
	add	a, e
	pop	hl
	push	hl
	ld	(hl), a
;src/bullet.c:17: bullet_draw(bullet);
	push	bc
	call	_bullet_draw
	add	sp, #2
00106$:
;src/bullet.c:18: }
	add	sp, #3
	ret
;src/bullet.c:20: void bullet_draw(bullet_t *bullet) {
;	---------------------------------
; Function bullet_draw
; ---------------------------------
_bullet_draw::
	add	sp, #-4
;src/bullet.c:21: set_sprite_tile(bullet->sprite, bullet->sprite);
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	bc
	push	bc
	inc	bc
	inc	bc
	ld	a, (bc)
	ld	d, a
	ld	e, d
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	l, d
	ld	h, #0x00
	add	hl, hl
	add	hl, hl
	ld	a, #<(_shadow_OAM)
	add	a, l
	ld	l, a
	ld	a, #>(_shadow_OAM)
	adc	a, h
	ld	h, a
	inc	hl
	inc	hl
	ld	(hl), e
;src/bullet.c:22: move_sprite(bullet->sprite, bullet->pos.x, bullet->pos.y);
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#2
	ld	(hl), a
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#3
	ld	(hl), a
	ld	a, (bc)
	ld	e, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	bc, #_shadow_OAM+0
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, bc
	ld	c, l
	ld	b, h
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	(bc), a
	inc	bc
	ld	a, (hl)
	ld	(bc), a
;src/bullet.c:22: move_sprite(bullet->sprite, bullet->pos.x, bullet->pos.y);
;src/bullet.c:23: }
	add	sp, #4
	ret
;src/bullet.c:25: void bullet_destroy(bullet_t *bullet) {
;	---------------------------------
; Function bullet_destroy
; ---------------------------------
_bullet_destroy::
;src/bullet.c:26: bullet->disabled = true;
	pop	bc
	pop	de
	push	de
	push	bc
	ld	hl, #0x0005
	add	hl, de
	ld	(hl), #0x01
;src/bullet.c:27: set_sprite_tile(bullet->sprite, S_NOTHING);
	inc	de
	inc	de
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	b, #0x00
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, hl
	ld	bc, #_shadow_OAM
	add	hl, bc
	inc	hl
	inc	hl
	ld	(hl), #0x1f
;src/bullet.c:28: move_sprite(bullet->sprite, 0, 0);
	ld	a, (de)
	ld	e, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	bc, #_shadow_OAM+0
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, bc
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, #0x00
	ld	(hl+), a
	ld	(hl), #0x00
;src/bullet.c:28: move_sprite(bullet->sprite, 0, 0);
;src/bullet.c:29: }
	ret
	.area _CODE
	.area _CABS (ABS)
