#include <gb/gb.h>
#include <gb/drawing.h>
#include "main.h"
#include "bullet.h"

void bullet_spawn(bullet_t *bullet) {
    bullet_draw(bullet);
}

void bullet_update(bullet_t *bullet) {
    if (bullet->disabled) return;
    if (bullet->pos.x < 16 || bullet->pos.x > SCREENWIDTH - 8) {
        bullet_destroy(bullet);
        return;
    }
    bullet->pos.x += (bullet->direction == fa_left ? -bullet->spd : bullet->spd);
    bullet_draw(bullet);
}

void bullet_draw(bullet_t *bullet) {
    set_sprite_tile(bullet->sprite, bullet->sprite);
    move_sprite(bullet->sprite, bullet->pos.x, bullet->pos.y);
}

void bullet_destroy(bullet_t *bullet) {
    bullet->disabled = true;
    set_sprite_tile(bullet->sprite, S_NOTHING);
    move_sprite(bullet->sprite, 0, 0);
}
