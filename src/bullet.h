#include <gb/gb.h>
#include "main.h"
#ifndef _BULLET_H_
#define _BULLET_H_

typedef struct bullet_t {
    POINT pos;
    UINT8 sprite, spd;
    facing_t direction;
    BOOL disabled;
} bullet_t;

void bullet_spawn(bullet_t *bullet);
void bullet_update(bullet_t *bullet);
void bullet_update_enemy(bullet_t *bullet);
void bullet_draw(bullet_t *bullet);
void bullet_draw_enemy(bullet_t *bullet);
void bullet_destroy(bullet_t *bullet);

#endif
