;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module coin
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _abs
	.globl _arand
	.globl _initarand
	.globl _factor
	.globl _rand_y
	.globl _rand_x
	.globl _coin_spawn
	.globl _coin_destroy
	.globl _coin_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
_rand_x::
	.ds 1
_rand_y::
	.ds 1
_factor::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;src/coin.c:8: INT8 rand_x = 0, rand_y = 0;
	ld	hl, #_rand_x
	ld	(hl), #0x00
;src/coin.c:8: UINT8 factor;
	ld	hl, #_rand_y
	ld	(hl), #0x00
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/coin.c:10: void coin_spawn(coin_t *coin, POINT *player_pos) {
;	---------------------------------
; Function coin_spawn
; ---------------------------------
_coin_spawn::
	add	sp, #-2
;src/coin.c:11: factor = (global_frame % 2 != 0 ? 1 : 2);
	ld	hl, #_global_frame
	bit	0, (hl)
	jr	Z, 00112$
	ld	bc, #0x0001
	jr	00113$
00112$:
	ld	bc, #0x0002
00113$:
	ld	hl, #_factor
	ld	(hl), c
;src/coin.c:12: seed = DIV_REG;
	ldh	a, (_DIV_REG+0)
	ld	hl, #_seed
	ld	(hl), a
;src/coin.c:13: seed |= (UINT16)DIV_REG << 8;
	ldh	a, (_DIV_REG+0)
;src/coin.c:15: initarand(seed + rand_menukey + (rand_menuframe * factor) * (global_frame * factor) + player_pos->y);
	ld	c, (hl)
	ld	b, #0x00
	ld	hl, #_rand_menukey
	ld	e, (hl)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, bc
	inc	sp
	inc	sp
	push	hl
	ld	a, (#_factor)
	push	af
	inc	sp
	ld	a, (#_rand_menuframe)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	push	de
	ld	a, (#_factor)
	push	af
	inc	sp
	ld	a, (#_global_frame)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	pop	bc
	push	de
	push	bc
	call	__mulint
	add	sp, #4
	pop	hl
	push	hl
	add	hl, de
	ld	e, l
	ld	d, h
	ldhl	sp,#6
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	l, c
	ld	h, b
	inc	hl
	ld	l, (hl)
	ld	h, #0x00
	add	hl, de
	push	bc
	push	hl
	call	_initarand
	add	sp, #2
	pop	bc
;src/coin.c:16: if (player_pos->x > (SCREENWIDTH / 2)) {
	ld	a, (bc)
	ld	b, a
;src/coin.c:17: rand_x = (factor == 1 ? 16 : 24);
	ld	a, (#_factor)
	dec	a
	ld	a, #0x01
	jr	Z, 00160$
	xor	a, a
00160$:
	ld	c, a
;src/coin.c:16: if (player_pos->x > (SCREENWIDTH / 2)) {
	ld	a, #0x50
	sub	a, b
	jr	NC, 00102$
;src/coin.c:17: rand_x = (factor == 1 ? 16 : 24);
	ld	a, c
	or	a, a
	jr	Z, 00114$
	ld	bc, #0x0010
	jr	00115$
00114$:
	ld	bc, #0x0018
00115$:
	ld	hl, #_rand_x
	ld	(hl), c
	jr	00125$
00102$:
;src/coin.c:19: rand_x = (factor == 1 ? (SCREENWIDTH - 24) : (SCREENWIDTH - 28));
	ld	a, c
	or	a, a
	ld	a, #0x88
	jr	NZ, 00117$
	ld	a, #0x84
00117$:
	ld	(#_rand_x),a
;src/coin.c:22: while (rand_y < 32 || rand_y > 120 || rand_y == coin->pos.y || rand_y % 2 != 0)
00125$:
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	bc
	push	bc
	inc	bc
00107$:
	ld	hl, #_rand_y
	ld	a, (hl)
	xor	a, #0x80
	sub	a, #0xa0
	jr	C, 00108$
	ld	e, (hl)
	ld	a,#0x78
	ld	d,a
	sub	a, (hl)
	bit	7, e
	jr	Z, 00161$
	bit	7, d
	jr	NZ, 00162$
	cp	a, a
	jr	00162$
00161$:
	bit	7, d
	jr	Z, 00162$
	scf
00162$:
	jr	C, 00108$
	ld	a, (#_rand_y)
	ld	e, a
	rla
	sbc	a, a
	ld	d, a
	ld	a, (bc)
	ld	l, a
	ld	h, #0x00
	ld	a, l
	sub	a, e
	jr	NZ, 00163$
	ld	a, h
	sub	a, d
	jr	Z, 00108$
00163$:
	push	bc
	ld	hl, #0x0002
	push	hl
	push	de
	call	__modsint
	add	sp, #4
	pop	bc
	ld	a, d
	or	a, e
	jr	Z, 00109$
00108$:
;src/coin.c:23: rand_y = abs(arand());
	push	bc
	call	_arand
	ld	a, e
	push	af
	inc	sp
	call	_abs
	inc	sp
	pop	bc
	ld	hl, #_rand_y
	ld	(hl), e
	jr	00107$
00109$:
;src/coin.c:25: coin->pos.x = rand_x;
	pop	de
	push	de
	ld	a, (#_rand_x)
	ld	(de), a
;src/coin.c:26: coin->pos.y = rand_y;
	ld	a, (#_rand_y)
	ld	(bc), a
;src/coin.c:28: coin_draw(coin);
	pop	hl
	push	hl
	push	hl
	call	_coin_draw
	add	sp, #2
;src/coin.c:29: }
	add	sp, #2
	ret
_fps:
	.db #0x3c	; 60
;src/coin.c:32: void coin_destroy(coin_t *coin, POINT *player_pos) {
;	---------------------------------
; Function coin_destroy
; ---------------------------------
_coin_destroy::
;src/coin.c:33: coin_spawn(coin, player_pos);
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_coin_spawn
	add	sp, #4
;src/coin.c:34: }
	ret
;src/coin.c:36: void coin_draw(coin_t *coin) {
;	---------------------------------
; Function coin_draw
; ---------------------------------
_coin_draw::
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	hl, #(_shadow_OAM + 0x0066)
	ld	(hl), #0x19
;src/coin.c:38: move_sprite(S_COIN, coin->pos.x, coin->pos.y);
	pop	bc
	pop	de
	push	de
	push	bc
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #(_shadow_OAM + 0x0064)
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/coin.c:38: move_sprite(S_COIN, coin->pos.x, coin->pos.y);
;src/coin.c:39: }
	ret
	.area _CODE
	.area _CABS (ABS)
