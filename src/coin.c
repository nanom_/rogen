#include <gb/gb.h>
#include <rand.h>
#include "math.h"
#include "main.h"
#include "game.h"
#include "coin.h"

INT8 rand_x = 0, rand_y = 0;
UINT8 factor;
void coin_spawn(coin_t *coin, POINT *player_pos) {
    factor = (global_frame % 2 != 0 ? 1 : 2);
    seed = DIV_REG;
    seed |= (UINT16)DIV_REG << 8;

    initarand(seed + rand_menukey + (rand_menuframe * factor) * (global_frame * factor) + player_pos->y);
    if (player_pos->x > (SCREENWIDTH / 2)) {
        rand_x = (factor == 1 ? 16 : 24);
    } else {
        rand_x = (factor == 1 ? (SCREENWIDTH - 24) : (SCREENWIDTH - 28));
    }

    while (rand_y < 32 || rand_y > 120 || rand_y == coin->pos.y || rand_y % 2 != 0)
        rand_y = abs(arand());

    coin->pos.x = rand_x;
    coin->pos.y = rand_y;
    
    coin_draw(coin);
}

// "destroying" is just an illusion OwO
void coin_destroy(coin_t *coin, POINT *player_pos) {
    coin_spawn(coin, player_pos);
}

void coin_draw(coin_t *coin) {
    set_sprite_tile(S_COIN, 25);
    move_sprite(S_COIN, coin->pos.x, coin->pos.y);
}
