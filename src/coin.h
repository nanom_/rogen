#include <gb/gb.h>
#include "main.h"
#ifndef _COIN_H_
#define _COIN_H_

typedef struct coin_t {
    POINT pos;
} coin_t;

void coin_spawn(coin_t *, POINT *player_pos);
void coin_destroy(coin_t *, POINT *player_pos);
void coin_draw(coin_t *);

#endif
