;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module drone
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _bullet_destroy
	.globl _abs
	.globl _arand
	.globl _initarand
	.globl _randnum
	.globl _randfactor
	.globl _drone_spawn
	.globl _drone_update
	.globl _drone_check_collisions
	.globl _drone_nextframe
	.globl _drone_kill
	.globl _drone_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
_randfactor::
	.ds 1
_randnum::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/drone.c:11: void drone_spawn(drone_t *drone, coin_t *coin) {
;	---------------------------------
; Function drone_spawn
; ---------------------------------
_drone_spawn::
	add	sp, #-6
;src/drone.c:12: drone->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x + 8 : coin->pos.x - 8);
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	bc
	push	bc
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	l, a
	sub	a, #0x50
	jr	NC, 00114$
	ld	a, l
	add	a, #0x08
	jr	00115$
00114$:
	ld	a, l
	add	a, #0xf8
00115$:
	ld	(bc), a
;src/drone.c:13: drone->pos.y = coin->pos.y;
	pop	bc
	push	bc
	inc	bc
	inc	de
	ld	a, (de)
	ld	(bc), a
;src/drone.c:14: drone->entity.is_dead = false;
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	e, l
	ld	d, h
	xor	a, a
	ld	(de), a
;src/drone.c:15: drone->destroy_timer = 0;
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	e, l
	ld	d, h
	xor	a, a
	ld	(de), a
;src/drone.c:17: randfactor = (global_frame % 2 != 0 ? 1 : 2);
	ld	hl, #_global_frame
	bit	0, (hl)
	jr	Z, 00116$
	ld	de, #0x0001
	jr	00117$
00116$:
	ld	de, #0x0002
00117$:
	ld	hl, #_randfactor
	ld	(hl), e
;src/drone.c:18: seed = DIV_REG;
	ldh	a, (_DIV_REG+0)
	ld	hl, #_seed
	ld	(hl), a
;src/drone.c:19: seed |= (UINT16)DIV_REG << 8;
	ldh	a, (_DIV_REG+0)
;src/drone.c:20: initarand(seed + rand_menukey + (rand_menuframe * randfactor) * (global_frame * randfactor) + drone->pos.y);
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ld	hl, #_rand_menukey
	ld	e, (hl)
	ld	d, #0x00
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
	push	bc
	ld	a, (#_randfactor)
	push	af
	inc	sp
	ld	a, (#_rand_menuframe)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	ldhl	sp,	#6
	ld	a, e
	ld	(hl+), a
	ld	(hl), d
	ld	a, (#_randfactor)
	push	af
	inc	sp
	ld	a, (#_global_frame)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	push	de
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	__mulint
	add	sp, #4
	pop	bc
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (bc)
	ld	l, a
	ld	h, #0x00
	add	hl, de
	push	hl
	call	_initarand
	add	sp, #2
;src/drone.c:22: randnum = abs(arand());
	call	_arand
	ld	a, e
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	hl, #_randnum
	ld	(hl), e
;src/drone.c:26: drone->direction = d_left;
	pop	de
	push	de
	ld	hl, #0x000f
	add	hl, de
	ld	c, l
	ld	b, h
;src/drone.c:25: if (randnum <= 31)
	ld	a, #0x1f
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00110$
;src/drone.c:26: drone->direction = d_left;
	xor	a, a
	ld	(bc), a
	jr	00112$
00110$:
;src/drone.c:27: else if (randnum <= 62)
	ld	a, #0x3e
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00107$
;src/drone.c:28: drone->direction = d_right;
	ld	a, #0x01
	ld	(bc), a
	jr	00112$
00107$:
;src/drone.c:29: else if (randnum <= 93)
	ld	a, #0x5d
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00104$
;src/drone.c:30: drone->direction = d_down;
	ld	a, #0x02
	ld	(bc), a
	jr	00112$
00104$:
;src/drone.c:31: else if (randnum <= 127)
	ld	a, #0x7f
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00112$
;src/drone.c:32: drone->direction = d_up;
	ld	a, #0x03
	ld	(bc), a
00112$:
;src/drone.c:33: }
	add	sp, #6
	ret
_fps:
	.db #0x3c	; 60
;src/drone.c:35: void drone_update(drone_t *drone, coin_t *coin, bullet_t *bullet) {
;	---------------------------------
; Function drone_update
; ---------------------------------
_drone_update::
	add	sp, #-11
;src/drone.c:36: if (drone->entity.is_dead && drone->destroy_timer >= 50) return;
	ldhl	sp,	#13
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	c, (hl)
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#9
	ld	(hl+), a
	ld	(hl), d
	ld	a, c
	or	a, a
	jr	Z, 00102$
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jp	NC,00152$
00102$:
;src/drone.c:37: if (drone->entity.is_dead && drone->destroy_timer < 50) {
	ld	a, c
	or	a, a
	jr	Z, 00109$
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	cp	a, #0x32
	jr	NC, 00109$
;src/drone.c:38: drone->destroy_timer++;
	ld	c, a
	inc	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/drone.c:39: if (drone->destroy_timer % 10 != 0)
	ld	b, #0x00
	ld	hl, #0x000a
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	ld	c, e
	ld	a, d
	or	a, c
	jr	Z, 00105$
;src/drone.c:40: drone->entity.sprite.frame = (drone->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);
	pop	de
	push	de
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	sub	a, #0x16
	jr	NZ, 00154$
	ld	de, #0x0017
	jr	00155$
00154$:
	ld	de, #0x0016
00155$:
	ld	a, e
	ld	(bc), a
00105$:
;src/drone.c:42: if (drone->destroy_timer == 50) drone_kill(drone);
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jr	NZ, 00107$
	pop	hl
	push	hl
	push	hl
	call	_drone_kill
	add	sp, #2
00107$:
;src/drone.c:44: drone_draw(drone);
	pop	hl
	push	hl
	push	hl
	call	_drone_draw
	add	sp, #2
;src/drone.c:45: return;
	jp	00152$
00109$:
;src/drone.c:47: if (!drone->can_move) return;
	pop	de
	push	de
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jp	Z,00152$
;src/drone.c:48: drone_nextframe(drone);
	pop	hl
	push	hl
	push	hl
	call	_drone_nextframe
	add	sp, #2
;src/drone.c:50: if (drone->pos.x > SCREENWIDTH - 16)
	pop	de
	push	de
	ld	a, (de)
	ld	c, a
;src/drone.c:51: drone->direction = d_left;
	pop	de
	push	de
	ld	hl, #0x000f
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
;src/drone.c:50: if (drone->pos.x > SCREENWIDTH - 16)
	ld	a, #0x90
	sub	a, c
	jr	NC, 00114$
;src/drone.c:51: drone->direction = d_left;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
00114$:
;src/drone.c:52: if (drone->pos.x < 16)
	pop	de
	push	de
	ld	a, (de)
	sub	a, #0x10
	jr	NC, 00116$
;src/drone.c:53: drone->direction = d_right;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
00116$:
;src/drone.c:54: if (drone->pos.y > SCREENHEIGHT - 16)
	pop	de
	push	de
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#4
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	ld	a, #0x80
	sub	a, c
	jr	NC, 00118$
;src/drone.c:55: drone->direction = d_up;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x03
00118$:
;src/drone.c:56: if (drone->pos.y < 32)
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x20
	jr	NC, 00120$
;src/drone.c:57: drone->direction = d_down;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x02
00120$:
;src/drone.c:59: if (abs(drone->pos.x - coin->pos.x) > 24 || abs(drone->pos.y - coin->pos.y) > 24) {
	pop	de
	push	de
	ld	a, (de)
	ld	c, a
	ldhl	sp,	#15
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#6
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
	ld	a, c
	sub	a, b
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	a, #0x18
	sub	a, e
	jr	C, 00143$
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#8
	ld	(hl-), a
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#9
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	hl
	dec	hl
	ld	a, (hl)
	sub	a, c
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	a, #0x18
	sub	a, e
	jr	NC, 00144$
00143$:
;src/drone.c:60: switch(drone->direction) {
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	Z, 00121$
	cp	a, #0x01
	jr	Z, 00122$
	cp	a, #0x02
	jr	Z, 00123$
	sub	a, #0x03
	jr	Z, 00124$
	jp	00145$
;src/drone.c:61: case d_left:
00121$:
;src/drone.c:62: drone->direction = d_right;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
;src/drone.c:63: break;
	jp	00145$
;src/drone.c:64: case d_right:
00122$:
;src/drone.c:65: drone->direction = d_left;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/drone.c:66: break;
	jp	00145$
;src/drone.c:67: case d_down:
00123$:
;src/drone.c:68: drone->direction = d_up;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x03
;src/drone.c:69: break;
	jp	00145$
;src/drone.c:70: case d_up:
00124$:
;src/drone.c:71: drone->direction = d_down;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x02
;src/drone.c:73: }
	jp	00145$
00144$:
;src/drone.c:74: } else if (global_frame % 25 == 0) {
	ld	hl, #_global_frame
	ld	c, (hl)
	ld	b, #0x00
	push	bc
	ld	hl, #0x0019
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	pop	bc
	ld	a, d
	or	a, e
	jp	NZ, 00145$
;src/drone.c:75: initarand(drone->pos.x * drone->pos.y - global_frame);
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,#4
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	push	af
	ld	a, (de)
	ld	h, a
	pop	af
	push	bc
	push	hl
	inc	sp
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	pop	bc
	ld	a, e
	sub	a, c
	ld	c, a
	ld	a, d
	sbc	a, b
	ld	b, a
	push	bc
	call	_initarand
	add	sp, #2
;src/drone.c:76: randnum = abs(arand());
	call	_arand
	ld	a, e
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	hl, #_randnum
	ld	(hl), e
;src/drone.c:77: switch(drone->direction) {
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
;src/drone.c:82: if (randnum <= 45) drone->direction = d_right;
	ld	a, #0x2d
	ld	hl, #_randnum
	sub	a, (hl)
	ld	a, #0x00
	rla
	ld	c, a
;src/drone.c:77: switch(drone->direction) {
	ld	a, b
	or	a, a
	jr	Z, 00134$
	ld	a, b
	dec	a
	jr	Z, 00137$
	ld	a,b
	cp	a,#0x02
	jr	Z, 00129$
	sub	a, #0x03
	jr	NZ, 00145$
;src/drone.c:79: if (randnum <= 20) drone->direction = d_left;
	ld	a, #0x14
	sub	a, (hl)
	jr	C, 00145$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/drone.c:80: break;
	jr	00145$
;src/drone.c:81: case d_down:
00129$:
;src/drone.c:82: if (randnum <= 45) drone->direction = d_right;
	bit	0, c
	jr	NZ, 00131$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
00131$:
;src/drone.c:83: if (randnum <= 10) drone->direction = d_left;
	ld	a, #0x0a
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00145$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/drone.c:84: break;
	jr	00145$
;src/drone.c:85: case d_left:
00134$:
;src/drone.c:86: if (randnum <= 45) drone->direction = d_down;
	bit	0, c
	jr	NZ, 00145$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x02
;src/drone.c:87: break;
	jr	00145$
;src/drone.c:88: case d_right:
00137$:
;src/drone.c:89: if (randnum <= 45) drone->direction = d_up;
	bit	0, c
	jr	NZ, 00145$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x03
;src/drone.c:91: }
00145$:
;src/drone.c:94: switch(drone->direction) {
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
;src/drone.c:96: drone->pos.x -= drone->entity.spd;
	pop	de
	push	de
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
;src/drone.c:97: drone->entity.facing = fa_left;
	pop	de
	push	de
	ld	hl, #0x0009
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#7
	ld	(hl+), a
	ld	(hl), d
;src/drone.c:98: drone->entity.is_moving = true;
	pop	de
	push	de
	ld	hl, #0x000b
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#9
	ld	(hl+), a
	ld	(hl), d
;src/drone.c:94: switch(drone->direction) {
	ld	a, c
	or	a, a
	jr	Z, 00147$
	ld	a, c
	dec	a
	jr	Z, 00148$
	ld	a,c
	cp	a,#0x02
	jr	Z, 00149$
	sub	a, #0x03
	jr	Z, 00150$
	jr	00151$
;src/drone.c:95: case d_left:
00147$:
;src/drone.c:96: drone->pos.x -= drone->entity.spd;
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,#2
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	push	af
	ld	a, (de)
	ld	c, a
	pop	af
	sub	a, c
	ld	c, a
	pop	hl
	push	hl
	ld	(hl), c
;src/drone.c:97: drone->entity.facing = fa_left;
	ldhl	sp,	#7
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/drone.c:98: drone->entity.is_moving = true;
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/drone.c:99: break;
	jr	00151$
;src/drone.c:100: case d_right:
00148$:
;src/drone.c:101: drone->pos.x += drone->entity.spd;
	pop	de
	push	de
	ld	a, (de)
	ld	c, a
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, c
	ld	c, a
	pop	hl
	push	hl
	ld	(hl), c
;src/drone.c:102: drone->entity.facing = fa_right;
	ldhl	sp,	#7
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/drone.c:103: drone->entity.is_moving = true;
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/drone.c:104: break;
	jr	00151$
;src/drone.c:105: case d_down:
00149$:
;src/drone.c:106: drone->pos.y += drone->entity.spd;
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	hl
	dec	hl
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, c
	ld	c, a
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/drone.c:107: drone->entity.is_moving = true;
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/drone.c:108: break;
	jr	00151$
;src/drone.c:109: case d_up:
00150$:
;src/drone.c:110: drone->pos.y -= drone->entity.spd;
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	dec	hl
	dec	hl
	dec	hl
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	push	af
	ld	a, (de)
	ld	c, a
	pop	af
	sub	a, c
	ld	c, a
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/drone.c:111: drone->entity.is_moving = true;
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/drone.c:113: }
00151$:
;src/drone.c:115: drone_check_collisions(drone, bullet);
	ldhl	sp,	#17
	ld	a, (hl+)
	ld	h, (hl)
	ld	c, a
	ld	b, h
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_drone_check_collisions
	add	sp, #4
;src/drone.c:116: drone_draw(drone);
	ldhl	sp,	#13
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_drone_draw
	add	sp, #2
00152$:
;src/drone.c:117: }
	add	sp, #11
	ret
;src/drone.c:119: void drone_check_collisions(drone_t *drone, bullet_t *bullet) {
;	---------------------------------
; Function drone_check_collisions
; ---------------------------------
_drone_check_collisions::
	add	sp, #-12
;src/drone.c:120: if (!bullet->disabled && drone->pos.x >= bullet->pos.x - 2 && drone->pos.y >= bullet->pos.y - 2 && drone->pos.x <= bullet->pos.x + 10 && drone->pos.y <= bullet->pos.y + 10) {
	ldhl	sp,#16
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0005
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jp	NZ, 00107$
	ldhl	sp,	#14
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#9
	ld	(hl), a
	ld	a, (bc)
	ldhl	sp,	#2
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0002
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#11
	ld	(hl-), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#4
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00134$
	bit	7, d
	jr	NZ, 00135$
	cp	a, a
	jr	00135$
00134$:
	bit	7, d
	jr	Z, 00135$
	scf
00135$:
	jp	C, 00107$
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#11
	ld	(hl), a
	ld	e, c
	ld	d, b
	inc	de
	ld	a, (de)
	ldhl	sp,	#6
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0002
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#9
	ld	(hl-), a
	ld	(hl), e
	ldhl	sp,	#11
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#10
	ld	e, l
	ld	d, h
	ldhl	sp,	#8
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00136$
	bit	7, d
	jr	NZ, 00137$
	cp	a, a
	jr	00137$
00136$:
	bit	7, d
	jr	Z, 00137$
	scf
00137$:
	jp	C, 00107$
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#4
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00138$
	bit	7, d
	jr	NZ, 00139$
	cp	a, a
	jr	00139$
00138$:
	bit	7, d
	jr	Z, 00139$
	scf
00139$:
	jr	C, 00107$
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00140$
	bit	7, d
	jr	NZ, 00141$
	cp	a, a
	jr	00141$
00140$:
	bit	7, d
	jr	Z, 00141$
	scf
00141$:
	jr	C, 00107$
;src/drone.c:121: drone->entity.is_dead = true;
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	(hl), #0x01
;src/drone.c:122: drone->entity.sprite.frame = S_EXPLO1;
	pop	de
	push	de
	ld	hl, #0x0008
	add	hl, de
	ld	(hl), #0x16
;src/drone.c:123: drone->destroy_timer = 0;
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	e, l
	ld	d, h
	xor	a, a
	ld	(de), a
;src/drone.c:125: bullet_destroy(bullet);
	push	bc
	call	_bullet_destroy
	add	sp, #2
00107$:
;src/drone.c:127: }
	add	sp, #12
	ret
;src/drone.c:129: void drone_nextframe(drone_t *drone) {
;	---------------------------------
; Function drone_nextframe
; ---------------------------------
_drone_nextframe::
	add	sp, #-3
;src/drone.c:130: if (global_frame % 15 != 0) return;
	ld	hl, #_global_frame
	ld	c, (hl)
	ld	b, #0x00
	ld	hl, #0x000f
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	ld	a, d
	or	a, e
	jr	NZ, 00106$
;src/drone.c:131: if (drone->entity.sprite.frame < drone->entity.sprite.last_frame)
	ldhl	sp,#5
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	push	de
	ld	hl, #0x0002
	add	hl, de
	pop	de
	inc	sp
	inc	sp
	push	hl
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	ldhl	sp,	#2
	ld	(hl), a
	inc	de
	inc	de
	inc	de
	ld	a, (de)
	ld	e, a
;src/drone.c:132: drone->entity.sprite.frame++;
	ld	a,(hl)
	cp	a,e
	jr	NC, 00104$
	inc	a
	ld	(bc), a
	jr	00106$
00104$:
;src/drone.c:134: drone->entity.sprite.frame = drone->entity.sprite.first_frame;
	pop	de
	push	de
	ld	a, (de)
	ld	(bc), a
00106$:
;src/drone.c:135: }
	add	sp, #3
	ret
;src/drone.c:137: void drone_kill(drone_t *drone) {
;	---------------------------------
; Function drone_kill
; ---------------------------------
_drone_kill::
;src/drone.c:139: drone->pos.x = 200;
	pop	bc
	pop	de
	push	de
	push	bc
	ld	a, #0xc8
	ld	(de), a
;src/drone.c:140: drone->pos.y = 200;
	ld	l, e
	ld	h, d
	inc	hl
	ld	(hl), #0xc8
;src/drone.c:142: if (!drone->entity.is_dead) drone->entity.is_dead = true;
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00103$
	ld	(hl), #0x01
;src/drone.c:143: if (!drone->destroy_timer != 50) drone->destroy_timer = 50;
00103$:
	ld	hl, #0x0011
	add	hl, de
	ld	(hl), #0x32
;src/drone.c:144: drone_draw(drone);
	push	de
	call	_drone_draw
	add	sp, #2
;src/drone.c:145: }
	ret
;src/drone.c:147: void drone_draw(drone_t *drone) {
;	---------------------------------
; Function drone_draw
; ---------------------------------
_drone_draw::
	dec	sp
;src/drone.c:148: set_sprite_tile(S_DRONE, drone->entity.sprite.frame);
	ldhl	sp,#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	bc, #_shadow_OAM+34
	ldhl	sp,	#0
	ld	a, (hl)
	ld	(bc), a
;src/drone.c:149: move_sprite(S_DRONE, drone->pos.x, drone->pos.y);
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #(_shadow_OAM + 0x0020)
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/drone.c:149: move_sprite(S_DRONE, drone->pos.x, drone->pos.y);
;src/drone.c:150: }
	inc	sp
	ret
	.area _CODE
	.area _CABS (ABS)
