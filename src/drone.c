#include <gb/gb.h>
#include <rand.h>
#include "main.h"
#include "game.h"
#include "math.h"
#include "coin.h"
#include "bullet.h"
#include "drone.h"

UINT8 randfactor, randnum;
void drone_spawn(drone_t *drone, coin_t *coin) {
    drone->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x + 8 : coin->pos.x - 8);
    drone->pos.y = coin->pos.y;
    drone->entity.is_dead = false;
    drone->destroy_timer = 0;

    randfactor = (global_frame % 2 != 0 ? 1 : 2);
    seed = DIV_REG;
    seed |= (UINT16)DIV_REG << 8;
    initarand(seed + rand_menukey + (rand_menuframe * randfactor) * (global_frame * randfactor) + drone->pos.y);

    randnum = abs(arand());

    // random direction (the worst way to do this since Game Boy's RNGs are quite bad)
    if (randnum <= 31)
        drone->direction = d_left;
    else if (randnum <= 62)
        drone->direction = d_right;
    else if (randnum <= 93)
        drone->direction = d_down;
    else if (randnum <= 127)
        drone->direction = d_up;
}

void drone_update(drone_t *drone, coin_t *coin, bullet_t *bullet) {
    if (drone->entity.is_dead && drone->destroy_timer >= 50) return;
    if (drone->entity.is_dead && drone->destroy_timer < 50) {
        drone->destroy_timer++;
        if (drone->destroy_timer % 10 != 0)
            drone->entity.sprite.frame = (drone->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);

        if (drone->destroy_timer == 50) drone_kill(drone);

        drone_draw(drone);
        return;
    }
    if (!drone->can_move) return;
    drone_nextframe(drone);

    if (drone->pos.x > SCREENWIDTH - 16)
        drone->direction = d_left;
    if (drone->pos.x < 16)
        drone->direction = d_right;
    if (drone->pos.y > SCREENHEIGHT - 16)
        drone->direction = d_up;
    if (drone->pos.y < 32)
        drone->direction = d_down;

    if (abs(drone->pos.x - coin->pos.x) > 24 || abs(drone->pos.y - coin->pos.y) > 24) {
        switch(drone->direction) {
            case d_left:
                drone->direction = d_right;
                break;
            case d_right:
                drone->direction = d_left;
                break;
            case d_down:
                drone->direction = d_up;
                break;
            case d_up:
                drone->direction = d_down;
                break;
        }
    } else if (global_frame % 25 == 0) {
        initarand(drone->pos.x * drone->pos.y - global_frame);
        randnum = abs(arand());
        switch(drone->direction) {
            case d_up:
                if (randnum <= 20) drone->direction = d_left;
                break;
            case d_down:
                if (randnum <= 45) drone->direction = d_right;
                if (randnum <= 10) drone->direction = d_left;
                break;
            case d_left:
                if (randnum <= 45) drone->direction = d_down;
                break;
            case d_right:
                if (randnum <= 45) drone->direction = d_up;
                break;
        }
    }

    switch(drone->direction) {
        case d_left:
            drone->pos.x -= drone->entity.spd;
            drone->entity.facing = fa_left;
            drone->entity.is_moving = true;
            break;
        case d_right:
            drone->pos.x += drone->entity.spd;
            drone->entity.facing = fa_right;
            drone->entity.is_moving = true;
            break;
        case d_down:
            drone->pos.y += drone->entity.spd;
            drone->entity.is_moving = true;
            break;
        case d_up:
            drone->pos.y -= drone->entity.spd;
            drone->entity.is_moving = true;
            break;
    }

    drone_check_collisions(drone, bullet);
    drone_draw(drone);
}

void drone_check_collisions(drone_t *drone, bullet_t *bullet) {
    if (!bullet->disabled && drone->pos.x >= bullet->pos.x - 2 && drone->pos.y >= bullet->pos.y - 2 && drone->pos.x <= bullet->pos.x + 10 && drone->pos.y <= bullet->pos.y + 10) {
        drone->entity.is_dead = true;
        drone->entity.sprite.frame = S_EXPLO1;
        drone->destroy_timer = 0;

        bullet_destroy(bullet);
    }
}

void drone_nextframe(drone_t *drone) {
    if (global_frame % 15 != 0) return;
    if (drone->entity.sprite.frame < drone->entity.sprite.last_frame)
        drone->entity.sprite.frame++;
    else
        drone->entity.sprite.frame = drone->entity.sprite.first_frame;
}

void drone_kill(drone_t *drone) {
    // we just move the enemy out of the view
    drone->pos.x = 200;
    drone->pos.y = 200;
    // and we turn off the entity's updating function by setting the "is_dead" property to "true" and the "destroy_timer" property to 50
    if (!drone->entity.is_dead) drone->entity.is_dead = true;
    if (!drone->destroy_timer != 50) drone->destroy_timer = 50;
    drone_draw(drone);
}

void drone_draw(drone_t *drone) {
    set_sprite_tile(S_DRONE, drone->entity.sprite.frame);
    move_sprite(S_DRONE, drone->pos.x, drone->pos.y);
}
