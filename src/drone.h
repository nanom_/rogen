#include <gb/gb.h>
#include "main.h"
#include "coin.h"
#include "bullet.h"
#ifndef _DRONE_H_
#define _DRONE_H_

typedef struct drone_t {
    POINT pos;
    entity_t entity;
    direction_t direction;
    BOOL can_move;
    UINT8 destroy_timer;
} drone_t;

void drone_spawn(drone_t *drone, coin_t *coin);
void drone_update(drone_t *drone, coin_t *coin, bullet_t *bullet);
void drone_check_collisions(drone_t *drone, bullet_t *bullet);
void drone_nextframe(drone_t *drone);
void drone_kill(drone_t *drone);
void drone_draw(drone_t *drone);

#endif
