;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module game
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _is_prime
	.globl _abs
	.globl _beam_update
	.globl _beam_spawn
	.globl _hud_start
	.globl _player_update
	.globl _player_input
	.globl _player_spawn
	.globl _item_update
	.globl _item_spawn
	.globl _spike_update
	.globl _spike_spawn
	.globl _heatball_update
	.globl _heatball_spawn
	.globl _skulltron_update
	.globl _skulltron_spawn
	.globl _turret_update
	.globl _turret_spawn
	.globl _splitter_update
	.globl _splitter_spawn
	.globl _drone_update
	.globl _drone_spawn
	.globl _bullet_update
	.globl _coin_spawn
	.globl _arand
	.globl _initarand
	.globl _set_sprite_data
	.globl _set_bkg_tiles
	.globl _set_bkg_data
	.globl _already_used
	.globl _beams
	.globl _rand_beam_x
	.globl _rand_seed_factor
	.globl __b
	.globl _b
	.globl _global_beam_spd
	.globl _beam_count
	.globl _enemy_bullet
	.globl _player_bullet
	.globl _item
	.globl _spike
	.globl _heatball
	.globl _turret2
	.globl _turret
	.globl _skullperior
	.globl _skulltron
	.globl _splitter
	.globl _drone
	.globl _coin
	.globl _player
	.globl _game_start
	.globl _game_input
	.globl _game_update
	.globl _game_next_enemy
	.globl _game_spawn_item
	.globl _game_new_beam
	.globl _game_beam_speed_medium
	.globl _game_beam_speed_hard
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
_player::
	.ds 23
_coin::
	.ds 2
_drone::
	.ds 18
_splitter::
	.ds 18
_skulltron::
	.ds 22
_skullperior::
	.ds 22
_turret::
	.ds 21
_turret2::
	.ds 21
_heatball::
	.ds 18
_spike::
	.ds 18
_item::
	.ds 4
_player_bullet::
	.ds 6
_enemy_bullet::
	.ds 6
_beam_count::
	.ds 1
_global_beam_spd::
	.ds 1
_b::
	.ds 1
__b::
	.ds 1
_rand_seed_factor::
	.ds 1
_rand_beam_x::
	.ds 1
_beams::
	.ds 28
_already_used::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;src/game.c:23: player_t player = {{16, 78}, {16, 78}, {{0, 7, 0, 3, 4, 7, 4}, fa_right, 2, false, false, true, false}, 3, 0, 3, true, false, 0};
	ld	hl, #_player
	ld	(hl), #0x10
	ld	hl, #(_player + 0x0001)
	ld	(hl), #0x4e
	ld	hl, #(_player + 0x0002)
	ld	(hl), #0x10
	ld	hl, #(_player + 0x0003)
	ld	(hl), #0x4e
	ld	hl, #(_player + 0x0004)
	ld	(hl), #0x00
	ld	hl, #(_player + 0x0005)
	ld	(hl), #0x07
	ld	hl, #(_player + 0x0006)
	ld	(hl), #0x00
	ld	hl, #(_player + 0x0007)
	ld	(hl), #0x03
	ld	hl, #(_player + 0x0008)
	ld	(hl), #0x04
	ld	hl, #(_player + 0x0009)
	ld	(hl), #0x07
	ld	hl, #(_player + 0x000a)
	ld	(hl), #0x04
	ld	hl, #(_player + 0x000b)
	ld	(hl), #0x01
	ld	hl, #(_player + 0x000c)
	ld	(hl), #0x02
	ld	hl, #(_player + 0x000d)
	ld	(hl), #0x00
	ld	hl, #(_player + 0x000e)
	ld	(hl), #0x00
	ld	hl, #(_player + 0x000f)
	ld	(hl), #0x01
	ld	hl, #(_player + 0x0010)
	ld	(hl), #0x00
	ld	hl, #(_player + 0x0011)
	ld	(hl), #0x03
	ld	hl, #(_player + 0x0012)
	ld	(hl), #0x00
	ld	hl, #(_player + 0x0013)
	ld	(hl), #0x03
	ld	hl, #(_player + 0x0014)
	ld	(hl), #0x01
	ld	hl, #(_player + 0x0015)
	ld	(hl), #0x00
	ld	hl, #(_player + 0x0016)
	ld	(hl), #0x00
;src/game.c:24: coin_t coin = {{0, 0}};
	ld	bc, #_coin+0
	xor	a, a
	ld	(bc), a
	inc	bc
	xor	a, a
	ld	(bc), a
;src/game.c:26: drone_t drone = {
	ld	hl, #_drone
	ld	(hl), #0xc8
	ld	hl, #(_drone + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_drone + 0x0002)
	ld	(hl), #0x08
	ld	hl, #(_drone + 0x0003)
	ld	(hl), #0x09
	ld	hl, #(_drone + 0x0004)
	ld	(hl), #0x08
	ld	hl, #(_drone + 0x0005)
	ld	(hl), #0x09
	ld	hl, #(_drone + 0x0006)
	ld	(hl), #0x08
	ld	hl, #(_drone + 0x0007)
	ld	(hl), #0x09
	ld	hl, #(_drone + 0x0008)
	ld	(hl), #0x08
	ld	hl, #(_drone + 0x0009)
	ld	(hl), #0x00
	ld	hl, #(_drone + 0x000a)
	ld	(hl), #0x01
	ld	hl, #(_drone + 0x000b)
	ld	(hl), #0x00
	ld	hl, #(_drone + 0x000c)
	ld	(hl), #0x00
	ld	hl, #(_drone + 0x000d)
	ld	(hl), #0x00
	ld	hl, #(_drone + 0x000e)
	ld	(hl), #0x01
	ld	hl, #(_drone + 0x000f)
	ld	(hl), #0x04
	ld	hl, #(_drone + 0x0010)
	ld	(hl), #0x01
	ld	hl, #(_drone + 0x0011)
	ld	(hl), #0x32
;src/game.c:31: splitter_t splitter = {
	ld	hl, #_splitter
	ld	(hl), #0xc8
	ld	hl, #(_splitter + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_splitter + 0x0002)
	ld	(hl), #0x0a
	ld	hl, #(_splitter + 0x0003)
	ld	(hl), #0x0b
	ld	hl, #(_splitter + 0x0004)
	ld	(hl), #0x0a
	ld	hl, #(_splitter + 0x0005)
	ld	(hl), #0x0b
	ld	hl, #(_splitter + 0x0006)
	ld	(hl), #0x0a
	ld	hl, #(_splitter + 0x0007)
	ld	(hl), #0x0b
	ld	hl, #(_splitter + 0x0008)
	ld	(hl), #0x0a
	ld	hl, #(_splitter + 0x0009)
	ld	(hl), #0x00
	ld	hl, #(_splitter + 0x000a)
	ld	(hl), #0x02
	ld	hl, #(_splitter + 0x000b)
	ld	(hl), #0x00
	ld	hl, #(_splitter + 0x000c)
	ld	(hl), #0x00
	ld	hl, #(_splitter + 0x000d)
	ld	(hl), #0x00
	ld	hl, #(_splitter + 0x000e)
	ld	(hl), #0x01
	ld	hl, #(_splitter + 0x000f)
	ld	(hl), #0x04
	ld	hl, #(_splitter + 0x0010)
	ld	(hl), #0x01
	ld	hl, #(_splitter + 0x0011)
	ld	(hl), #0x32
;src/game.c:36: skulltron_t skulltron = {
	ld	hl, #_skulltron
	ld	(hl), #0xc8
	ld	hl, #(_skulltron + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_skulltron + 0x0002)
	ld	(hl), #0x0c
	ld	hl, #(_skulltron + 0x0003)
	ld	(hl), #0x0d
	ld	hl, #(_skulltron + 0x0004)
	ld	(hl), #0x0c
	ld	hl, #(_skulltron + 0x0005)
	ld	(hl), #0x0c
	ld	hl, #(_skulltron + 0x0006)
	ld	(hl), #0x0d
	ld	hl, #(_skulltron + 0x0007)
	ld	(hl), #0x0d
	ld	hl, #(_skulltron + 0x0008)
	ld	(hl), #0x0c
	ld	hl, #(_skulltron + 0x0009)
	ld	(hl), #0x00
	ld	hl, #(_skulltron + 0x000a)
	ld	(hl), #0x01
	ld	hl, #(_skulltron + 0x000b)
	ld	(hl), #0x00
	ld	hl, #(_skulltron + 0x000c)
	ld	(hl), #0x00
	ld	hl, #(_skulltron + 0x000d)
	ld	(hl), #0x00
	ld	hl, #(_skulltron + 0x000e)
	ld	(hl), #0x01
	ld	hl, #(_skulltron + 0x000f)
	ld	(hl), #0x04
	ld	hl, #(_skulltron + 0x0010)
	ld	(hl), #0x01
	ld	hl, #(_skulltron + 0x0011)
	ld	(hl), #0x00
	ld	hl, #(_skulltron + 0x0012)
	ld	(hl), #0x00
	ld	hl, #(_skulltron + 0x0013)
	ld	(hl), #0x2a
	ld	hl, #(_skulltron + 0x0014)
	ld	(hl), #0x32
	ld	hl, #(_skulltron + 0x0015)
	ld	(hl), #0x00
;src/game.c:41: skulltron_t skullperior = {
	ld	hl, #_skullperior
	ld	(hl), #0xc8
	ld	hl, #(_skullperior + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_skullperior + 0x0002)
	ld	(hl), #0x0e
	ld	hl, #(_skullperior + 0x0003)
	ld	(hl), #0x0f
	ld	hl, #(_skullperior + 0x0004)
	ld	(hl), #0x0e
	ld	hl, #(_skullperior + 0x0005)
	ld	(hl), #0x0e
	ld	hl, #(_skullperior + 0x0006)
	ld	(hl), #0x0f
	ld	hl, #(_skullperior + 0x0007)
	ld	(hl), #0x0f
	ld	hl, #(_skullperior + 0x0008)
	ld	(hl), #0x0e
	ld	hl, #(_skullperior + 0x0009)
	ld	(hl), #0x00
	ld	hl, #(_skullperior + 0x000a)
	ld	(hl), #0x02
	ld	hl, #(_skullperior + 0x000b)
	ld	(hl), #0x00
	ld	hl, #(_skullperior + 0x000c)
	ld	(hl), #0x00
	ld	hl, #(_skullperior + 0x000d)
	ld	(hl), #0x01
	ld	hl, #(_skullperior + 0x000e)
	ld	(hl), #0x01
	ld	hl, #(_skullperior + 0x000f)
	ld	(hl), #0x04
	ld	hl, #(_skullperior + 0x0010)
	ld	(hl), #0x01
	ld	hl, #(_skullperior + 0x0011)
	ld	(hl), #0x00
	ld	hl, #(_skullperior + 0x0012)
	ld	(hl), #0x00
	ld	hl, #(_skullperior + 0x0013)
	ld	(hl), #0x2c
	ld	hl, #(_skullperior + 0x0014)
	ld	(hl), #0x01
	ld	hl, #(_skullperior + 0x0015)
	ld	(hl), #0x32
;src/game.c:46: turret_t turret = {
	ld	hl, #_turret
	ld	(hl), #0xc8
	ld	hl, #(_turret + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_turret + 0x0002)
	ld	(hl), #0x10
	ld	hl, #(_turret + 0x0003)
	ld	(hl), #0x11
	ld	hl, #(_turret + 0x0004)
	ld	(hl), #0x11
	ld	hl, #(_turret + 0x0005)
	ld	(hl), #0x11
	ld	hl, #(_turret + 0x0006)
	ld	(hl), #0x10
	ld	hl, #(_turret + 0x0007)
	ld	(hl), #0x10
	ld	hl, #(_turret + 0x0008)
	ld	(hl), #0x10
	ld	hl, #(_turret + 0x0009)
	ld	(hl), #0x00
	ld	hl, #(_turret + 0x000a)
	ld	(hl), #0x01
	ld	hl, #(_turret + 0x000b)
	ld	(hl), #0x00
	ld	hl, #(_turret + 0x000c)
	ld	(hl), #0x00
	ld	hl, #(_turret + 0x000d)
	ld	(hl), #0x01
	ld	hl, #(_turret + 0x000e)
	ld	(hl), #0x01
	ld	hl, #(_turret + 0x000f)
	ld	(hl), #0x04
	ld	hl, #(_turret + 0x0010)
	ld	(hl), #0x01
	ld	hl, #(_turret + 0x0011)
	ld	(hl), #0x00
	ld	hl, #(_turret + 0x0012)
	ld	(hl), #0x00
	ld	hl, #(_turret + 0x0013)
	ld	(hl), #0x31
	ld	hl, #(_turret + 0x0014)
	ld	(hl), #0x32
;src/game.c:51: turret_t turret2 = {
	ld	hl, #_turret2
	ld	(hl), #0xc8
	ld	hl, #(_turret2 + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_turret2 + 0x0002)
	ld	(hl), #0x10
	ld	hl, #(_turret2 + 0x0003)
	ld	(hl), #0x11
	ld	hl, #(_turret2 + 0x0004)
	ld	(hl), #0x11
	ld	hl, #(_turret2 + 0x0005)
	ld	(hl), #0x11
	ld	hl, #(_turret2 + 0x0006)
	ld	(hl), #0x10
	ld	hl, #(_turret2 + 0x0007)
	ld	(hl), #0x10
	ld	hl, #(_turret2 + 0x0008)
	ld	(hl), #0x10
	ld	hl, #(_turret2 + 0x0009)
	ld	(hl), #0x00
	ld	hl, #(_turret2 + 0x000a)
	ld	(hl), #0x01
	ld	hl, #(_turret2 + 0x000b)
	ld	(hl), #0x00
	ld	hl, #(_turret2 + 0x000c)
	ld	(hl), #0x00
	ld	hl, #(_turret2 + 0x000d)
	ld	(hl), #0x01
	ld	hl, #(_turret2 + 0x000e)
	ld	(hl), #0x01
	ld	hl, #(_turret2 + 0x000f)
	ld	(hl), #0x04
	ld	hl, #(_turret2 + 0x0010)
	ld	(hl), #0x01
	ld	hl, #(_turret2 + 0x0011)
	ld	(hl), #0x01
	ld	hl, #(_turret2 + 0x0012)
	ld	(hl), #0x00
	ld	hl, #(_turret2 + 0x0013)
	ld	(hl), #0x18
	ld	hl, #(_turret2 + 0x0014)
	ld	(hl), #0x32
;src/game.c:56: heatball_t heatball = {
	ld	hl, #_heatball
	ld	(hl), #0xc8
	ld	hl, #(_heatball + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_heatball + 0x0002)
	ld	(hl), #0x12
	ld	hl, #(_heatball + 0x0003)
	ld	(hl), #0x13
	ld	hl, #(_heatball + 0x0004)
	ld	(hl), #0x12
	ld	hl, #(_heatball + 0x0005)
	ld	(hl), #0x13
	ld	hl, #(_heatball + 0x0006)
	ld	(hl), #0x12
	ld	hl, #(_heatball + 0x0007)
	ld	(hl), #0x13
	ld	hl, #(_heatball + 0x0008)
	ld	(hl), #0x12
	ld	hl, #(_heatball + 0x0009)
	ld	(hl), #0x00
	ld	hl, #(_heatball + 0x000a)
	ld	(hl), #0x02
	ld	hl, #(_heatball + 0x000b)
	ld	(hl), #0x00
	ld	hl, #(_heatball + 0x000c)
	ld	(hl), #0x00
	ld	hl, #(_heatball + 0x000d)
	ld	(hl), #0x00
	ld	hl, #(_heatball + 0x000e)
	ld	(hl), #0x01
	ld	hl, #(_heatball + 0x000f)
	ld	(hl), #0x04
	ld	hl, #(_heatball + 0x0010)
	ld	(hl), #0x01
	ld	hl, #(_heatball + 0x0011)
	ld	(hl), #0x32
;src/game.c:61: spike_t spike = {
	ld	hl, #_spike
	ld	(hl), #0xc8
	ld	hl, #(_spike + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_spike + 0x0002)
	ld	(hl), #0x14
	ld	hl, #(_spike + 0x0003)
	ld	(hl), #0x15
	ld	hl, #(_spike + 0x0004)
	ld	(hl), #0x14
	ld	hl, #(_spike + 0x0005)
	ld	(hl), #0x15
	ld	hl, #(_spike + 0x0006)
	ld	(hl), #0x14
	ld	hl, #(_spike + 0x0007)
	ld	(hl), #0x15
	ld	hl, #(_spike + 0x0008)
	ld	(hl), #0x14
	ld	hl, #(_spike + 0x0009)
	ld	(hl), #0x00
	ld	hl, #(_spike + 0x000a)
	ld	(hl), #0x02
	ld	hl, #(_spike + 0x000b)
	ld	(hl), #0x00
	ld	hl, #(_spike + 0x000c)
	ld	(hl), #0x00
	ld	hl, #(_spike + 0x000d)
	ld	(hl), #0x00
	ld	hl, #(_spike + 0x000e)
	ld	(hl), #0x01
	ld	hl, #(_spike + 0x000f)
	ld	(hl), #0x04
	ld	hl, #(_spike + 0x0010)
	ld	(hl), #0x01
	ld	hl, #(_spike + 0x0011)
	ld	(hl), #0x32
;src/game.c:66: item_t item = {{200, 200}, i_disabled, S_NOTHING};
	ld	hl, #_item
	ld	(hl), #0xc8
	ld	hl, #(_item + 0x0001)
	ld	(hl), #0xc8
	ld	hl, #(_item + 0x0002)
	ld	(hl), #0x00
	ld	hl, #(_item + 0x0003)
	ld	(hl), #0x1f
;src/game.c:69: UINT8 beam_count = 0, global_beam_spd = 4, b=0, _b=0, rand_seed_factor, rand_beam_x;
	ld	hl, #_beam_count
	ld	(hl), #0x00
;src/game.c:69: beam_t beams[7];
	ld	hl, #_global_beam_spd
	ld	(hl), #0x04
;src/game.c:69: UINT8 beam_count = 0, global_beam_spd = 4, b=0, _b=0, rand_seed_factor, rand_beam_x;
	ld	hl, #_b
	ld	(hl), #0x00
;src/game.c:69: beam_t beams[7];
	ld	hl, #__b
	ld	(hl), #0x00
;src/game.c:190: BOOL already_used = false;
	ld	hl, #_already_used
	ld	(hl), #0x00
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/game.c:71: void game_start() {
;	---------------------------------
; Function game_start
; ---------------------------------
_game_start::
;src/game.c:73: NR52_REG = 0x80;
	ld	a, #0x80
	ldh	(_NR52_REG+0),a
;src/game.c:74: NR50_REG = 0x50;
	ld	a, #0x50
	ldh	(_NR50_REG+0),a
;src/game.c:75: NR51_REG = 0xFF;
	ld	a, #0xff
	ldh	(_NR51_REG+0),a
;src/game.c:78: set_sprite_data(0, 30, sprites);
	ld	hl, #_sprites
	push	hl
	ld	a, #0x1e
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_set_sprite_data
	add	sp, #4
;src/game.c:79: player_spawn(&player);
	ld	hl, #_player
	push	hl
	call	_player_spawn
	add	sp, #2
;src/game.c:82: coin_spawn(&coin, &player.pos);
	ld	hl, #_player
	push	hl
	ld	hl, #_coin
	push	hl
	call	_coin_spawn
	add	sp, #4
;src/game.c:85: hud_start(player.score, player.ammo, player.lives);
	ld	hl, #(_player + 0x0011)
	ld	c, (hl)
	ld	hl, #(_player + 0x0013)
	ld	b, (hl)
	ld	de, #_player + 18
	ld	a, (de)
	ld	h, c
	ld	l, b
	push	hl
	push	af
	inc	sp
	call	_hud_start
	add	sp, #3
;src/game.c:88: player_bullet.disabled = true;
	ld	hl, #(_player_bullet + 0x0005)
	ld	(hl), #0x01
;src/game.c:91: game_new_beam();
	call	_game_new_beam
;src/game.c:94: game_next_enemy();
	call	_game_next_enemy
;src/game.c:97: set_bkg_data(44, 70, tiles);
	ld	hl, #_tiles
	push	hl
	ld	de, #0x462c
	push	de
	call	_set_bkg_data
	add	sp, #4
;src/game.c:98: set_bkg_tiles(0, 1, 20, 17, level);
	ld	hl, #_level
	push	hl
	ld	de, #0x1114
	push	de
	ld	a, #0x01
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_set_bkg_tiles
	add	sp, #6
;src/game.c:100: SHOW_SPRITES;
	ldh	a, (_LCDC_REG+0)
	or	a, #0x02
	ldh	(_LCDC_REG+0),a
;src/game.c:101: SHOW_BKG;
	ldh	a, (_LCDC_REG+0)
	or	a, #0x01
	ldh	(_LCDC_REG+0),a
;src/game.c:102: }
	ret
_fps:
	.db #0x3c	; 60
;src/game.c:105: void game_input() {
;	---------------------------------
; Function game_input
; ---------------------------------
_game_input::
;src/game.c:106: player_input(&player, &player_bullet);
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_player
	push	hl
	call	_player_input
	add	sp, #4
;src/game.c:107: }
	ret
;src/game.c:110: void game_update() {
;	---------------------------------
; Function game_update
; ---------------------------------
_game_update::
;src/game.c:114: &heatball, &spike);
;src/game.c:113: &skullperior, &enemy_bullet, &turret, &turret2,
;src/game.c:112: &item, &drone, &splitter, &skulltron,
;src/game.c:111: player_update(&player, &coin, &player_bullet, beam_count,
	ld	hl, #_spike
	push	hl
	ld	hl, #_heatball
	push	hl
	ld	hl, #_turret2
	push	hl
	ld	hl, #_turret
	push	hl
	ld	hl, #_enemy_bullet
	push	hl
	ld	hl, #_skullperior
	push	hl
	ld	hl, #_skulltron
	push	hl
	ld	hl, #_splitter
	push	hl
	ld	hl, #_drone
	push	hl
	ld	hl, #_item
	push	hl
	ld	a, (#_beam_count)
	push	af
	inc	sp
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_coin
	push	hl
	ld	hl, #_player
	push	hl
	call	_player_update
	add	sp, #27
;src/game.c:116: if (!player_bullet.disabled)
	ld	a, (#(_player_bullet + 0x0005) + 0)
	or	a, a
	jr	NZ, 00102$
;src/game.c:117: bullet_update(&player_bullet);
	ld	hl, #_player_bullet
	push	hl
	call	_bullet_update
	add	sp, #2
00102$:
;src/game.c:118: if (!enemy_bullet.disabled)
	ld	a, (#(_enemy_bullet + 0x0005) + 0)
	or	a, a
	jr	NZ, 00104$
;src/game.c:119: bullet_update(&enemy_bullet);
	ld	hl, #_enemy_bullet
	push	hl
	call	_bullet_update
	add	sp, #2
00104$:
;src/game.c:121: if (drone.pos.x != 200) drone_update(&drone, &coin, &player_bullet);
	ld	a, (#_drone + 0)
	sub	a, #0xc8
	jr	Z, 00106$
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_coin
	push	hl
	ld	hl, #_drone
	push	hl
	call	_drone_update
	add	sp, #6
00106$:
;src/game.c:122: if (splitter.pos.x != 200) splitter_update(&splitter, &coin, &player_bullet);
	ld	a, (#_splitter + 0)
	sub	a, #0xc8
	jr	Z, 00108$
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_coin
	push	hl
	ld	hl, #_splitter
	push	hl
	call	_splitter_update
	add	sp, #6
00108$:
;src/game.c:123: if (skulltron.pos.x != 200) skulltron_update(&skulltron, &player_bullet, &player.pos, &enemy_bullet);
	ld	a, (#_skulltron + 0)
	sub	a, #0xc8
	jr	Z, 00110$
	ld	hl, #_enemy_bullet
	push	hl
	ld	hl, #_player
	push	hl
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_skulltron
	push	hl
	call	_skulltron_update
	add	sp, #8
00110$:
;src/game.c:124: if (skullperior.pos.x != 200) skulltron_update(&skullperior, &player_bullet, &player.pos, &enemy_bullet);
	ld	a, (#_skullperior + 0)
	sub	a, #0xc8
	jr	Z, 00112$
	ld	hl, #_enemy_bullet
	push	hl
	ld	hl, #_player
	push	hl
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_skullperior
	push	hl
	call	_skulltron_update
	add	sp, #8
00112$:
;src/game.c:125: if (turret.pos.x != 200) turret_update(&turret, &player_bullet, &player.pos, &enemy_bullet);
	ld	a, (#_turret + 0)
	sub	a, #0xc8
	jr	Z, 00114$
	ld	hl, #_enemy_bullet
	push	hl
	ld	hl, #_player
	push	hl
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_turret
	push	hl
	call	_turret_update
	add	sp, #8
00114$:
;src/game.c:126: if (turret2.pos.x != 200) turret_update(&turret2, &player_bullet, &player.pos, &enemy_bullet);
	ld	a, (#_turret2 + 0)
	sub	a, #0xc8
	jr	Z, 00116$
	ld	hl, #_enemy_bullet
	push	hl
	ld	hl, #_player
	push	hl
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_turret2
	push	hl
	call	_turret_update
	add	sp, #8
00116$:
;src/game.c:127: if (heatball.pos.x != 200) heatball_update(&heatball);
	ld	a, (#_heatball + 0)
	sub	a, #0xc8
	jr	Z, 00118$
	ld	hl, #_heatball
	push	hl
	call	_heatball_update
	add	sp, #2
00118$:
;src/game.c:128: if (spike.pos.x != 200) spike_update(&spike, &player_bullet);
	ld	a, (#_spike + 0)
	sub	a, #0xc8
	jr	Z, 00120$
	ld	hl, #_player_bullet
	push	hl
	ld	hl, #_spike
	push	hl
	call	_spike_update
	add	sp, #4
00120$:
;src/game.c:129: if (item.type != i_disabled) item_update(&item);
	ld	a, (#(_item + 0x0002) + 0)
	or	a, a
	jr	Z, 00122$
	ld	hl, #_item
	push	hl
	call	_item_update
	add	sp, #2
00122$:
;src/game.c:130: for (b=0; b < beam_count; b++)
	ld	hl, #_b
	ld	(hl), #0x00
00125$:
	ld	a, (#_b)
	ld	hl, #_beam_count
	sub	a, (hl)
	ret	NC
;src/game.c:131: beam_update(&beams[b], &player, &drone, &splitter, &skulltron, &skullperior, &turret, &turret2, &heatball, &spike);
	ld	bc, #_player
	ld	hl, #_b
	ld	e, (hl)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	ld	de, #_beams
	add	hl, de
	ld	de, #_spike
	push	de
	ld	de, #_heatball
	push	de
	ld	de, #_turret2
	push	de
	ld	de, #_turret
	push	de
	ld	de, #_skullperior
	push	de
	ld	de, #_skulltron
	push	de
	ld	de, #_splitter
	push	de
	ld	de, #_drone
	push	de
	push	bc
	push	hl
	call	_beam_update
	add	sp, #20
;src/game.c:130: for (b=0; b < beam_count; b++)
	ld	hl, #_b
	inc	(hl)
;src/game.c:132: }
	jr	00125$
;src/game.c:139: void game_next_enemy() {
;	---------------------------------
; Function game_next_enemy
; ---------------------------------
_game_next_enemy::
;src/game.c:140: switch(last_enemy) {
	ld	a, #0x07
	ld	hl, #_last_enemy
	sub	a, (hl)
	ret	C
	ld	c, (hl)
	ld	b, #0x00
	ld	hl, #00133$
	add	hl, bc
	add	hl, bc
	add	hl, bc
	jp	(hl)
00133$:
	jp	00103$
	jp	00106$
	jp	00104$
	jp	00110$
	jp	00105$
	jp	00111$
	jp	00102$
	jp	00102$
;src/game.c:142: case E_HEATBALL_SPIKE:
00102$:
;src/game.c:143: drone_spawn(&drone, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_drone
	push	hl
	call	_drone_spawn
	add	sp, #4
;src/game.c:144: last_enemy = E_DRONE;
	ld	hl, #_last_enemy
	ld	(hl), #0x00
;src/game.c:145: break;
	ret
;src/game.c:146: case E_DRONE:
00103$:
;src/game.c:147: turret_spawn(&turret, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_turret
	push	hl
	call	_turret_spawn
	add	sp, #4
;src/game.c:148: last_enemy = E_TURRET;
	ld	hl, #_last_enemy
	ld	(hl), #0x02
;src/game.c:149: break;
	ret
;src/game.c:150: case E_TURRET:
00104$:
;src/game.c:151: skulltron_spawn(&skulltron, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_skulltron
	push	hl
	call	_skulltron_spawn
	add	sp, #4
;src/game.c:152: last_enemy = E_SKULLTRON;
	ld	hl, #_last_enemy
	ld	(hl), #0x04
;src/game.c:153: break;
	ret
;src/game.c:154: case E_SKULLTRON:
00105$:
;src/game.c:155: splitter_spawn(&splitter, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_splitter
	push	hl
	call	_splitter_spawn
	add	sp, #4
;src/game.c:156: last_enemy = E_SPLITTER;
	ld	hl, #_last_enemy
	ld	(hl), #0x01
;src/game.c:157: break;
	ret
;src/game.c:158: case E_SPLITTER:
00106$:
;src/game.c:159: if (player.score > 10) {
	ld	hl, #(_player + 0x0012)
	ld	c, (hl)
	ld	a, #0x0a
	sub	a, c
	jr	NC, 00108$
;src/game.c:160: turret_spawn(&turret2, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_turret2
	push	hl
	call	_turret_spawn
	add	sp, #4
;src/game.c:161: last_enemy = E_TURRET2;
	ld	hl, #_last_enemy
	ld	(hl), #0x03
	ret
00108$:
;src/game.c:163: heatball_spawn(&heatball, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_heatball
	push	hl
	call	_heatball_spawn
	add	sp, #4
;src/game.c:164: last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;src/game.c:166: break;
	ret
;src/game.c:167: case E_TURRET2:
00110$:
;src/game.c:168: skulltron_spawn(&skullperior, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_skullperior
	push	hl
	call	_skulltron_spawn
	add	sp, #4
;src/game.c:169: last_enemy = E_SKULLPERIOR;
	ld	hl, #_last_enemy
	ld	(hl), #0x05
;src/game.c:170: break;
	ret
;src/game.c:171: case E_SKULLPERIOR:
00111$:
;src/game.c:172: if (player.score < 120) {
	ld	a, (#(_player + 0x0012) + 0)
	sub	a, #0x78
	jr	NC, 00113$
;src/game.c:173: heatball_spawn(&heatball, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_heatball
	push	hl
	call	_heatball_spawn
	add	sp, #4
;src/game.c:174: spike_spawn(&spike, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_spike
	push	hl
	call	_spike_spawn
	add	sp, #4
;src/game.c:175: last_enemy = E_HEATBALL_SPIKE;
	ld	hl, #_last_enemy
	ld	(hl), #0x07
	ret
00113$:
;src/game.c:177: skulltron_spawn(&skullperior, &coin);
	ld	hl, #_coin
	push	hl
	ld	hl, #_skullperior
	push	hl
	call	_skulltron_spawn
	add	sp, #4
;src/game.c:178: last_enemy = E_SKULLPERIOR;
	ld	hl, #_last_enemy
	ld	(hl), #0x05
;src/game.c:181: }
;src/game.c:182: }
	ret
;src/game.c:185: void game_spawn_item() {
;	---------------------------------
; Function game_spawn_item
; ---------------------------------
_game_spawn_item::
;src/game.c:186: if (player.score == 0 || player.score == 1 || is_prime(player.score)) return;
	ld	a, (#(_player + 0x0012) + 0)
	or	a, a
	ret	Z
	cp	a, #0x01
	ret	Z
	push	af
	inc	sp
	call	_is_prime
	inc	sp
	ld	a, e
	or	a, a
	ret	NZ
;src/game.c:187: item_spawn(&item);
	ld	hl, #_item
	push	hl
	call	_item_spawn
	add	sp, #2
;src/game.c:188: }
	ret
;src/game.c:191: void game_new_beam() {
;	---------------------------------
; Function game_new_beam
; ---------------------------------
_game_new_beam::
	add	sp, #-4
;src/game.c:192: rand_seed_factor = (global_frame % 2 != 0 ? 1 : 2);
	ld	hl, #_global_frame
	bit	0, (hl)
	jr	Z, 00115$
	ld	bc, #0x0001
	jr	00116$
00115$:
	ld	bc, #0x0002
00116$:
	ld	hl, #_rand_seed_factor
	ld	(hl), c
;src/game.c:193: seed = DIV_REG;
	ldh	a, (_DIV_REG+0)
	ld	hl, #_seed
	ld	(hl), a
;src/game.c:194: seed |= (UINT16)DIV_REG << 8;
	ldh	a, (_DIV_REG+0)
;src/game.c:195: initarand(seed + rand_menukey + (rand_menuframe * rand_seed_factor) * (global_frame * rand_seed_factor) * beam_count);
	ld	c, (hl)
	ld	b, #0x00
	ld	hl, #_rand_menukey
	ld	e, (hl)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
	ld	a, (#_rand_seed_factor)
	push	af
	inc	sp
	ld	a, (#_rand_menuframe)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	push	de
	ld	a, (#_rand_seed_factor)
	push	af
	inc	sp
	ld	a, (#_global_frame)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	pop	bc
	push	de
	push	bc
	call	__mulint
	add	sp, #4
	ld	hl, #_beam_count
	ld	c, (hl)
	ld	b, #0x00
	push	bc
	push	de
	call	__mulint
	add	sp, #4
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	push	hl
	call	_initarand
	add	sp, #2
;src/game.c:196: do {
00107$:
;src/game.c:197: rand_beam_x = abs(arand());
	call	_arand
	ld	a, e
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	hl, #_rand_beam_x
	ld	(hl), e
;src/game.c:198: for(b=0; b < beam_count; b++)
	ld	hl, #_b
	ld	(hl), #0x00
00111$:
;src/game.c:199: already_used = (rand_beam_x >= (beams[b].pos.x - 16) && rand_beam_x <= (beams[b].pos.x + 16) ? true : false);
	ld	a, (#_rand_beam_x)
	ldhl	sp,	#0
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
;src/game.c:198: for(b=0; b < beam_count; b++)
	ld	a, (#_b)
	ld	hl, #_beam_count
	sub	a, (hl)
	jr	NC, 00108$
;src/game.c:199: already_used = (rand_beam_x >= (beams[b].pos.x - 16) && rand_beam_x <= (beams[b].pos.x + 16) ? true : false);
	ld	hl, #_b
	ld	c, (hl)
	ld	b, #0x00
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, hl
	ld	de, #_beams
	add	hl, de
	ld	c, (hl)
	ld	b, #0x00
	ld	de, #0x0010
	ld	a, c
	sub	a, e
	ld	e, a
	ld	a, b
	sbc	a, d
	ldhl	sp,	#3
	ld	(hl-), a
	ld	(hl), e
	ldhl	sp,	#0
	ld	e, l
	ld	d, h
	ldhl	sp,	#2
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00182$
	bit	7, d
	jr	NZ, 00183$
	cp	a, a
	jr	00183$
00182$:
	bit	7, d
	jr	Z, 00183$
	scf
00183$:
	jr	C, 00117$
	ld	hl, #0x0010
	add	hl, bc
	ld	c, l
	ld	b, h
	ldhl	sp,	#0
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00184$
	bit	7, d
	jr	NZ, 00185$
	cp	a, a
	jr	00185$
00184$:
	bit	7, d
	jr	Z, 00185$
	scf
00185$:
	jr	C, 00117$
	ld	bc, #0x0001
	jr	00118$
00117$:
	ld	bc, #0x0000
00118$:
	ld	hl, #_already_used
	ld	(hl), c
;src/game.c:198: for(b=0; b < beam_count; b++)
	ld	hl, #_b
	inc	(hl)
	jp	00111$
00108$:
;src/game.c:200: } while(rand_beam_x < 36 ||
	ld	hl, #_rand_beam_x
	ld	a, (hl)
	sub	a, #0x24
	jp	C, 00107$
;src/game.c:201: rand_beam_x > (SCREENWIDTH - 40) ||
	ld	a, #0x78
	sub	a, (hl)
	jp	C, 00107$
;src/game.c:202: rand_beam_x % 2 != 0 || rand_beam_x == player.pos.x ||
	ldhl	sp,	#0
	bit	0, (hl)
	jp	NZ,00107$
	ld	hl, #_player
	ld	c, (hl)
	ld	a, (#_rand_beam_x)
	sub	a, c
	jp	Z,00107$
;src/game.c:203: rand_beam_x == player.check_point.x || already_used);
	ld	hl, #(_player + 0x0002)
	ld	c, (hl)
	ld	a, (#_rand_beam_x)
	sub	a, c
	jp	Z,00107$
	ld	a, (#_already_used)
	or	a, a
	jp	NZ, 00107$
;src/game.c:206: beams[beam_count].pos.x = rand_beam_x;
	ld	hl, #_beam_count
	ld	c, (hl)
	ld	b, #0x00
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, hl
	ld	a, l
	add	a, #<(_beams)
	ld	c, a
	ld	a, h
	adc	a, #>(_beams)
	ld	b, a
	ld	a, (#_rand_beam_x)
	ld	(bc), a
;src/game.c:207: beams[beam_count].pos.y = 24;
	ld	hl, #_beam_count
	ld	c, (hl)
	ld	b, #0x00
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, hl
	ld	de, #_beams
	add	hl, de
	inc	hl
	ld	(hl), #0x18
;src/game.c:208: beams[beam_count].spd = 1;
	ld	hl, #_beam_count
	ld	c, (hl)
	ld	b, #0x00
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, hl
	ld	de, #_beams
	add	hl, de
	inc	hl
	inc	hl
	inc	hl
	ld	(hl), #0x01
;src/game.c:209: beams[beam_count].sprite = COPY_BEAM + beam_count;
	ld	hl, #_beam_count
	ld	c, (hl)
	ld	b, #0x00
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, hl
	ld	de, #_beams
	add	hl, de
	inc	hl
	inc	hl
	ld	c, l
	ld	b, h
	ld	hl, #_beam_count
	ld	a, (hl)
	add	a, #0x20
	ld	(bc), a
;src/game.c:210: beam_spawn(&beams[beam_count]);
	ld	c, (hl)
	ld	b, #0x00
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, hl
	ld	de, #_beams
	add	hl, de
	push	hl
	call	_beam_spawn
	add	sp, #2
;src/game.c:212: beam_count++;
	ld	hl, #_beam_count
	inc	(hl)
;src/game.c:213: }
	add	sp, #4
	ret
;src/game.c:215: void game_beam_speed_medium() {
;	---------------------------------
; Function game_beam_speed_medium
; ---------------------------------
_game_beam_speed_medium::
;src/game.c:216: for(_b=0; _b < beam_count; _b++)
	ld	hl, #__b
	ld	(hl), #0x00
	ld	bc, #_beams+0
00103$:
	ld	a, (#__b)
	ld	hl, #_beam_count
	sub	a, (hl)
	ret	NC
;src/game.c:217: beams[_b].spd = 2;
	ld	hl, #__b
	ld	e, (hl)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, bc
	inc	hl
	inc	hl
	inc	hl
	ld	(hl), #0x02
;src/game.c:216: for(_b=0; _b < beam_count; _b++)
	ld	hl, #__b
	inc	(hl)
;src/game.c:218: }
	jr	00103$
;src/game.c:220: void game_beam_speed_hard() {
;	---------------------------------
; Function game_beam_speed_hard
; ---------------------------------
_game_beam_speed_hard::
;src/game.c:221: for(_b=0; _b < beam_count; _b++)
	ld	hl, #__b
	ld	(hl), #0x00
	ld	bc, #_beams+0
00103$:
	ld	a, (#__b)
	ld	hl, #_beam_count
	sub	a, (hl)
	ret	NC
;src/game.c:222: beams[_b].spd = 4;
	ld	hl, #__b
	ld	e, (hl)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, bc
	inc	hl
	inc	hl
	inc	hl
	ld	(hl), #0x04
;src/game.c:221: for(_b=0; _b < beam_count; _b++)
	ld	hl, #__b
	inc	(hl)
;src/game.c:223: }
	jr	00103$
	.area _CODE
	.area _CABS (ABS)
