#include <gb/gb.h>
#include <gb/drawing.h>
#include <rand.h>
#include "main.h"
#include "player.h"
#include "sprites.h"
#include "coin.h"
#include "bullet.h"
#include "drone.h"
#include "splitter.h"
#include "skulltron.h"
#include "turret.h"
#include "heatball.h"
#include "spike.h"
#include "hud.h"
#include "tiles.h"
#include "level.h"
#include "beam.h"
#include "item.h"
#include "math.h"
#include "game.h"

player_t player = {{16, 78}, {16, 78}, {{0, 7, 0, 3, 4, 7, 4}, fa_right, 2, false, false, true, false}, 3, 0, 3, true, false, 0};
coin_t coin = {{0, 0}};
// when an enemy is at position (200, 200) and it's dead with dead_timer = 50, the game processes it as an invisible, inactive enemy.
drone_t drone = {
    {200, 200},
    {{S_DRONE, S_DRONE+1, S_DRONE, S_DRONE+1, S_DRONE, S_DRONE+1, S_DRONE}, fa_left, 1, false, false, false, true},
    d_nodir, true, 50
};
splitter_t splitter = {
    {200, 200},
    {{S_SPLITTER, S_SPLITTER+1, S_SPLITTER, S_SPLITTER+1, S_SPLITTER, S_SPLITTER+1, S_SPLITTER},
    fa_left, 2, false, false, false, true}, d_nodir, true, 50
};
skulltron_t skulltron = {
    {200, 200}, 
    {{S_SKULLTRON, S_SKULLTRON+1, S_SKULLTRON, S_SKULLTRON, S_SKULLTRON+1, S_SKULLTRON+1, S_SKULLTRON},
    fa_left, 1, false, false, false, true}, d_nodir, true, false, 0, 42, 50, false
};
skulltron_t skullperior = {
    {200, 200},
    {{S_SKULLPERIOR, S_SKULLPERIOR+1, S_SKULLPERIOR, S_SKULLPERIOR, S_SKULLPERIOR+1, S_SKULLPERIOR+1, S_SKULLPERIOR},
    fa_left, 2, false, false, true, true}, d_nodir, true, false, 0, 44, true, 50, true
};
turret_t turret = {
    {200, 200},
    {{S_TURRET, S_TURRET+1, S_TURRET+1, S_TURRET+1, S_TURRET, S_TURRET, S_TURRET},
    fa_left, 1, false, false, true, true}, d_nodir, true, false, 0, 49, 50
};
turret_t turret2 = {
    {200, 200},
    {{S_TURRET, S_TURRET+1, S_TURRET+1, S_TURRET+1, S_TURRET, S_TURRET, S_TURRET},
    fa_left, 1, false, false, true, true}, d_nodir, true, true, 0, 24, 50
};
heatball_t heatball = {
    {200, 200},
    {{S_HEATBALL, S_HEATBALL+1, S_HEATBALL, S_HEATBALL+1, S_HEATBALL, S_HEATBALL+1, S_HEATBALL},
    fa_left, 2, false, false, false, true}, d_nodir, true, 50
};
spike_t spike = {
    {200, 200},
    {{S_SPIKE, S_SPIKE+1, S_SPIKE, S_SPIKE+1, S_SPIKE, S_SPIKE+1, S_SPIKE},
    fa_left, 2, false, false, false, true}, d_nodir, true, 50
};
item_t item = {{200, 200}, i_disabled, S_NOTHING};
bullet_t player_bullet;
bullet_t enemy_bullet;
UINT8 beam_count = 0, global_beam_spd = 4, b=0, _b=0, rand_seed_factor, rand_beam_x;
beam_t beams[7];
void game_start() {
    // enable sound
    NR52_REG = 0x80;
    NR50_REG = 0x50;
    NR51_REG = 0xFF;

    // initialize player and sprites
    set_sprite_data(0, 30, sprites);
    player_spawn(&player);

    // spawn the coin
    coin_spawn(&coin, &player.pos);

    // initialize the HUD
    hud_start(player.score, player.ammo, player.lives);

    // disable the bullet
    player_bullet.disabled = true;

    // spawn the first beam
    game_new_beam();

    // spawn the enemy
    game_next_enemy();

    //initialize walls
    set_bkg_data(44, 70, tiles);
    set_bkg_tiles(0, 1, 20, 17, level);

    SHOW_SPRITES;
    SHOW_BKG;
}

// in case you don't know it yet, this function is executed every frame :3
void game_input() {
    player_input(&player, &player_bullet);
}

// in case you don't know it yet, this function is executed every frame :3    -- yes, i copied the same comment twice. don't hate me ;w;
void game_update() {
    player_update(&player, &coin, &player_bullet, beam_count,
        &item, &drone, &splitter, &skulltron,
        &skullperior, &enemy_bullet, &turret, &turret2,
        &heatball, &spike);

    if (!player_bullet.disabled)
        bullet_update(&player_bullet);
    if (!enemy_bullet.disabled)
        bullet_update(&enemy_bullet);

    if (drone.pos.x != 200) drone_update(&drone, &coin, &player_bullet);
    if (splitter.pos.x != 200) splitter_update(&splitter, &coin, &player_bullet);
    if (skulltron.pos.x != 200) skulltron_update(&skulltron, &player_bullet, &player.pos, &enemy_bullet);
    if (skullperior.pos.x != 200) skulltron_update(&skullperior, &player_bullet, &player.pos, &enemy_bullet);
    if (turret.pos.x != 200) turret_update(&turret, &player_bullet, &player.pos, &enemy_bullet);
    if (turret2.pos.x != 200) turret_update(&turret2, &player_bullet, &player.pos, &enemy_bullet);
    if (heatball.pos.x != 200) heatball_update(&heatball);
    if (spike.pos.x != 200) spike_update(&spike, &player_bullet);
    if (item.type != i_disabled) item_update(&item);
    for (b=0; b < beam_count; b++)
        beam_update(&beams[b], &player, &drone, &splitter, &skulltron, &skullperior, &turret, &turret2, &heatball, &spike);
}


// pattern (levels 0 - 4, too easy): drone -> turret -> skulltron -> splitter -> heatball
// pattern (levels 5 - 10, easy): drone -> turret -> skulltron -> splitter -> turret2 -> heatball + spike
// pattern (levels 11 - 119, medium): drone -> turret -> skulltron -> splitter -> turret2 -> skullperior -> heatball + spike
// pattern (levels 120 - 126, hard): skullperior -> heatball + spike
void game_next_enemy() {
    switch(last_enemy) {
        case E_HEATBALL:
        case E_HEATBALL_SPIKE:
            drone_spawn(&drone, &coin);
            last_enemy = E_DRONE;
            break;
        case E_DRONE:
            turret_spawn(&turret, &coin);
            last_enemy = E_TURRET;
            break;
        case E_TURRET:
            skulltron_spawn(&skulltron, &coin);
            last_enemy = E_SKULLTRON;
            break;
        case E_SKULLTRON:
            splitter_spawn(&splitter, &coin);
            last_enemy = E_SPLITTER;
            break;
        case E_SPLITTER:
            if (player.score > 10) {
                turret_spawn(&turret2, &coin);
                last_enemy = E_TURRET2;
            } else {
                heatball_spawn(&heatball, &coin);
                last_enemy = E_HEATBALL;
            }
            break;
        case E_TURRET2:
            skulltron_spawn(&skullperior, &coin);
            last_enemy = E_SKULLPERIOR;
            break;
        case E_SKULLPERIOR:
            if (player.score < 120) {
                heatball_spawn(&heatball, &coin);
                spike_spawn(&spike, &coin);
                last_enemy = E_HEATBALL_SPIKE;
            } else {
                skulltron_spawn(&skullperior, &coin);
                last_enemy = E_SKULLPERIOR;
            }
            break;
    }
}

// if player score is either zero, one or a prime number, the item won't spawn. Otherwise item spawning chance is 20%
void game_spawn_item() {
    if (player.score == 0 || player.score == 1 || is_prime(player.score)) return;
    item_spawn(&item);
}

BOOL already_used = false;
void game_new_beam() {
    rand_seed_factor = (global_frame % 2 != 0 ? 1 : 2);
    seed = DIV_REG;
    seed |= (UINT16)DIV_REG << 8;
    initarand(seed + rand_menukey + (rand_menuframe * rand_seed_factor) * (global_frame * rand_seed_factor) * beam_count);
    do {
        rand_beam_x = abs(arand());
        for(b=0; b < beam_count; b++)
            already_used = (rand_beam_x >= (beams[b].pos.x - 16) && rand_beam_x <= (beams[b].pos.x + 16) ? true : false);
    } while(rand_beam_x < 36 ||
        rand_beam_x > (SCREENWIDTH - 40) ||
        rand_beam_x % 2 != 0 || rand_beam_x == player.pos.x ||
        rand_beam_x == player.check_point.x || already_used);
        

    beams[beam_count].pos.x = rand_beam_x;
    beams[beam_count].pos.y = 24;
    beams[beam_count].spd = 1;
    beams[beam_count].sprite = COPY_BEAM + beam_count;
    beam_spawn(&beams[beam_count]);

    beam_count++;
}

void game_beam_speed_medium() {
    for(_b=0; _b < beam_count; _b++)
        beams[_b].spd = 2;
}

void game_beam_speed_hard() {
    for(_b=0; _b < beam_count; _b++)
        beams[_b].spd = 4;
}
