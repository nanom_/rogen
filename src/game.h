#include <gb/gb.h>
#include "main.h"
#ifndef _GAME_H_
#define _GAME_H_

static const UINT8 fps = 60;
static BOOL paused = false;
extern UINT8 global_frame, seed, rand_menukey, rand_menuframe;

typedef enum {E_DRONE, E_SPLITTER, E_TURRET, E_TURRET2, E_SKULLTRON, E_SKULLPERIOR, E_HEATBALL, E_HEATBALL_SPIKE} enemy_t;
static enemy_t last_enemy = E_HEATBALL;

void game_start(void);
void game_input(void);
void game_update(void);
void game_next_enemy(void);
void game_spawn_item(void);
void game_new_beam(void);
void game_beam_speed_medium(void);
void game_beam_speed_hard(void);

#endif
