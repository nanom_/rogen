;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module heatball
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _heatball_spawn
	.globl _heatball_update
	.globl _heatball_check_collisions
	.globl _heatball_nextframe
	.globl _heatball_kill
	.globl _heatball_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/heatball.c:8: void heatball_spawn(heatball_t *heatball, coin_t *coin) {
;	---------------------------------
; Function heatball_spawn
; ---------------------------------
_heatball_spawn::
	add	sp, #-4
;src/heatball.c:9: heatball->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (160 - 16));
	ldhl	sp,#6
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	inc	sp
	inc	sp
	push	bc
	inc	hl
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#2
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00103$
	ld	de, #0x0010
	jr	00104$
00103$:
	ld	de, #0xff90
00104$:
	ld	a, e
	pop	hl
	push	hl
	ld	(hl), a
;src/heatball.c:10: heatball->pos.y = 32;
	ld	l, c
	ld	h, b
	inc	hl
	ld	(hl), #0x20
;src/heatball.c:11: heatball->entity.is_dead = false;
	ld	hl, #0x000e
	add	hl, bc
	ld	(hl), #0x00
;src/heatball.c:12: heatball->destroy_timer = 0;
	ld	hl, #0x0011
	add	hl, bc
	ld	(hl), #0x00
;src/heatball.c:13: heatball->direction = d_down;
	ld	hl, #0x000f
	add	hl, bc
	ld	(hl), #0x02
;src/heatball.c:14: heatball->entity.facing = (coin->pos.x < (SCREENWIDTH / 2) ? fa_right : fa_left);
	ld	hl, #0x0009
	add	hl, bc
	ld	c, l
	ld	b, h
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00105$
	ld	de, #0x0001
	jr	00106$
00105$:
	ld	de, #0x0000
00106$:
	ld	a, e
	ld	(bc), a
;src/heatball.c:15: }
	add	sp, #4
	ret
_fps:
	.db #0x3c	; 60
;src/heatball.c:17: void heatball_update(heatball_t *heatball) {
;	---------------------------------
; Function heatball_update
; ---------------------------------
_heatball_update::
	add	sp, #-8
;src/heatball.c:18: if (!heatball->can_move) return;
	ldhl	sp,	#10
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#2
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	ldhl	sp,	#7
	ld	(hl), a
	or	a, a
	jp	Z,00112$
;src/heatball.c:19: heatball_nextframe(heatball);
	pop	bc
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_heatball_nextframe
	add	sp, #2
;src/heatball.c:21: if (heatball->entity.facing == fa_left) {
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0009
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#6
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#4
;src/heatball.c:22: if (heatball->direction == d_down) {
	ld	(hl-), a
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000f
	add	hl, de
	inc	sp
	inc	sp
	push	hl
;src/heatball.c:23: heatball->pos.x -= 1;
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#7
	ld	(hl), a
;src/heatball.c:24: heatball->pos.y += 2;
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#5
	ld	(hl+), a
	ld	(hl), d
;src/heatball.c:22: if (heatball->direction == d_down) {
	pop	de
	push	de
	ld	a, (de)
;src/heatball.c:23: heatball->pos.x -= 1;
;src/heatball.c:22: if (heatball->direction == d_down) {
	sub	a, #0x02
	ld	a, #0x01
	jr	Z, 00135$
	xor	a, a
00135$:
	ld	c, a
;src/heatball.c:21: if (heatball->entity.facing == fa_left) {
	ldhl	sp,	#4
	ld	a, (hl)
	or	a, a
	jr	NZ, 00110$
;src/heatball.c:23: heatball->pos.x -= 1;
	ldhl	sp,	#7
	ld	b, (hl)
	dec	b
;src/heatball.c:22: if (heatball->direction == d_down) {
	ld	a, c
	or	a, a
	jr	Z, 00104$
;src/heatball.c:23: heatball->pos.x -= 1;
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), b
;src/heatball.c:24: heatball->pos.y += 2;
	ldhl	sp,#5
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, #0x02
	ld	c, a
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
	jr	00111$
00104$:
;src/heatball.c:26: heatball->pos.x -= 1;
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), b
;src/heatball.c:27: heatball->pos.y -= 2;
	ldhl	sp,#5
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
	jr	00111$
00110$:
;src/heatball.c:31: heatball->pos.x += 1;
	ldhl	sp,	#7
	ld	b, (hl)
	inc	b
;src/heatball.c:30: if (heatball->direction == d_down) {
	ld	a, c
	or	a, a
	jr	Z, 00107$
;src/heatball.c:31: heatball->pos.x += 1;
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), b
;src/heatball.c:32: heatball->pos.y += 2;
	ldhl	sp,#5
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, #0x02
	ld	c, a
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
	jr	00111$
00107$:
;src/heatball.c:34: heatball->pos.x += 1;
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), b
;src/heatball.c:35: heatball->pos.y -= 2;
	ldhl	sp,#5
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
00111$:
;src/heatball.c:39: heatball_check_collisions(heatball);
	pop	bc
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_heatball_check_collisions
	add	sp, #2
;src/heatball.c:40: heatball_draw(heatball);
	ldhl	sp,	#10
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_heatball_draw
	add	sp, #2
00112$:
;src/heatball.c:41: }
	add	sp, #8
	ret
;src/heatball.c:43: void heatball_check_collisions(heatball_t *heatball) {
;	---------------------------------
; Function heatball_check_collisions
; ---------------------------------
_heatball_check_collisions::
	add	sp, #-5
;src/heatball.c:44: if (heatball->pos.y <= 32)
	ldhl	sp,#7
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0001
	add	hl, bc
	inc	sp
	inc	sp
	ld	e, l
	ld	d, h
	push	de
	ld	a, (de)
	ldhl	sp,	#2
	ld	(hl), a
;src/heatball.c:45: heatball->direction = d_down;
	ld	hl, #0x000f
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#3
	ld	(hl+), a
	ld	(hl), d
;src/heatball.c:44: if (heatball->pos.y <= 32)
	ld	a, #0x20
	dec	hl
	dec	hl
	sub	a, (hl)
	jr	C, 00102$
;src/heatball.c:45: heatball->direction = d_down;
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x02
00102$:
;src/heatball.c:47: if (heatball->pos.y >= 144)
	pop	de
	push	de
	ld	a, (de)
	sub	a, #0x90
	jr	C, 00104$
;src/heatball.c:48: heatball->direction = d_up;
	ldhl	sp,	#3
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x03
00104$:
;src/heatball.c:50: if (heatball->pos.x >= (160 - 8)) {
	ld	a, (bc)
	ld	l, a
;src/heatball.c:51: heatball->entity.facing = fa_left;
	ld	a, c
	add	a, #0x09
	ld	e, a
	ld	a, b
	adc	a, #0x00
	ld	d, a
;src/heatball.c:50: if (heatball->pos.x >= (160 - 8)) {
	ld	a, l
	sub	a, #0x98
	jr	C, 00106$
;src/heatball.c:51: heatball->entity.facing = fa_left;
	xor	a, a
	ld	(de), a
00106$:
;src/heatball.c:54: if (heatball->pos.x <= 16) {
	ld	a, (bc)
	ld	c, a
	ld	a, #0x10
	sub	a, c
	jr	C, 00109$
;src/heatball.c:55: heatball->entity.facing = fa_right;
	ld	a, #0x01
	ld	(de), a
00109$:
;src/heatball.c:57: }
	add	sp, #5
	ret
;src/heatball.c:59: void heatball_nextframe(heatball_t *heatball) {
;	---------------------------------
; Function heatball_nextframe
; ---------------------------------
_heatball_nextframe::
;src/heatball.c:60: if (global_frame % 4 != 0) return;
	ld	a, (#_global_frame)
	and	a, #0x03
	ret	NZ
;src/heatball.c:61: heatball->entity.sprite.frame = (heatball->entity.sprite.frame == S_HEATBALL ? S_HEATBALL+1 : S_HEATBALL);
	pop	bc
	pop	de
	push	de
	push	bc
	ld	hl, #0x0008
	add	hl, de
	ld	a, (hl)
	sub	a, #0x12
	jr	NZ, 00105$
	ld	bc, #0x0013
	jr	00106$
00105$:
	ld	bc, #0x0012
00106$:
	ld	(hl), c
;src/heatball.c:62: }
	ret
;src/heatball.c:64: void heatball_kill(heatball_t *heatball) {
;	---------------------------------
; Function heatball_kill
; ---------------------------------
_heatball_kill::
;src/heatball.c:66: heatball->pos.x = 200;
	pop	bc
	pop	de
	push	de
	push	bc
	ld	a, #0xc8
	ld	(de), a
;src/heatball.c:67: heatball->pos.y = 200;
	ld	l, e
	ld	h, d
	inc	hl
	ld	(hl), #0xc8
;src/heatball.c:69: if (!heatball->entity.is_dead) heatball->entity.is_dead = true;
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00103$
	ld	(hl), #0x01
;src/heatball.c:70: if (!heatball->destroy_timer != 50) heatball->destroy_timer = 50;
00103$:
	ld	hl, #0x0011
	add	hl, de
	ld	(hl), #0x32
;src/heatball.c:71: heatball_draw(heatball);
	push	de
	call	_heatball_draw
	add	sp, #2
;src/heatball.c:72: }
	ret
;src/heatball.c:74: void heatball_draw(heatball_t *heatball) {
;	---------------------------------
; Function heatball_draw
; ---------------------------------
_heatball_draw::
	dec	sp
;src/heatball.c:75: set_sprite_tile(S_HEATBALL, heatball->entity.sprite.frame);
	ldhl	sp,#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	bc, #_shadow_OAM+74
	ldhl	sp,	#0
	ld	a, (hl)
	ld	(bc), a
;src/heatball.c:76: move_sprite(S_HEATBALL, heatball->pos.x, heatball->pos.y);
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #(_shadow_OAM + 0x0048)
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/heatball.c:76: move_sprite(S_HEATBALL, heatball->pos.x, heatball->pos.y);
;src/heatball.c:77: }
	inc	sp
	ret
	.area _CODE
	.area _CABS (ABS)
