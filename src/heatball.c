#include <gb/gb.h>
#include "main.h"
#include "game.h"
#include "coin.h"
#include "bullet.h"
#include "heatball.h"

void heatball_spawn(heatball_t *heatball, coin_t *coin) {
    heatball->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (160 - 16));
    heatball->pos.y = 32;
    heatball->entity.is_dead = false;
    heatball->destroy_timer = 0;
    heatball->direction = d_down;
    heatball->entity.facing = (coin->pos.x < (SCREENWIDTH / 2) ? fa_right : fa_left);
}

void heatball_update(heatball_t *heatball) {
    if (!heatball->can_move) return;
    heatball_nextframe(heatball);

    if (heatball->entity.facing == fa_left) {
        if (heatball->direction == d_down) {
            heatball->pos.x -= 1;
            heatball->pos.y += 2;
        } else {
            heatball->pos.x -= 1;
            heatball->pos.y -= 2;
        }
    } else {
        if (heatball->direction == d_down) {
            heatball->pos.x += 1;
            heatball->pos.y += 2;
        } else {
            heatball->pos.x += 1;
            heatball->pos.y -= 2;
        }
    }

    heatball_check_collisions(heatball);
    heatball_draw(heatball);
}

void heatball_check_collisions(heatball_t *heatball) {
    if (heatball->pos.y <= 32)
        heatball->direction = d_down;
    
    if (heatball->pos.y >= 144)
        heatball->direction = d_up;
    
    if (heatball->pos.x >= (160 - 8)) {
        heatball->entity.facing = fa_left;
    }

    if (heatball->pos.x <= 16) {
        heatball->entity.facing = fa_right;
    }
}

void heatball_nextframe(heatball_t *heatball) {
    if (global_frame % 4 != 0) return;
    heatball->entity.sprite.frame = (heatball->entity.sprite.frame == S_HEATBALL ? S_HEATBALL+1 : S_HEATBALL);
}

void heatball_kill(heatball_t *heatball) {
    // we just move the enemy out of the view
    heatball->pos.x = 200;
    heatball->pos.y = 200;
    // and we turn off the entity's updating function by setting the "is_dead" property to "true" and the "destroy_timer" property to 50
    if (!heatball->entity.is_dead) heatball->entity.is_dead = true;
    if (!heatball->destroy_timer != 50) heatball->destroy_timer = 50;
    heatball_draw(heatball);
}

void heatball_draw(heatball_t *heatball) {
    set_sprite_tile(S_HEATBALL, heatball->entity.sprite.frame);
    move_sprite(S_HEATBALL, heatball->pos.x, heatball->pos.y);
}
