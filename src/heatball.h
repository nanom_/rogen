#include <gb/gb.h>
#include "main.h"
#include "coin.h"
#include "bullet.h"
#ifndef _HEATBALL_H_
#define _HEATBALL_H_

typedef struct heatball_t {
    POINT pos;
    entity_t entity;
    direction_t direction;
    BOOL can_move;
    UINT8 destroy_timer;
} heatball_t;

void heatball_spawn(heatball_t *heatball, coin_t *coin);
void heatball_update(heatball_t *heatball);
void heatball_check_collisions(heatball_t *heatball);
void heatball_nextframe(heatball_t *heatball);
void heatball_kill(heatball_t *heatball);
void heatball_draw(heatball_t *heatball);

#endif
