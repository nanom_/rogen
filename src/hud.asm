;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module hud
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _strlen
	.globl _itoa
	.globl _printf
	.globl _putchar
	.globl _font_set
	.globl _font_load
	.globl _font_init
	.globl _color
	.globl _gotoxy
	.globl _reset
	.globl _waitpadup
	.globl _waitpad
	.globl _i
	.globl _font_lives
	.globl _font_hud
	.globl _ammo_str
	.globl _score_str
	.globl _hud_start
	.globl _hud_redraw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_score_str::
	.ds 4
_ammo_str::
	.ds 2
_font_hud::
	.ds 2
_font_lives::
	.ds 2
_i::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/hud.c:13: UINT8 score_str[] = "000";
	ld	hl, #_score_str
	ld	(hl), #0x30
	ld	hl, #(_score_str + 0x0001)
	ld	(hl), #0x30
	ld	hl, #(_score_str + 0x0002)
	ld	(hl), #0x30
	ld	hl, #(_score_str + 0x0003)
	ld	(hl), #0x00
;src/hud.c:14: UINT8 ammo_str[] = "0";
	ld	bc, #_ammo_str+0
	ld	a, #0x30
	ld	(bc), a
	inc	bc
	xor	a, a
	ld	(bc), a
;src/hud.c:16: UINT8 i=0;
	ld	hl, #_i
	ld	(hl), #0x00
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/hud.c:18: void hud_start(UINT8 score, UINT8 ammo, UINT8 lives) {
;	---------------------------------
; Function hud_start
; ---------------------------------
_hud_start::
;src/hud.c:19: font_init();
	call	_font_init
;src/hud.c:20: color(WHITE, DKGREY, SOLID);
	xor	a, a
	ld	d,a
	ld	e,#0x02
	push	de
	xor	a, a
	push	af
	inc	sp
	call	_color
	add	sp, #3
;src/hud.c:21: font_hud = font_load(font_min);
	ld	hl, #_font_min
	push	hl
	call	_font_load
	add	sp, #2
	ld	hl, #_font_hud
	ld	a, e
	ld	(hl+), a
	ld	(hl), d
;src/hud.c:22: font_lives = font_load(font_ibm);
	ld	hl, #_font_ibm
	push	hl
	call	_font_load
	add	sp, #2
	ld	hl, #_font_lives
	ld	a, e
	ld	(hl+), a
	ld	(hl), d
;src/hud.c:23: hud_redraw(score, ammo, lives);
	ldhl	sp,	#4
	ld	a, (hl)
	push	af
	inc	sp
	dec	hl
	ld	a, (hl)
	push	af
	inc	sp
	dec	hl
	ld	a, (hl)
	push	af
	inc	sp
	call	_hud_redraw
	add	sp, #3
;src/hud.c:24: }
	ret
;src/hud.c:26: void hud_redraw(UINT8 score, UINT8 ammo, UINT8 lives) {
;	---------------------------------
; Function hud_redraw
; ---------------------------------
_hud_redraw::
	add	sp, #-2
;src/hud.c:27: font_set(font_hud);
	ld	hl, #_font_hud
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_font_set
	add	sp, #2
;src/hud.c:28: gotoxy(0, 0);
	xor	a, a
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_gotoxy
	add	sp, #2
;src/hud.c:34: itoa(score, score_str);
	ld	de, #_score_str
	ldhl	sp,	#4
	ld	c, (hl)
	ld	b, #0x00
	push	de
	push	bc
	call	_itoa
	add	sp, #4
;src/hud.c:35: itoa(ammo, ammo_str);
	ld	de, #_ammo_str
	ldhl	sp,	#5
	ld	c, (hl)
	ld	b, #0x00
	push	de
	push	bc
	call	_itoa
	add	sp, #4
;src/hud.c:36: switch(strlen(score_str)) {
	ld	hl, #_score_str
	push	hl
	call	_strlen
	add	sp, #2
;src/hud.c:38: score_str[2] = score_str[0];
	push	de
	ld	de, #_score_str
	ld	hl, #0x0002
	add	hl, de
	pop	de
	inc	sp
	inc	sp
	push	hl
;src/hud.c:39: score_str[1] = '0';
	ld	bc, #_score_str + 1
;src/hud.c:36: switch(strlen(score_str)) {
	ld	a, e
	dec	a
	or	a, d
	jr	Z, 00101$
	ld	a, e
	sub	a, #0x02
	or	a, d
	jr	Z, 00102$
	jr	00103$
;src/hud.c:37: case 1:
00101$:
;src/hud.c:38: score_str[2] = score_str[0];
	ld	a, (#_score_str + 0)
	pop	hl
	push	hl
	ld	(hl), a
;src/hud.c:39: score_str[1] = '0';
	ld	a, #0x30
	ld	(bc), a
;src/hud.c:40: score_str[0] = '0';
	ld	hl, #_score_str
	ld	(hl), #0x30
;src/hud.c:41: break;
	jr	00103$
;src/hud.c:42: case 2:
00102$:
;src/hud.c:43: score_str[2] = score_str[1];
	ld	a, (bc)
	pop	hl
	push	hl
	ld	(hl), a
;src/hud.c:44: score_str[1] = score_str[0];
	ld	a, (#_score_str + 0)
	ld	(bc), a
;src/hud.c:45: score_str[0] = '0';
	ld	hl, #_score_str
	ld	(hl), #0x30
;src/hud.c:47: }
00103$:
;src/hud.c:50: if (lives == 4) {
	ldhl	sp,	#6
	ld	a, (hl)
	sub	a, #0x04
	jr	NZ, 00105$
;src/hud.c:51: HIDE_SPRITES;
	ldh	a, (_LCDC_REG+0)
	and	a, #0xfd
	ldh	(_LCDC_REG+0),a
;src/hud.c:52: printf("                    ");
	ld	hl, #___str_0
	push	hl
	call	_printf
	add	sp, #2
;src/hud.c:53: gotoxy(0, 0);
	xor	a, a
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_gotoxy
	add	sp, #2
;src/hud.c:54: printf("\n\n\n\n\n\n\n\n      GAME OVER                         ");
	ld	hl, #___str_1
	push	hl
	call	_printf
	add	sp, #2
;src/hud.c:55: printf("      SCORE ");
	ld	hl, #___str_2
	push	hl
	call	_printf
	add	sp, #2
;src/hud.c:56: putchar(score_str[0]);
	ld	a, (#_score_str + 0)
	push	af
	inc	sp
	call	_putchar
	inc	sp
;src/hud.c:39: score_str[1] = '0';
	ld	bc, #_score_str + 1
;src/hud.c:57: putchar(score_str[1]);
	ld	a, (bc)
	push	bc
	push	af
	inc	sp
	call	_putchar
	inc	sp
	pop	bc
;src/hud.c:38: score_str[2] = score_str[0];
	ld	de, #_score_str
	ld	hl, #0x0002
	add	hl, de
	inc	sp
	inc	sp
;src/hud.c:58: putchar(score_str[2]);
	ld	e, l
	ld	d, h
	push	de
	ld	a, (de)
	push	bc
	push	af
	inc	sp
	call	_putchar
	inc	sp
	ld	hl, #___str_3
	push	hl
	call	_printf
	add	sp, #2
	pop	bc
;src/hud.c:60: waitpad(J_ANYKEY);
	ld	a, #0xff
	push	af
	inc	sp
	call	_waitpad
	inc	sp
;src/hud.c:61: waitpadup();
	call	_waitpadup
;src/hud.c:62: reset();
	push	bc
	call	_reset
	pop	bc
00105$:
;src/hud.c:65: printf("SCORE ");
	push	bc
	ld	hl, #___str_4
	push	hl
	call	_printf
	add	sp, #2
	pop	bc
;src/hud.c:66: putchar(score_str[0]);
	ld	a, (#_score_str + 0)
	push	bc
	push	af
	inc	sp
	call	_putchar
	inc	sp
	pop	bc
;src/hud.c:67: putchar(score_str[1]);
	ld	a, (bc)
	push	af
	inc	sp
	call	_putchar
	inc	sp
;src/hud.c:68: putchar(score_str[2]);
	pop	de
	push	de
	ld	a, (de)
	push	af
	inc	sp
	call	_putchar
	inc	sp
;src/hud.c:69: printf(" AMMO ");
	ld	hl, #___str_5
	push	hl
	call	_printf
	add	sp, #2
;src/hud.c:70: putchar(ammo_str[0]);
	ld	a, (#_ammo_str + 0)
	push	af
	inc	sp
	call	_putchar
	inc	sp
;src/hud.c:71: printf(" ");
	ld	hl, #___str_6
	push	hl
	call	_printf
	add	sp, #2
;src/hud.c:73: font_set(font_lives);
	ld	hl, #_font_lives
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_font_set
	add	sp, #2
;src/hud.c:74: for(i=1; i<=3; i++)
	ld	hl, #_i
	ld	(hl), #0x01
00107$:
;src/hud.c:75: putchar(lives >= i ? '-' : ' ');
	ldhl	sp,	#6
	ld	a, (hl)
	ld	hl, #_i
	sub	a, (hl)
	jr	C, 00111$
	ld	bc, #0x002d
	jr	00112$
00111$:
	ld	bc, #0x0020
00112$:
	ld	a, c
	push	af
	inc	sp
	call	_putchar
	inc	sp
;src/hud.c:74: for(i=1; i<=3; i++)
	ld	hl, #_i
	inc	(hl)
	ld	a, #0x03
	sub	a, (hl)
	jr	NC, 00107$
;src/hud.c:76: }
	add	sp, #2
	ret
___str_0:
	.ascii "                    "
	.db 0x00
___str_1:
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.ascii "      GAME OVER                         "
	.db 0x00
___str_2:
	.ascii "      SCORE "
	.db 0x00
___str_3:
	.ascii "     "
	.db 0x00
___str_4:
	.ascii "SCORE "
	.db 0x00
___str_5:
	.ascii " AMMO "
	.db 0x00
___str_6:
	.ascii " "
	.db 0x00
	.area _CODE
	.area _CABS (ABS)
