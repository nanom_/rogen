#include <gb/gb.h>
#include <gb/console.h>
#include <gb/drawing.h>
#include <gb/font.h>
#include <rand.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "player.h"
#include "hud.h"

UINT8 score_str[] = "000";
UINT8 ammo_str[] = "0";
font_t font_hud, font_lives;
UINT8 i=0;
// score, ammo, lives
void hud_start(UINT8 score, UINT8 ammo, UINT8 lives) {
    font_init();
    color(WHITE, DKGREY, SOLID);
    font_hud = font_load(font_min);
    font_lives = font_load(font_ibm);
    hud_redraw(score, ammo, lives);
}

void hud_redraw(UINT8 score, UINT8 ammo, UINT8 lives) {
    font_set(font_hud);
    gotoxy(0, 0);

    /*
        this is probably the worst way to print a number with three leading 0s, but looks like the gbdk
        compiler (lcc) doesn't support %03u. weird.
    */
    itoa(score, score_str);
    itoa(ammo, ammo_str);
    switch(strlen(score_str)) {
        case 1:
            score_str[2] = score_str[0];
            score_str[1] = '0';
            score_str[0] = '0';
            break;
        case 2:
            score_str[2] = score_str[1];
            score_str[1] = score_str[0];
            score_str[0] = '0';
            break;
    }


    if (lives == 4) {
        HIDE_SPRITES;
        printf("                    ");
        gotoxy(0, 0);
        printf("\n\n\n\n\n\n\n\n      GAME OVER                         ");
        printf("      SCORE ");
        putchar(score_str[0]);
        putchar(score_str[1]);
        putchar(score_str[2]);
        printf("     ");
        waitpad(J_ANYKEY);
        waitpadup();
        reset();
    }

    printf("SCORE ");
    putchar(score_str[0]);
    putchar(score_str[1]);
    putchar(score_str[2]);
    printf(" AMMO ");
    putchar(ammo_str[0]);
    printf(" ");
    // dirty trick for turning hyphens - into hearts <3
    font_set(font_lives);
    for(i=1; i<=3; i++)
        putchar(lives >= i ? '-' : ' ');
}
