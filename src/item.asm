;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module item
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _abs
	.globl _arand
	.globl _initarand
	.globl _item_spawn_range
	.globl _item_factor
	.globl _item_spawn
	.globl _item_update
	.globl _item_destroy
	.globl _item_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
_item_factor::
	.ds 1
_item_spawn_range::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/item.c:9: void item_spawn(item_t *item) {    
;	---------------------------------
; Function item_spawn
; ---------------------------------
_item_spawn::
	add	sp, #-2
;src/item.c:10: item_factor = (global_frame % 2 != 0 ? 1 : 2);
	ld	hl, #_global_frame
	bit	0, (hl)
	jr	Z, 00105$
	ld	bc, #0x0001
	jr	00106$
00105$:
	ld	bc, #0x0002
00106$:
	ld	hl, #_item_factor
	ld	(hl), c
;src/item.c:11: seed = DIV_REG;
	ldh	a, (_DIV_REG+0)
	ld	hl, #_seed
	ld	(hl), a
;src/item.c:12: seed |= (UINT16)DIV_REG << 8;
	ldh	a, (_DIV_REG+0)
;src/item.c:13: initarand(seed + rand_menukey + (rand_menuframe * item_factor) * (global_frame * item_factor));
	ld	c, (hl)
	ld	b, #0x00
	ld	hl, #_rand_menukey
	ld	e, (hl)
	ld	d, #0x00
	ld	a, e
	add	a, c
	ld	c, a
	ld	a, d
	adc	a, b
	ld	b, a
	push	bc
	ld	a, (#_item_factor)
	push	af
	inc	sp
	ld	a, (#_rand_menuframe)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	ldhl	sp,	#2
	ld	a, e
	ld	(hl+), a
	ld	(hl), d
	ld	a, (#_item_factor)
	push	af
	inc	sp
	ld	a, (#_global_frame)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	push	de
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	__mulint
	add	sp, #4
	pop	bc
	ld	l, e
	ld	h, d
	add	hl, bc
	push	hl
	call	_initarand
	add	sp, #2
;src/item.c:15: item_spawn_range = abs(arand());
	call	_arand
	ld	a, e
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	hl, #_item_spawn_range
	ld	(hl), e
;src/item.c:17: item->type = (item_spawn_range > 51 ? i_disabled : (global_frame % 15 != 0 && global_frame != 33 ? i_ammo : i_extralife));
	ldhl	sp,#4
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0002
	add	hl, bc
	inc	sp
	inc	sp
	push	hl
	ld	a, #0x33
	ld	hl, #_item_spawn_range
	sub	a, (hl)
	jr	NC, 00107$
	ld	de, #0x0000
	jr	00108$
00107$:
	ld	hl, #_global_frame
	ld	e, (hl)
	ld	d, #0x00
	push	bc
	ld	hl, #0x000f
	push	hl
	push	de
	call	__modsint
	add	sp, #4
	pop	bc
	ld	a, d
	or	a, e
	jr	Z, 00109$
	ld	a, (#_global_frame)
	sub	a, #0x21
	ld	e, #0x01
	jr	NZ, 00110$
00109$:
	ld	e, #0x02
00110$:
00108$:
	ld	a, e
	pop	hl
	push	hl
	ld	(hl), a
;src/item.c:18: if (item->type == i_disabled) return;
	pop	de
	push	de
	push	af
	ld	a, (de)
	ld	l, a
	pop	af
	or	a, a
	jr	Z, 00103$
;src/item.c:20: item->sprite = (item->type == i_ammo ? S_GUN : S_EXTRALIFE);
	ld	e, c
	ld	d, b
	inc	de
	inc	de
	inc	de
	dec	l
	jr	NZ, 00114$
	ld	hl, #0x001a
	jr	00115$
00114$:
	ld	hl, #0x001d
00115$:
	ld	a, l
	ld	(de), a
;src/item.c:22: item->pos.x = 88;
	ld	a, #0x58
	ld	(bc), a
;src/item.c:23: item->pos.y = 82;
	ld	l, c
	ld	h, b
	inc	hl
	ld	(hl), #0x52
;src/item.c:25: item_draw(item);
	push	bc
	call	_item_draw
	add	sp, #2
00103$:
;src/item.c:26: }
	add	sp, #2
	ret
_fps:
	.db #0x3c	; 60
;src/item.c:28: void item_update(item_t *item) {
;	---------------------------------
; Function item_update
; ---------------------------------
_item_update::
	add	sp, #-3
;src/item.c:29: if (item->type == i_disabled) return;
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	hl
	push	hl
	inc	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#2
	ld	(hl), a
	ldhl	sp,	#2
	ld	a, (hl)
	or	a, a
	jr	Z, 00103$
;src/item.c:30: item->sprite = (global_frame % 6 == 0 ? S_NOTHING : (item->type == i_ammo ? S_GUN : S_EXTRALIFE));
	pop	bc
	push	bc
	inc	bc
	inc	bc
	inc	bc
	ld	hl, #_global_frame
	ld	e, (hl)
	ld	d, #0x00
	push	bc
	ld	hl, #0x0006
	push	hl
	push	de
	call	__modsint
	add	sp, #4
	pop	bc
	ld	a, d
	or	a, e
	jr	NZ, 00105$
	ld	de, #0x001f
	jr	00106$
00105$:
	ldhl	sp,	#2
	ld	a, (hl)
	dec	a
	jr	NZ, 00107$
	ld	de, #0x001a
	jr	00108$
00107$:
	ld	de, #0x001d
00108$:
00106$:
	ld	a, e
	ld	(bc), a
;src/item.c:31: item_draw(item);
	pop	hl
	push	hl
	push	hl
	call	_item_draw
	add	sp, #2
00103$:
;src/item.c:32: }
	add	sp, #3
	ret
;src/item.c:34: void item_destroy(item_t *item) {
;	---------------------------------
; Function item_destroy
; ---------------------------------
_item_destroy::
;src/item.c:35: item->type = i_disabled;
	pop	bc
	pop	de
	push	de
	push	bc
	ld	c, e
	ld	b, d
	inc	bc
	inc	bc
	xor	a, a
	ld	(bc), a
;src/item.c:36: item->sprite = S_NOTHING;
	ld	l, e
	ld	h, d
	inc	hl
	inc	hl
	inc	hl
	ld	(hl), #0x1f
;src/item.c:37: item->pos.x = 200;
	ld	a, #0xc8
	ld	(de), a
;src/item.c:38: item->pos.y = 200;
	ld	l, e
	ld	h, d
	inc	hl
	ld	(hl), #0xc8
;src/item.c:39: item_draw(item);
	push	de
	call	_item_draw
	add	sp, #2
;src/item.c:40: }
	ret
;src/item.c:42: void item_draw(item_t *item) {
;	---------------------------------
; Function item_draw
; ---------------------------------
_item_draw::
	dec	sp
;src/item.c:43: set_sprite_tile(S_ITEM, item->sprite);
	ldhl	sp,#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	c, e
	ld	b, d
	inc	bc
	inc	bc
	inc	bc
	ld	a, (bc)
	ldhl	sp,	#0
	ld	(hl), a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	bc, #_shadow_OAM+122
	ld	a, (hl)
	ld	(bc), a
;src/item.c:44: move_sprite(S_ITEM, item->pos.x, item->pos.y);
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #(_shadow_OAM + 0x0078)
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/item.c:44: move_sprite(S_ITEM, item->pos.x, item->pos.y);
;src/item.c:45: }
	inc	sp
	ret
	.area _CODE
	.area _CABS (ABS)
