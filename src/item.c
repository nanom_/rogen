#include <gb/gb.h>
#include <rand.h>
#include "game.h"
#include "main.h"
#include "math.h"
#include "item.h"

UINT8 item_factor, item_spawn_range;
void item_spawn(item_t *item) {    
    item_factor = (global_frame % 2 != 0 ? 1 : 2);
    seed = DIV_REG;
    seed |= (UINT16)DIV_REG << 8;
    initarand(seed + rand_menukey + (rand_menuframe * item_factor) * (global_frame * item_factor));

    item_spawn_range = abs(arand());
    // there's a 20% chance that a random item will spawn in a level (as long as the player score is not a prime number, in that case the item spawn chance is 0)
    item->type = (item_spawn_range > 51 ? i_disabled : (global_frame % 15 != 0 && global_frame != 33 ? i_ammo : i_extralife));
    if (item->type == i_disabled) return;
    
    item->sprite = (item->type == i_ammo ? S_GUN : S_EXTRALIFE);

    item->pos.x = 88;
    item->pos.y = 82;
    
    item_draw(item);
}

void item_update(item_t *item) {
    if (item->type == i_disabled) return;
    item->sprite = (global_frame % 6 == 0 ? S_NOTHING : (item->type == i_ammo ? S_GUN : S_EXTRALIFE));
    item_draw(item);
}

void item_destroy(item_t *item) {
    item->type = i_disabled;
    item->sprite = S_NOTHING;
    item->pos.x = 200;
    item->pos.y = 200;
    item_draw(item);
}

void item_draw(item_t *item) {
    set_sprite_tile(S_ITEM, item->sprite);
    move_sprite(S_ITEM, item->pos.x, item->pos.y);
}
