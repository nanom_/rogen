#include <gb/gb.h>
#include "main.h"
#ifndef _ITEM_H_
#define _ITEM_H_

typedef enum {i_disabled, i_ammo, i_extralife} item_type_t;

typedef struct item_t {
    POINT pos;
    item_type_t type;
    UINT8 sprite;
} item_t;

void item_spawn(item_t *item);
void item_update(item_t *item);
void item_destroy(item_t *item);
void item_draw(item_t *item);

#endif
