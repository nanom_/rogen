;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module level
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _level
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_level::
	.ds 340
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/level.c:9: unsigned char level[] =
	ld	bc, #_level+0
	ld	e, c
	ld	d, b
	call	__initrleblock
	.db	#1
	.db	#0x2D
	.db	#-18, #0x32
	.db	#2
	.db	#0x30, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2E
	.db	#-18, #0x2c
	.db	#2
	.db	#0x2E, #0x2F
	.db	#-18, #0x32
	.db	#1
	.db	#0x31
	.db	#0
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	.area _CODE
	.area _CABS (ABS)
