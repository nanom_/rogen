/*
    Level data generated using the GameBoy Map Builder
    (see assets/ to get the editable files.)
*/
#ifndef _LEVEL_H_
#define _LEVEL_H_

#define levelWidth 20
#define levelHeight 17
#define levelBank 0

extern unsigned char level[];

#endif
