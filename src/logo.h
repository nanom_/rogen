/*
    Tile data generated using the GameBoy Tile Designer
    (see assets/ to get the editable files.)
*/
#ifndef _LOGO_H_
#define _LOGO_H_

#define logoWidth 20
#define logoHeight 17
#define logoBank 0

extern unsigned char logo[];

#endif
