;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module main
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _game_update
	.globl _game_input
	.globl _game_start
	.globl _show_title
	.globl _show_warning
	.globl _delay
	.globl _global_frame
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
_global_frame::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;src/main.c:7: UINT8 global_frame = 0;
	ld	hl, #_global_frame
	ld	(hl), #0x00
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/main.c:9: void main() {
;	---------------------------------
; Function main
; ---------------------------------
_main::
;src/main.c:10: show_warning();
	call	_show_warning
;src/main.c:11: show_title();
	call	_show_title
;src/main.c:12: game_start();
	call	_game_start
;src/main.c:13: while(1) {
00107$:
;src/main.c:14: if (global_frame < 59)
	ld	hl, #_global_frame
	ld	a, (hl)
	sub	a, #0x3b
	jr	NC, 00102$
;src/main.c:15: global_frame++;
	inc	(hl)
	jr	00103$
00102$:
;src/main.c:17: global_frame = 0;
	ld	hl, #_global_frame
	ld	(hl), #0x00
00103$:
;src/main.c:19: if (!paused) {
	ld	a, (#_paused)
	or	a, a
	jr	NZ, 00105$
;src/main.c:20: game_input();
	call	_game_input
;src/main.c:21: game_update();
	call	_game_update
00105$:
;src/main.c:24: delay(1000 / fps);
	ld	hl, #_fps
	ld	c, (hl)
	ld	b, #0x00
	push	bc
	ld	hl, #0x03e8
	push	hl
	call	__divsint
	add	sp, #4
	push	de
	call	_delay
	add	sp, #2
;src/main.c:26: }
	jr	00107$
_fps:
	.db #0x3c	; 60
	.area _CODE
	.area _CABS (ABS)
