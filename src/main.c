#include <gb/gb.h>
#include "main.h"
#include "warning.h"
#include "title.h"
#include "game.h"

UINT8 global_frame = 0;

void main() {
    show_warning();
    show_title();
    game_start();
    while(1) {
        if (global_frame < 59)
            global_frame++;
        else
            global_frame = 0;

        if (!paused) {
            game_input();
            game_update();
        }
        
        delay(1000 / fps);
    }
}
