#include <gb/gb.h>
#ifndef _MAIN_H_
#define _MAIN_H_

// sprite constants
#define S_PLAYER 0
#define S_DRONE 8
#define S_SPLITTER 10
#define S_SKULLTRON 12
#define S_SKULLPERIOR 14
#define S_TURRET 16
#define S_HEATBALL 18
#define S_SPIKE 20
#define S_BOMB 21
#define S_EXPLO1 22
#define S_EXPLO2 23
#define S_BEAM 24
#define S_COIN 25
#define S_GUN 26
#define S_TURRETBEAM 27
#define S_BULLET 28
#define S_EXTRALIFE 29
#define S_ITEM 30 // this is a heart sprite, but now it's used to copy S_GUN, S_NOTHING and S_EXTRALIFE when necessary
#define S_NOTHING 31 // this is, well, an empty sprite.
#define COPY_BEAM 32 // 32 to 39 are sprites for beam sprite data (S_BEAM is copied to each one)

#define J_ANYKEY J_LEFT | J_RIGHT | J_DOWN | J_UP | J_A | J_B | J_START | J_SELECT
#define VERSION "10" // this is the version string to be written in the title screen (max. length 2, only alphanumeric characters)
typedef enum {false, true} BOOL;
typedef enum {fa_left, fa_right} facing_t;
typedef enum {d_left, d_right, d_down, d_up, d_nodir} direction_t;

typedef struct POINT {
    UINT8 x, y;
} POINT;

typedef struct frames_t {
    UINT8 first_frame, last_frame, left_anim_start, left_anim_end, right_anim_start, right_anim_end, frame;
} frames_t;

typedef struct entity_t {
    frames_t sprite;
    facing_t facing;
    UINT8 spd;
    BOOL is_moving, is_shooting, can_shoot, is_dead;
} entity_t;

#endif
