;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module math
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _is_prime
	.globl _absdw
	.globl _absw
	.globl _abs
	.globl _n
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_n::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/math.c:4: UINT8 n=0;
	ld	hl, #_n
	ld	(hl), #0x00
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/math.c:6: UINT8 abs(INT8 x) {
;	---------------------------------
; Function abs
; ---------------------------------
_abs::
;src/math.c:7: return (UINT8)(x < 0 ? x*(-1) : x);
	ldhl	sp,	#2
	bit	7, (hl)
	jr	Z, 00103$
	xor	a, a
	sub	a, (hl)
	ld	e, a
	ret
00103$:
	ldhl	sp,	#2
	ld	e, (hl)
;src/math.c:8: }
	ret
;src/math.c:11: UINT16 absw(INT16 x) {
;	---------------------------------
; Function absw
; ---------------------------------
_absw::
;src/math.c:12: return (UINT16)(x < 0 ? x*(-1) : x);
	ldhl	sp,	#2
	ld	a, (hl)
	sub	a, #0x00
	inc	hl
	ld	a, (hl)
	sbc	a, #0x00
	ld	d, (hl)
	ld	a, #0x00
	bit	7,a
	jr	Z, 00110$
	bit	7, d
	jr	NZ, 00111$
	cp	a, a
	jr	00111$
00110$:
	bit	7, d
	jr	Z, 00111$
	scf
00111$:
	jr	NC, 00103$
	ld	de, #0x0000
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	jr	00104$
00103$:
	ldhl	sp,#2
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
00104$:
	ld	e, c
	ld	d, b
;src/math.c:13: }
	ret
;src/math.c:16: UINT32 absdw(INT32 x) {
;	---------------------------------
; Function absdw
; ---------------------------------
_absdw::
	add	sp, #-4
;src/math.c:17: return (UINT32)(x < 0 ? x*(-1) : x);
	ldhl	sp,	#6
	ld	a, (hl)
	sub	a, #0x00
	inc	hl
	ld	a, (hl)
	sbc	a, #0x00
	inc	hl
	ld	a, (hl)
	sbc	a, #0x00
	inc	hl
	ld	a, (hl)
	sbc	a, #0x00
	ld	d, (hl)
	ld	a, #0x00
	bit	7,a
	jr	Z, 00110$
	bit	7, d
	jr	NZ, 00111$
	cp	a, a
	jr	00111$
00110$:
	bit	7, d
	jr	Z, 00111$
	scf
00111$:
	jr	NC, 00103$
	ld	de, #0x0000
	ld	a, e
	ldhl	sp,	#6
	sub	a, (hl)
	ld	e, a
	ld	a, d
	inc	hl
	sbc	a, (hl)
	push	af
	ldhl	sp,	#3
	ld	(hl-), a
	ld	(hl), e
	ld	de, #0x0000
	ldhl	sp,	#10
	pop	af
	ld	a, e
	sbc	a, (hl)
	ld	e, a
	ld	a, d
	inc	hl
	sbc	a, (hl)
	ldhl	sp,	#3
	ld	(hl-), a
	ld	(hl), e
	jr	00104$
00103$:
	ldhl	sp,	#6
	ld	d, h
	ld	e, l
	ldhl	sp,	#0
	ld	a, (de)
	ld	(hl+), a
	inc	de
	ld	a, (de)
	ld	(hl+), a
	inc	de
	ld	a, (de)
	ld	(hl+), a
	inc	de
	ld	a, (de)
	ld	(hl), a
00104$:
	ldhl	sp,	#0
	ld	a, (hl+)
	ld	e, a
	ld	a, (hl+)
	ld	d, a
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	l, c
	ld	h, b
;src/math.c:18: }
	add	sp, #4
	ret
;src/math.c:21: BOOL is_prime(UINT8 x) {
;	---------------------------------
; Function is_prime
; ---------------------------------
_is_prime::
;src/math.c:22: if (x == 0 || x == 1) return false;
	ldhl	sp,	#2
	ld	a, (hl)
	or	a, a
	jr	Z, 00101$
	ld	a, (hl)
	dec	a
	jr	NZ, 00102$
00101$:
	ld	e, #0x00
	ret
00102$:
;src/math.c:23: for (n=2; n < x; n++) {
	ld	hl, #_n
	ld	(hl), #0x02
00108$:
	ld	a, (#_n)
	ldhl	sp,	#2
	sub	a, (hl)
	jr	NC, 00106$
;src/math.c:24: if (x % n == 0) return false;
	ld	a, (#_n)
	push	af
	inc	sp
	ldhl	sp,	#3
	ld	a, (hl)
	push	af
	inc	sp
	call	__moduchar
	add	sp, #2
	ld	a, e
	or	a,a
	jr	NZ, 00109$
	ld	e,a
	ret
00109$:
;src/math.c:23: for (n=2; n < x; n++) {
	ld	hl, #_n
	inc	(hl)
	jr	00108$
00106$:
;src/math.c:26: return true;
	ld	e, #0x01
;src/math.c:27: }
	ret
	.area _CODE
	.area _CABS (ABS)
