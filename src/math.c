#include <gb/gb.h>
#include "main.h"

UINT8 n=0;
// returns the modulus of an int8 as an unsigned char
UINT8 abs(INT8 x) {
    return (UINT8)(x < 0 ? x*(-1) : x);
}

// returns the modulus of an int16 as a word
UINT16 absw(INT16 x) {
    return (UINT16)(x < 0 ? x*(-1) : x);
}

// returns the modulus of an int32 as a dword
UINT32 absdw(INT32 x) {
    return (UINT32)(x < 0 ? x*(-1) : x);
}

// returns whether the given number x is prime or not
BOOL is_prime(UINT8 x) {
    if (x == 0 || x == 1) return false;
    for (n=2; n < x; n++) {
        if (x % n == 0) return false;
    }
    return true;
}

