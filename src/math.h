#include <gb/gb.h>
#include "main.h"
#ifndef _MATH_H_
#define _MATH_H_

UINT8 abs(INT8);
UINT16 absw(INT16);
UINT32 absdw(INT32);
BOOL is_prime(UINT8);

#endif
