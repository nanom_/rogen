;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module player
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _item_destroy
	.globl _spike_kill
	.globl _heatball_kill
	.globl _turret_kill
	.globl _skulltron_kill
	.globl _splitter_kill
	.globl _drone_kill
	.globl _bullet_destroy
	.globl _bullet_spawn
	.globl _coin_destroy
	.globl _hud_redraw
	.globl _game_beam_speed_hard
	.globl _game_beam_speed_medium
	.globl _game_new_beam
	.globl _game_spawn_item
	.globl _game_next_enemy
	.globl _joypad
	.globl _total_frames
	.globl _max_frame
	.globl _min_frame
	.globl _player_spawn
	.globl _player_input
	.globl _player_update
	.globl _player_shoot
	.globl _player_check_collisions
	.globl _player_set_check_point
	.globl _player_nextframe
	.globl _player_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
_min_frame::
	.ds 1
_max_frame::
	.ds 1
_total_frames::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/player.c:16: void player_spawn(player_t *player) {
;	---------------------------------
; Function player_spawn
; ---------------------------------
_player_spawn::
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	hl, #(_shadow_OAM + 0x0002)
	ld	(hl), #0x00
;src/player.c:18: player_draw(player);
	pop	bc
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_player_draw
	add	sp, #2
;src/player.c:19: }
	ret
_fps:
	.db #0x3c	; 60
;src/player.c:21: void player_input(player_t *player, bullet_t *bullet) {
;	---------------------------------
; Function player_input
; ---------------------------------
_player_input::
	add	sp, #-10
;src/player.c:22: if (player->is_dead) return;
	ldhl	sp,	#12
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	hl, #0x0015
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jp	NZ,00133$
;src/player.c:23: if (joypad() & J_A) {
	call	_joypad
	bit	4, e
	jr	Z, 00108$
;src/player.c:24: if (player->ammo > 0 && player->entity.can_shoot && bullet->disabled) {
	pop	de
	push	de
	ld	hl, #0x0013
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jr	Z, 00108$
	pop	de
	push	de
	ld	hl, #0x000f
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#7
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	Z, 00108$
	ldhl	sp,#14
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0005
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00108$
;src/player.c:25: player_shoot(player, bullet);
	push	bc
	push	de
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_player_shoot
	add	sp, #4
	pop	bc
;src/player.c:26: player->ammo--;
	ld	a, (bc)
	add	a, #0xff
	ldhl	sp,	#9
	ld	(hl), a
	ld	(bc), a
;src/player.c:27: player->entity.can_shoot = false;
	dec	hl
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:28: hud_redraw(player->score, player->ammo, player->lives);
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	c,l
	ld	b, (hl)
	pop	de
	push	de
	ld	hl, #0x0012
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (de)
	push	bc
	inc	sp
	ldhl	sp,	#10
	ld	h, (hl)
	push	hl
	inc	sp
	push	af
	inc	sp
	call	_hud_redraw
	add	sp, #3
00108$:
;src/player.c:31: if (!player->can_move) {
	pop	de
	push	de
	ld	hl, #0x0014
	add	hl, de
	ld	c, (hl)
;src/player.c:32: player->entity.is_moving = false;
	pop	de
	push	de
	ld	hl, #0x000d
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
;src/player.c:31: if (!player->can_move) {
	ld	a, c
	or	a, a
	jr	NZ, 00110$
;src/player.c:32: player->entity.is_moving = false;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/player.c:33: return;
	jp	00133$
00110$:
;src/player.c:35: if (joypad() & J_LEFT) {
	call	_joypad
	ld	c, e
;src/player.c:37: player->pos.x -= player->entity.spd;
	pop	de
	push	de
	ld	hl, #0x000c
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#4
	ld	(hl+), a
	ld	(hl), d
;src/player.c:39: if (player->entity.facing == fa_right)
	pop	de
	push	de
	ld	hl, #0x000b
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#6
	ld	(hl+), a
	ld	(hl), d
;src/player.c:40: player->entity.sprite.frame = 0;
	pop	de
	push	de
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
;src/player.c:35: if (joypad() & J_LEFT) {
	bit	1, c
	jr	Z, 00116$
;src/player.c:36: if (player->pos.x > 16) {
	pop	de
	push	de
	ld	a, (de)
	ld	c, a
	ld	a, #0x10
	sub	a, c
	jr	NC, 00116$
;src/player.c:37: player->pos.x -= player->entity.spd;
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
	ld	a, c
	sub	a, b
	ld	c, a
	pop	hl
	push	hl
	ld	(hl), c
;src/player.c:38: player->entity.is_moving = true;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
;src/player.c:39: if (player->entity.facing == fa_right)
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	dec	a
	jr	NZ, 00112$
;src/player.c:40: player->entity.sprite.frame = 0;
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
00112$:
;src/player.c:41: player->entity.facing = fa_left;
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
00116$:
;src/player.c:44: if (joypad() & J_RIGHT) {
	call	_joypad
	ld	a, e
	rrca
	jr	NC, 00122$
;src/player.c:36: if (player->pos.x > 16) {
	pop	de
	push	de
	ld	a, (de)
;src/player.c:45: if (player->pos.x < SCREENWIDTH - 8) {
	ld	c, a
	sub	a, #0x98
	jr	NC, 00122$
;src/player.c:46: player->pos.x += player->entity.spd;
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, c
	ld	c, a
	pop	hl
	push	hl
	ld	(hl), c
;src/player.c:47: player->entity.is_moving = true;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
;src/player.c:48: if (player->entity.facing == fa_left)
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00118$
;src/player.c:49: player->entity.sprite.frame = 4;
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x04
00118$:
;src/player.c:50: player->entity.facing = fa_right;
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00122$:
;src/player.c:53: if (joypad() & J_DOWN) {
	call	_joypad
;src/player.c:54: if (player->pos.y < SCREENHEIGHT) {
	pop	bc
	push	bc
	inc	bc
;src/player.c:53: if (joypad() & J_DOWN) {
	bit	3, e
	jr	Z, 00126$
;src/player.c:54: if (player->pos.y < SCREENHEIGHT) {
	ld	a, (bc)
	cp	a, #0x90
	jr	NC, 00126$
;src/player.c:55: player->pos.y += player->entity.spd;
	ldhl	sp,#4
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	push	af
	ld	a, (de)
	ld	l, a
	pop	af
	add	a, l
	ld	(bc), a
;src/player.c:56: player->entity.is_moving = true;
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00126$:
;src/player.c:59: if (joypad() & J_UP) {
	call	_joypad
	bit	2, e
	jr	Z, 00130$
;src/player.c:60: if (player->pos.y > 32) {
	ld	a, (bc)
	ldhl	sp,	#8
	ld	(hl), a
	ld	a, #0x20
	sub	a, (hl)
	jr	NC, 00130$
;src/player.c:61: player->pos.y -= player->entity.spd;
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#9
	ld	(hl-), a
	ld	a, (hl+)
	sub	a, (hl)
	ld	(bc), a
;src/player.c:62: player->entity.is_moving = true;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
00130$:
;src/player.c:65: if (!joypad())
	call	_joypad
	ld	a, e
	or	a, a
	jr	NZ, 00133$
;src/player.c:66: player->entity.is_moving = false;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
00133$:
;src/player.c:67: }
	add	sp, #10
	ret
;src/player.c:69: void player_update(player_t *player, coin_t *coin, bullet_t *bullet, UINT8 beam_count,
;	---------------------------------
; Function player_update
; ---------------------------------
_player_update::
	add	sp, #-14
;src/player.c:74: if (player->is_dead && player->respawn_timer == 121) return;
	ldhl	sp,#16
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0015
	add	hl, bc
	ld	a, (hl)
	ldhl	sp,	#11
	ld	(hl), a
	ld	hl, #0x0016
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#12
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	dec	hl
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x79
	jp	Z,00135$
	jr	00102$
00102$:
;src/player.c:75: if (player->is_dead) {
	ldhl	sp,	#11
	ld	a, (hl)
	or	a, a
	jp	Z, 00131$
;src/player.c:76: if (player->respawn_timer % 10 !=0 || player->entity.sprite.frame < 8) {
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	e, a
	ld	d, #0x00
	push	bc
	ld	hl, #0x000a
	push	hl
	push	de
	call	__modsint
	add	sp, #4
	ldhl	sp,	#11
	ld	a, e
	ld	(hl+), a
	ld	(hl), d
	pop	bc
	ld	hl, #0x000a
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#11
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	inc	hl
	ld	(hl), a
	ldhl	sp,	#10
	ld	a, (hl-)
	or	a, (hl)
	jr	NZ, 00106$
	ldhl	sp,	#13
	ld	a, (hl)
	sub	a, #0x08
	jr	NC, 00107$
00106$:
;src/player.c:77: player->entity.sprite.frame = (player->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);
	ldhl	sp,	#13
	ld	a, (hl)
	sub	a, #0x16
	jr	NZ, 00137$
	ld	de, #0x0017
	jr	00138$
00137$:
	ld	de, #0x0016
00138$:
	ld	a, e
	ldhl	sp,	#11
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/player.c:78: if (player->entity.sprite.frame == S_EXPLO2)
	sub	a, #0x17
	jr	NZ, 00107$
;src/player.c:79: BGP_REG = (BGP_REG != 0xFE ? 0xFE : 0xE4);
	ldh	a, (_BGP_REG+0)
	sub	a, #0xfe
	jr	Z, 00139$
	ld	de, #0x00fe
	jr	00140$
00139$:
	ld	de, #0x00e4
00140$:
	ld	a, e
	ldh	(_BGP_REG+0),a
00107$:
;src/player.c:81: player_draw(player);
	push	bc
	call	_player_draw
	add	sp, #2
;src/player.c:74: if (player->is_dead && player->respawn_timer == 121) return;
	ldhl	sp,	#16
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
;src/player.c:82: player->respawn_timer++;
	pop	de
	push	de
	ld	hl, #0x0016
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	inc	a
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), a
;src/player.c:83: if (player->respawn_timer == 120) {
	sub	a, #0x78
	jp	NZ,00135$
;src/player.c:84: if (player->lives > 0) {
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#4
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
;src/player.c:76: if (player->respawn_timer % 10 !=0 || player->entity.sprite.frame < 8) {
	pop	de
	push	de
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#6
	ld	(hl+), a
	ld	(hl), d
;src/player.c:89: player->pos.y = player->check_point.y;
	pop	de
	push	de
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
;src/player.c:86: hud_redraw(player->score, player->ammo, player->lives);
	pop	de
	push	de
	ld	hl, #0x0013
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#10
	ld	(hl+), a
	ld	(hl), d
	pop	de
	push	de
	ld	hl, #0x0012
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#12
	ld	(hl+), a
	ld	(hl), d
;src/player.c:84: if (player->lives > 0) {
	ld	a, c
	or	a, a
	jp	Z, 00126$
;src/player.c:85: BGP_REG = 0xE4;
	ld	a, #0xe4
	ldh	(_BGP_REG+0),a
;src/player.c:86: hud_redraw(player->score, player->ammo, player->lives);
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	push	bc
	inc	sp
	ld	h, c
	push	hl
	inc	sp
	push	af
	inc	sp
	call	_hud_redraw
	add	sp, #3
;src/player.c:87: player->respawn_timer = 0;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/player.c:88: player->pos.x = player->check_point.x;
	pop	hl
	push	hl
	inc	hl
	inc	hl
	ld	c, (hl)
	pop	hl
	push	hl
	ld	(hl), c
;src/player.c:89: player->pos.y = player->check_point.y;
	pop	hl
	push	hl
	inc	hl
	inc	hl
	inc	hl
	ld	c, (hl)
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:90: player->entity.sprite.frame = (player->entity.facing == fa_left ? 0 : 4);
	pop	de
	push	de
	ld	hl, #0x000b
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jr	NZ, 00141$
	ld	bc, #0x0000
	jr	00142$
00141$:
	ld	bc, #0x0004
00142$:
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:91: if (!drone->entity.is_dead) {
	ldhl	sp,	#25
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#5
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	ldhl	sp,	#13
	ld	(hl), a
;src/player.c:93: drone->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
	ldhl	sp,	#18
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#7
	ld	(hl+), a
;src/player.c:94: drone->pos.y = coin->pos.y;
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#9
	ld	(hl+), a
	ld	(hl), d
;src/player.c:91: if (!drone->entity.is_dead) {
	ldhl	sp,	#13
	ld	a, (hl)
	or	a, a
	jr	NZ, 00110$
;src/player.c:92: drone->can_move = true;
	ldhl	sp,#5
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#12
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/player.c:93: drone->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#11
	ld	(hl+), a
	ld	(hl), e
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#13
	ld	(hl), a
	sub	a, #0x50
	jr	NC, 00143$
	ld	a, (hl)
	add	a, #0xf8
	ld	c, a
	jr	00144$
00143$:
	ldhl	sp,	#13
	ld	a, (hl)
	add	a, #0x08
	ld	c, a
00144$:
	ldhl	sp,	#11
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:94: drone->pos.y = coin->pos.y;
	ldhl	sp,#5
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	inc	bc
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(bc), a
00110$:
;src/player.c:96: if (!splitter->entity.is_dead) {
	ldhl	sp,#27
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x000e
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00112$
;src/player.c:97: splitter->can_move = true;
	ld	hl, #0x0010
	add	hl, bc
	ld	(hl), #0x01
;src/player.c:98: splitter->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
	ldhl	sp,	#12
	ld	a, c
	ld	(hl+), a
	ld	(hl), b
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	e, a
	sub	a, #0x50
	jr	NC, 00145$
	ld	a, e
	add	a, #0xf8
	jr	00146$
00145$:
	ld	a, e
	add	a, #0x08
00146$:
	ldhl	sp,	#12
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/player.c:99: splitter->pos.y = coin->pos.y;
	inc	bc
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(bc), a
00112$:
;src/player.c:101: if (!skulltron->entity.is_dead) {
	ldhl	sp,#29
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x000e
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00114$
;src/player.c:102: skulltron->can_move = true;
	ld	hl, #0x0010
	add	hl, bc
	ld	(hl), #0x01
;src/player.c:103: skulltron->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
	ldhl	sp,	#12
	ld	a, c
	ld	(hl+), a
	ld	(hl), b
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	e, a
	sub	a, #0x50
	jr	NC, 00147$
	ld	a, e
	add	a, #0xf8
	jr	00148$
00147$:
	ld	a, e
	add	a, #0x08
00148$:
	ldhl	sp,	#12
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/player.c:104: skulltron->pos.y = coin->pos.y;
	inc	bc
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(bc), a
00114$:
;src/player.c:106: if (!skullperior->entity.is_dead) {
	ldhl	sp,#31
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x000e
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00116$
;src/player.c:107: skullperior->can_move = true;
	ld	hl, #0x0010
	add	hl, bc
	ld	(hl), #0x01
;src/player.c:108: skullperior->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
	ldhl	sp,	#12
	ld	a, c
	ld	(hl+), a
	ld	(hl), b
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	e, a
	sub	a, #0x50
	jr	NC, 00149$
	ld	a, e
	add	a, #0xf8
	jr	00150$
00149$:
	ld	a, e
	add	a, #0x08
00150$:
	ldhl	sp,	#12
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/player.c:109: skullperior->pos.y = coin->pos.y;
	inc	bc
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(bc), a
00116$:
;src/player.c:111: if (!turret->entity.is_dead) {
	ldhl	sp,#35
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x000e
	add	hl, bc
	ld	e, (hl)
;src/player.c:113: turret->shoot_timer = 0;
	ld	hl, #0x0012
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#5
	ld	(hl+), a
	ld	(hl), d
;src/player.c:111: if (!turret->entity.is_dead) {
	ld	a, e
	or	a, a
	jr	NZ, 00118$
;src/player.c:112: turret->can_move = true;
	ld	hl, #0x0010
	add	hl, bc
	ld	(hl), #0x01
;src/player.c:113: turret->shoot_timer = 0;
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:114: turret->entity.can_shoot = true;
	ld	hl, #0x000d
	add	hl, bc
	ld	(hl), #0x01
;src/player.c:115: turret->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (SCREENWIDTH - 8));
	ldhl	sp,	#12
	ld	a, c
	ld	(hl+), a
	ld	(hl), b
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00151$
	ld	de, #0x0010
	jr	00152$
00151$:
	ld	de, #0xff98
00152$:
	ld	a, e
	ldhl	sp,	#12
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/player.c:116: turret->pos.y = coin->pos.y;
	inc	bc
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(bc), a
00118$:
;src/player.c:118: if (!turret2->entity.is_dead) {
	ldhl	sp,	#37
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#12
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jr	NZ, 00120$
;src/player.c:119: turret2->can_move = true;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x01
;src/player.c:120: turret->shoot_timer = 0;
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:121: turret2->entity.can_shoot = true;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000d
	add	hl, de
	ld	(hl), #0x01
;src/player.c:122: turret2->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (SCREENWIDTH - 8));
	ldhl	sp,#12
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00153$
	ld	de, #0x0010
	jr	00154$
00153$:
	ld	de, #0xff98
00154$:
	ld	a, e
	ld	(bc), a
;src/player.c:123: turret2->pos.y = coin->pos.y;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	inc	bc
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(bc), a
00120$:
;src/player.c:125: if (!heatball->entity.is_dead) {
	ldhl	sp,	#39
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#12
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00122$
;src/player.c:126: heatball->can_move = true;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x01
;src/player.c:127: heatball->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (160 - 16));
	ldhl	sp,#12
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00155$
	ld	de, #0x0010
	jr	00156$
00155$:
	ld	de, #0xff90
00156$:
	ld	a, e
	ld	(bc), a
;src/player.c:128: heatball->pos.y = 32;
	ldhl	sp,	#12
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	(hl), #0x20
00122$:
;src/player.c:130: if (!spike->entity.is_dead) {
	ldhl	sp,	#41
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#12
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00124$
;src/player.c:131: spike->can_move = true;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	(hl), #0x01
;src/player.c:132: spike->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (160 - 16));
	ldhl	sp,#12
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00157$
	ld	de, #0x0010
	jr	00158$
00157$:
	ld	de, #0xff90
00158$:
	ld	a, e
	ld	(bc), a
;src/player.c:133: spike->pos.y = (144 - 8);
	ldhl	sp,	#12
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	(hl), #0x88
00124$:
;src/player.c:136: player->is_dead = false;
	pop	de
	push	de
	ld	hl, #0x0015
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
	jp	00135$
00126$:
;src/player.c:138: BGP_REG = 0xE4;
	ld	a, #0xe4
	ldh	(_BGP_REG+0),a
;src/player.c:139: player->pos.x = 0;
	pop	hl
	push	hl
	ld	(hl), #0x00
;src/player.c:140: player->pos.y = 0;
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:141: player->entity.sprite.frame = S_NOTHING;
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x1f
;src/player.c:142: player->respawn_timer = 121;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x79
;src/player.c:143: player->lives = 4;
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x04
;src/player.c:144: player_draw(player);
	pop	hl
	push	hl
	push	hl
	call	_player_draw
	add	sp, #2
;src/player.c:145: hud_redraw(player->score, player->ammo, player->lives);
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	h, c
	ld	l, b
	push	hl
	push	af
	inc	sp
	call	_hud_redraw
	add	sp, #3
;src/player.c:148: return;
	jr	00135$
00131$:
;src/player.c:150: player_check_collisions(player, coin, beam_count, item,
	push	bc
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#43
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#44
	ld	a, (hl)
	push	af
	inc	sp
	ldhl	sp,	#41
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	push	bc
	call	_player_check_collisions
	add	sp, #25
	ldhl	sp,	#18
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_player_nextframe
	add	sp, #2
	ldhl	sp,	#18
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_player_draw
	add	sp, #2
	pop	bc
;src/player.c:157: if (bullet->disabled && !player->entity.can_shoot) 
	ldhl	sp,#20
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0005
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00135$
	ld	hl, #0x000f
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00135$
;src/player.c:158: player->entity.can_shoot = true;
	ld	(hl), #0x01
00135$:
;src/player.c:159: }
	add	sp, #14
	ret
;src/player.c:161: void player_shoot(player_t *player, bullet_t *bullet) {
;	---------------------------------
; Function player_shoot
; ---------------------------------
_player_shoot::
	add	sp, #-4
;src/player.c:162: bullet->pos.x = player->pos.x;
	ldhl	sp,#8
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	a, (de)
	ld	(bc), a
;src/player.c:163: bullet->pos.y = player->pos.y;
	ld	e, c
	ld	d, b
	inc	de
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ld	(de), a
;src/player.c:164: bullet->direction = player->entity.facing;
	ld	hl, #0x0004
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
	pop	de
	push	de
	ld	hl, #0x000b
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (de)
	ldhl	sp,	#2
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/player.c:165: bullet->sprite = S_BULLET;
	ld	l, c
	ld	h, b
	inc	hl
	inc	hl
	ld	(hl), #0x1c
;src/player.c:166: bullet->spd = 6;
	ld	l, c
	ld	h, b
	inc	hl
	inc	hl
	inc	hl
	ld	(hl), #0x06
;src/player.c:167: bullet->disabled = false;
	ld	hl, #0x0005
	add	hl, bc
	ld	(hl), #0x00
;src/player.c:169: bullet_spawn(bullet);
	push	bc
	call	_bullet_spawn
	add	sp, #2
;src/player.c:170: }
	add	sp, #4
	ret
;src/player.c:172: void player_check_collisions(player_t *player, coin_t *coin, UINT8 beam_count, item_t *item,
;	---------------------------------
; Function player_check_collisions
; ---------------------------------
_player_check_collisions::
	add	sp, #-60
;src/player.c:177: if (player->pos.x >= coin->pos.x - 4 &&
	ldhl	sp,	#62
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#64
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#2
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	inc	hl
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#6
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
;src/player.c:188: if (!drone->entity.is_dead)
	ldhl	sp,	#69
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), e
;src/player.c:190: if (!splitter->entity.is_dead)
	ldhl	sp,	#71
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#10
	ld	(hl+), a
	ld	(hl), e
;src/player.c:192: if (!skulltron->entity.is_dead)
	ldhl	sp,	#73
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#12
	ld	(hl+), a
	ld	(hl), e
;src/player.c:194: if (!skullperior->entity.is_dead)
	ldhl	sp,	#75
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#14
	ld	(hl+), a
	ld	(hl), e
;src/player.c:196: if (!enemy_bullet->disabled)
	ldhl	sp,	#77
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#16
	ld	(hl+), a
	ld	(hl), e
;src/player.c:198: if (!turret->entity.is_dead)
	ldhl	sp,	#79
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#18
	ld	(hl+), a
	ld	(hl), e
;src/player.c:200: if (!turret2->entity.is_dead)
	ldhl	sp,	#81
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#20
	ld	(hl+), a
	ld	(hl), e
;src/player.c:202: if (!heatball->entity.is_dead)
	ldhl	sp,	#83
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#22
	ld	(hl+), a
	ld	(hl), e
;src/player.c:204: if (!spike->entity.is_dead)
	ldhl	sp,	#85
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#24
	ld	(hl+), a
	ld	(hl), e
;src/player.c:216: if (item->type != i_disabled) item_destroy(item);
	ldhl	sp,	#67
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#26
	ld	(hl+), a
	ld	(hl), e
;src/player.c:224: player->pos.y >= item->pos.y - 4 &&
	pop	de
	push	de
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#28
	ld	(hl+), a
	ld	(hl), d
;src/player.c:230: if (player->lives < 3) {
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#30
	ld	(hl+), a
	ld	(hl), d
;src/player.c:235: if (player->ammo < 9) {
	pop	de
	push	de
	ld	hl, #0x0013
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#32
	ld	(hl+), a
	ld	(hl), d
;src/player.c:280: !skullperior->entity.is_dead
	ldhl	sp,#14
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#34
	ld	(hl+), a
	ld	(hl), d
;src/player.c:185: player->score++;
	pop	de
	push	de
	ld	hl, #0x0012
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#36
	ld	(hl+), a
	ld	(hl), d
;src/player.c:188: if (!drone->entity.is_dead)
	ldhl	sp,#8
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#38
	ld	(hl+), a
	ld	(hl), d
;src/player.c:190: if (!splitter->entity.is_dead)
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#40
	ld	(hl+), a
	ld	(hl), d
;src/player.c:192: if (!skulltron->entity.is_dead)
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#42
	ld	(hl+), a
	ld	(hl), d
;src/player.c:196: if (!enemy_bullet->disabled)
	ldhl	sp,#16
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0005
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#44
	ld	(hl+), a
	ld	(hl), d
;src/player.c:198: if (!turret->entity.is_dead)
	ldhl	sp,#18
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#46
	ld	(hl+), a
	ld	(hl), d
;src/player.c:200: if (!turret2->entity.is_dead)
	ldhl	sp,#20
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#48
	ld	(hl+), a
	ld	(hl), d
;src/player.c:202: if (!heatball->entity.is_dead)
	ldhl	sp,#22
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#50
	ld	(hl+), a
	ld	(hl), d
;src/player.c:204: if (!spike->entity.is_dead)
	ldhl	sp,#24
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#52
	ld	(hl+), a
	ld	(hl), d
;src/player.c:216: if (item->type != i_disabled) item_destroy(item);
	ldhl	sp,#26
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0002
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#54
	ld	(hl+), a
	ld	(hl), d
;src/player.c:177: if (player->pos.x >= coin->pos.x - 4 &&
	ldhl	sp,	#6
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00581$
	bit	7, d
	jr	NZ, 00582$
	cp	a, a
	jr	00582$
00581$:
	bit	7, d
	jr	Z, 00582$
	scf
00582$:
	jp	C, 00130$
;src/player.c:178: player->pos.y >= coin->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	pop	bc
	pop	hl
	push	hl
	push	bc
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#56
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00583$
	bit	7, d
	jr	NZ, 00584$
	cp	a, a
	jr	00584$
00583$:
	bit	7, d
	jr	Z, 00584$
	scf
00584$:
	jp	C, 00130$
;src/player.c:179: player->pos.x <= coin->pos.x + 8 &&
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#6
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00585$
	bit	7, d
	jr	NZ, 00586$
	cp	a, a
	jr	00586$
00585$:
	bit	7, d
	jr	Z, 00586$
	scf
00586$:
	jp	C, 00130$
;src/player.c:180: player->pos.y <= coin->pos.y + 8
	ldhl	sp,#56
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#58
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00587$
	bit	7, d
	jr	NZ, 00588$
	cp	a, a
	jr	00588$
00587$:
	bit	7, d
	jr	Z, 00588$
	scf
00588$:
	jp	C, 00130$
;src/player.c:182: BGP_REG = 0xFF;
	ld	a, #0xff
	ldh	(_BGP_REG+0),a
;src/player.c:183: player_set_check_point(player, &coin->pos);
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	c, l
	ld	b, h
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_player_set_check_point
	add	sp, #4
;src/player.c:184: coin_destroy(coin, &player->pos);
	pop	hl
	push	hl
	push	hl
	ldhl	sp,	#66
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_coin_destroy
	add	sp, #4
;src/player.c:185: player->score++;
	ldhl	sp,#36
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	inc	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:186: hud_redraw(player->score, player->ammo, player->lives);
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	push	bc
	inc	sp
	ld	b, a
	push	bc
	call	_hud_redraw
	add	sp, #3
;src/player.c:188: if (!drone->entity.is_dead)
	ldhl	sp,#38
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00102$
;src/player.c:189: drone_kill(drone);
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_drone_kill
	add	sp, #2
00102$:
;src/player.c:190: if (!splitter->entity.is_dead)
	ldhl	sp,#40
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00104$
;src/player.c:191: splitter_kill(splitter);
	ldhl	sp,	#10
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_splitter_kill
	add	sp, #2
00104$:
;src/player.c:192: if (!skulltron->entity.is_dead)
	ldhl	sp,#42
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00106$
;src/player.c:193: skulltron_kill(skulltron);
	ldhl	sp,	#12
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_skulltron_kill
	add	sp, #2
00106$:
;src/player.c:194: if (!skullperior->entity.is_dead)
	ldhl	sp,#34
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00108$
;src/player.c:195: skulltron_kill(skullperior);
	ldhl	sp,	#14
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_skulltron_kill
	add	sp, #2
00108$:
;src/player.c:196: if (!enemy_bullet->disabled)
	ldhl	sp,#44
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00110$
;src/player.c:197: bullet_destroy(enemy_bullet);
	ldhl	sp,	#16
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_bullet_destroy
	add	sp, #2
00110$:
;src/player.c:198: if (!turret->entity.is_dead)
	ldhl	sp,#46
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00112$
;src/player.c:199: turret_kill(turret);
	ldhl	sp,	#18
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_turret_kill
	add	sp, #2
00112$:
;src/player.c:200: if (!turret2->entity.is_dead)
	ldhl	sp,#48
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00114$
;src/player.c:201: turret_kill(turret2);
	ldhl	sp,	#20
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_turret_kill
	add	sp, #2
00114$:
;src/player.c:202: if (!heatball->entity.is_dead)
	ldhl	sp,#50
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00116$
;src/player.c:203: heatball_kill(heatball);
	ldhl	sp,	#22
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_heatball_kill
	add	sp, #2
00116$:
;src/player.c:204: if (!spike->entity.is_dead)
	ldhl	sp,#52
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00118$
;src/player.c:205: spike_kill(spike);
	ldhl	sp,	#24
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_spike_kill
	add	sp, #2
00118$:
;src/player.c:207: if (beam_count < 7) {
	ldhl	sp,	#66
	ld	a, (hl)
	sub	a, #0x07
	jr	NC, 00125$
;src/player.c:208: game_new_beam();
	call	_game_new_beam
	jr	00126$
00125$:
;src/player.c:185: player->score++;
	ldhl	sp,#36
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
;src/player.c:210: if (player->score == 27)
	cp	a, #0x1b
	jr	NZ, 00122$
;src/player.c:211: game_beam_speed_medium();
	call	_game_beam_speed_medium
	jr	00126$
00122$:
;src/player.c:212: else if (player->score == 124)
	sub	a, #0x7c
	jr	NZ, 00126$
;src/player.c:213: game_beam_speed_hard();
	call	_game_beam_speed_hard
00126$:
;src/player.c:216: if (item->type != i_disabled) item_destroy(item);
	ldhl	sp,#54
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	Z, 00128$
	ldhl	sp,	#26
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_item_destroy
	add	sp, #2
00128$:
;src/player.c:218: game_spawn_item();
	call	_game_spawn_item
;src/player.c:219: game_next_enemy();
	call	_game_next_enemy
;src/player.c:220: BGP_REG = 0xE4;
	ld	a, #0xe4
	ldh	(_BGP_REG+0),a
00130$:
;src/player.c:223: if (player->pos.x >= item->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#26
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#4
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#6
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00593$
	bit	7, d
	jr	NZ, 00594$
	cp	a, a
	jr	00594$
00593$:
	bit	7, d
	jr	Z, 00594$
	scf
00594$:
	jp	C, 00144$
;src/player.c:224: player->pos.y >= item->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#26
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#56
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00595$
	bit	7, d
	jr	NZ, 00596$
	cp	a, a
	jr	00596$
00595$:
	bit	7, d
	jr	Z, 00596$
	scf
00596$:
	jp	C, 00144$
;src/player.c:225: player->pos.x <= item->pos.x + 12 &&
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000c
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#6
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00597$
	bit	7, d
	jr	NZ, 00598$
	cp	a, a
	jr	00598$
00597$:
	bit	7, d
	jr	Z, 00598$
	scf
00598$:
	jp	C, 00144$
;src/player.c:226: player->pos.y <= item->pos.y + 12 &&
	ldhl	sp,#56
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000c
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#58
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00599$
	bit	7, d
	jr	NZ, 00600$
	cp	a, a
	jr	00600$
00599$:
	bit	7, d
	jr	Z, 00600$
	scf
00600$:
	jr	C, 00144$
;src/player.c:227: item->type != i_disabled
	ldhl	sp,#54
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ld	a, (hl)
	or	a, a
	jr	Z, 00144$
;src/player.c:229: if (item->type == i_extralife) {
	ld	a, (hl)
	sub	a, #0x02
	jr	NZ, 00141$
;src/player.c:230: if (player->lives < 3) {
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	cp	a, #0x03
	jr	NC, 00142$
;src/player.c:231: player->lives++;
	ld	c, a
	inc	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:232: item_destroy(item);
	ldhl	sp,	#26
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_item_destroy
	add	sp, #2
	jr	00142$
00141$:
;src/player.c:234: } else if (item->type == i_ammo) {
	ldhl	sp,	#59
	ld	a, (hl)
	dec	a
	jr	NZ, 00142$
;src/player.c:235: if (player->ammo < 9) {
	ldhl	sp,#32
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	cp	a, #0x09
	jr	NC, 00142$
;src/player.c:236: player->ammo++;
	ld	c, a
	inc	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:237: item_destroy(item);
	ldhl	sp,	#26
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_item_destroy
	add	sp, #2
00142$:
;src/player.c:240: hud_redraw(player->score, player->ammo, player->lives);
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
	ldhl	sp,#36
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	h, c
	ld	l, b
	push	hl
	push	af
	inc	sp
	call	_hud_redraw
	add	sp, #3
00144$:
;src/player.c:243: if (player->pos.x >= drone->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#8
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#32
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#36
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
;src/player.c:251: player->is_dead = true;
	pop	de
	push	de
	ld	hl, #0x0015
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#54
	ld	(hl+), a
	ld	(hl), d
;src/player.c:243: if (player->pos.x >= drone->pos.x - 4 &&
	ldhl	sp,	#36
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00605$
	bit	7, d
	jr	NZ, 00606$
	cp	a, a
	jr	00606$
00605$:
	bit	7, d
	jr	Z, 00606$
	scf
00606$:
	jp	C, 00150$
;src/player.c:244: player->pos.y >= drone->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#56
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00607$
	bit	7, d
	jr	NZ, 00608$
	cp	a, a
	jr	00608$
00607$:
	bit	7, d
	jr	Z, 00608$
	scf
00608$:
	jr	C, 00150$
;src/player.c:245: player->pos.x <= drone->pos.x + 7 &&
	ldhl	sp,#32
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0007
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#36
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00609$
	bit	7, d
	jr	NZ, 00610$
	cp	a, a
	jr	00610$
00609$:
	bit	7, d
	jr	Z, 00610$
	scf
00610$:
	jr	C, 00150$
;src/player.c:246: player->pos.y <= drone->pos.y + 8 && 
	ldhl	sp,#56
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#58
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00611$
	bit	7, d
	jr	NZ, 00612$
	cp	a, a
	jr	00612$
00611$:
	bit	7, d
	jr	Z, 00612$
	scf
00612$:
	jr	C, 00150$
;src/player.c:247: !drone->entity.is_dead
	ldhl	sp,#38
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00150$
;src/player.c:249: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:250: drone->can_move = false;
	ldhl	sp,#8
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
;src/player.c:251: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00150$:
;src/player.c:254: if (player->pos.x >= splitter->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#36
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#38
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00613$
	bit	7, d
	jr	NZ, 00614$
	cp	a, a
	jr	00614$
00613$:
	bit	7, d
	jr	Z, 00614$
	scf
00614$:
	jp	C, 00156$
;src/player.c:255: player->pos.y >= splitter->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#10
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#56
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00615$
	bit	7, d
	jr	NZ, 00616$
	cp	a, a
	jr	00616$
00615$:
	bit	7, d
	jr	Z, 00616$
	scf
00616$:
	jr	C, 00156$
;src/player.c:256: player->pos.x <= splitter->pos.x + 7 &&
	ldhl	sp,#36
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0007
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#38
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00617$
	bit	7, d
	jr	NZ, 00618$
	cp	a, a
	jr	00618$
00617$:
	bit	7, d
	jr	Z, 00618$
	scf
00618$:
	jr	C, 00156$
;src/player.c:257: player->pos.y <= splitter->pos.y + 8 &&
	ldhl	sp,#56
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#58
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00619$
	bit	7, d
	jr	NZ, 00620$
	cp	a, a
	jr	00620$
00619$:
	bit	7, d
	jr	Z, 00620$
	scf
00620$:
	jr	C, 00156$
;src/player.c:258: !splitter->entity.is_dead
	ldhl	sp,#40
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00156$
;src/player.c:260: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:261: splitter->can_move = false;
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
;src/player.c:262: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00156$:
;src/player.c:265: if (player->pos.x >= skulltron->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#38
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#40
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00621$
	bit	7, d
	jr	NZ, 00622$
	cp	a, a
	jr	00622$
00621$:
	bit	7, d
	jr	Z, 00622$
	scf
00622$:
	jp	C, 00162$
;src/player.c:266: player->pos.y >= skulltron->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#12
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#56
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00623$
	bit	7, d
	jr	NZ, 00624$
	cp	a, a
	jr	00624$
00623$:
	bit	7, d
	jr	Z, 00624$
	scf
00624$:
	jr	C, 00162$
;src/player.c:267: player->pos.x <= skulltron->pos.x + 7 &&
	ldhl	sp,#38
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0007
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#40
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00625$
	bit	7, d
	jr	NZ, 00626$
	cp	a, a
	jr	00626$
00625$:
	bit	7, d
	jr	Z, 00626$
	scf
00626$:
	jr	C, 00162$
;src/player.c:268: player->pos.y <= skulltron->pos.y + 8 &&
	ldhl	sp,#56
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#58
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00627$
	bit	7, d
	jr	NZ, 00628$
	cp	a, a
	jr	00628$
00627$:
	bit	7, d
	jr	Z, 00628$
	scf
00628$:
	jr	C, 00162$
;src/player.c:269: !skulltron->entity.is_dead
	ldhl	sp,#42
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00162$
;src/player.c:271: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:272: skulltron->can_move = false;
	ldhl	sp,#12
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
;src/player.c:273: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00162$:
;src/player.c:276: if (player->pos.x >= skullperior->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#14
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#40
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#42
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
;src/player.c:283: skullperior->can_move = false;
	ldhl	sp,#14
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#58
	ld	(hl+), a
	ld	(hl), d
;src/player.c:276: if (player->pos.x >= skullperior->pos.x - 4 &&
	ldhl	sp,	#42
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00629$
	bit	7, d
	jr	NZ, 00630$
	cp	a, a
	jr	00630$
00629$:
	bit	7, d
	jr	Z, 00630$
	scf
00630$:
	jp	C, 00168$
;src/player.c:277: player->pos.y >= skullperior->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#57
	ld	(hl), a
	ldhl	sp,	#14
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#38
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#57
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00631$
	bit	7, d
	jr	NZ, 00632$
	cp	a, a
	jr	00632$
00631$:
	bit	7, d
	jr	Z, 00632$
	scf
00632$:
	jr	C, 00168$
;src/player.c:278: player->pos.x <= skullperior->pos.x + 7 &&
	ldhl	sp,#40
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0007
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#42
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00633$
	bit	7, d
	jr	NZ, 00634$
	cp	a, a
	jr	00634$
00633$:
	bit	7, d
	jr	Z, 00634$
	scf
00634$:
	jr	C, 00168$
;src/player.c:279: player->pos.y <= skullperior->pos.y + 8 &&
	ldhl	sp,#38
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#56
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00635$
	bit	7, d
	jr	NZ, 00636$
	cp	a, a
	jr	00636$
00635$:
	bit	7, d
	jr	Z, 00636$
	scf
00636$:
	jr	C, 00168$
;src/player.c:280: !skullperior->entity.is_dead
	ldhl	sp,#34
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00168$
;src/player.c:282: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:283: skullperior->can_move = false;
	ldhl	sp,	#58
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:284: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00168$:
;src/player.c:287: if (!enemy_bullet->disabled &&
	ldhl	sp,#44
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
;src/player.c:295: if (!turret->entity.is_dead) turret->entity.can_shoot = false;
	ldhl	sp,#18
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000d
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#36
	ld	(hl+), a
	ld	(hl), d
;src/player.c:296: if (!turret2->entity.is_dead) turret2->entity.can_shoot = false;
	ldhl	sp,#20
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000d
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#38
	ld	(hl+), a
	ld	(hl), d
;src/player.c:287: if (!enemy_bullet->disabled &&
	ld	a, c
	or	a, a
	jp	NZ, 00180$
;src/player.c:288: player->pos.x >= enemy_bullet->pos.x - 1 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#57
	ld	(hl), a
	ldhl	sp,#16
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#40
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	dec	bc
	ldhl	sp,	#57
	ld	a, (hl)
	ldhl	sp,	#42
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00637$
	bit	7, d
	jr	NZ, 00638$
	cp	a, a
	jr	00638$
00637$:
	bit	7, d
	jr	Z, 00638$
	scf
00638$:
	jp	C, 00180$
;src/player.c:289: player->pos.y >= enemy_bullet->pos.y - 1 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#57
	ld	(hl), a
	ldhl	sp,	#16
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#44
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	dec	bc
	ldhl	sp,	#57
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00639$
	bit	7, d
	jr	NZ, 00640$
	cp	a, a
	jr	00640$
00639$:
	bit	7, d
	jr	Z, 00640$
	scf
00640$:
	jp	C, 00180$
;src/player.c:290: player->pos.x <= enemy_bullet->pos.x + 8 &&
	ldhl	sp,#40
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#42
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00641$
	bit	7, d
	jr	NZ, 00642$
	cp	a, a
	jr	00642$
00641$:
	bit	7, d
	jr	Z, 00642$
	scf
00642$:
	jr	C, 00180$
;src/player.c:291: player->pos.y <= enemy_bullet->pos.y + 8
	ldhl	sp,#44
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#56
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00643$
	bit	7, d
	jr	NZ, 00644$
	cp	a, a
	jr	00644$
00643$:
	bit	7, d
	jr	Z, 00644$
	scf
00644$:
	jr	C, 00180$
;src/player.c:293: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:294: if (!skullperior->entity.is_dead) skullperior->can_move = false;
	ldhl	sp,#34
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#57
	ld	(hl), a
	ld	a, (hl)
	or	a, a
	jr	NZ, 00174$
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
00174$:
;src/player.c:295: if (!turret->entity.is_dead) turret->entity.can_shoot = false;
	ldhl	sp,#46
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00176$
	ldhl	sp,	#36
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
00176$:
;src/player.c:296: if (!turret2->entity.is_dead) turret2->entity.can_shoot = false;
	ldhl	sp,#48
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00178$
	ldhl	sp,	#38
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
00178$:
;src/player.c:297: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/player.c:299: bullet_destroy(enemy_bullet);
	ldhl	sp,	#16
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_bullet_destroy
	add	sp, #2
00180$:
;src/player.c:302: if (player->pos.x >= turret->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#18
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#42
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#44
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00645$
	bit	7, d
	jr	NZ, 00646$
	cp	a, a
	jr	00646$
00645$:
	bit	7, d
	jr	Z, 00646$
	scf
00646$:
	jp	C, 00186$
;src/player.c:303: player->pos.y >= turret->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#18
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#56
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00647$
	bit	7, d
	jr	NZ, 00648$
	cp	a, a
	jr	00648$
00647$:
	bit	7, d
	jr	Z, 00648$
	scf
00648$:
	jp	C, 00186$
;src/player.c:304: player->pos.x <= turret->pos.x + 7 &&
	ldhl	sp,#42
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0007
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#44
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00649$
	bit	7, d
	jr	NZ, 00650$
	cp	a, a
	jr	00650$
00649$:
	bit	7, d
	jr	Z, 00650$
	scf
00650$:
	jr	C, 00186$
;src/player.c:305: player->pos.y <= turret->pos.y + 8 &&
	ldhl	sp,#56
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#58
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00651$
	bit	7, d
	jr	NZ, 00652$
	cp	a, a
	jr	00652$
00651$:
	bit	7, d
	jr	Z, 00652$
	scf
00652$:
	jr	C, 00186$
;src/player.c:306: !turret->entity.is_dead
	ldhl	sp,#46
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00186$
;src/player.c:308: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:309: turret->can_move = false;
	ldhl	sp,#18
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
;src/player.c:310: turret->entity.can_shoot = false;
	ldhl	sp,	#36
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:311: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00186$:
;src/player.c:314: if (player->pos.x >= turret2->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#20
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#44
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#46
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00653$
	bit	7, d
	jr	NZ, 00654$
	cp	a, a
	jr	00654$
00653$:
	bit	7, d
	jr	Z, 00654$
	scf
00654$:
	jp	C, 00192$
;src/player.c:315: player->pos.y >= turret2->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#20
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#56
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00655$
	bit	7, d
	jr	NZ, 00656$
	cp	a, a
	jr	00656$
00655$:
	bit	7, d
	jr	Z, 00656$
	scf
00656$:
	jp	C, 00192$
;src/player.c:316: player->pos.x <= turret2->pos.x + 7 &&
	ldhl	sp,#44
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0007
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#46
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00657$
	bit	7, d
	jr	NZ, 00658$
	cp	a, a
	jr	00658$
00657$:
	bit	7, d
	jr	Z, 00658$
	scf
00658$:
	jr	C, 00192$
;src/player.c:317: player->pos.y <= turret2->pos.y + 8 &&
	ldhl	sp,#56
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#58
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00659$
	bit	7, d
	jr	NZ, 00660$
	cp	a, a
	jr	00660$
00659$:
	bit	7, d
	jr	Z, 00660$
	scf
00660$:
	jr	C, 00192$
;src/player.c:318: !turret2->entity.is_dead
	ldhl	sp,#48
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00192$
;src/player.c:320: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:321: turret2->can_move = false;
	ldhl	sp,#20
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#58
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:322: turret2->entity.can_shoot = false;
	ldhl	sp,	#38
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:323: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00192$:
;src/player.c:326: if (player->pos.x >= heatball->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#22
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#44
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#46
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
;src/player.c:333: heatball->can_move = false;
	ldhl	sp,#22
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#48
	ld	(hl+), a
	ld	(hl), d
;src/player.c:334: spike->can_move = false;
	ldhl	sp,#24
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0010
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#57
	ld	(hl+), a
	ld	(hl), d
;src/player.c:326: if (player->pos.x >= heatball->pos.x - 4 &&
	ldhl	sp,	#46
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00661$
	bit	7, d
	jr	NZ, 00662$
	cp	a, a
	jr	00662$
00661$:
	bit	7, d
	jr	Z, 00662$
	scf
00662$:
	jp	C, 00198$
;src/player.c:327: player->pos.y >= heatball->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#22
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#40
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#42
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00663$
	bit	7, d
	jr	NZ, 00664$
	cp	a, a
	jr	00664$
00663$:
	bit	7, d
	jr	Z, 00664$
	scf
00664$:
	jr	C, 00198$
;src/player.c:328: player->pos.x <= heatball->pos.x + 7 &&
	ldhl	sp,#44
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0007
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#46
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00665$
	bit	7, d
	jr	NZ, 00666$
	cp	a, a
	jr	00666$
00665$:
	bit	7, d
	jr	Z, 00666$
	scf
00666$:
	jr	C, 00198$
;src/player.c:329: player->pos.y <= heatball->pos.y + 8 &&
	ldhl	sp,#40
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#42
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00667$
	bit	7, d
	jr	NZ, 00668$
	cp	a, a
	jr	00668$
00667$:
	bit	7, d
	jr	Z, 00668$
	scf
00668$:
	jr	C, 00198$
;src/player.c:330: !heatball->entity.is_dead
	ldhl	sp,#50
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00198$
;src/player.c:332: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:333: heatball->can_move = false;
	ldhl	sp,	#48
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:334: spike->can_move = false;
	ldhl	sp,	#57
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:335: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00198$:
;src/player.c:338: if (player->pos.x >= spike->pos.x - 4 &&
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,#24
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#46
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#50
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00669$
	bit	7, d
	jr	NZ, 00670$
	cp	a, a
	jr	00670$
00669$:
	bit	7, d
	jr	Z, 00670$
	scf
00670$:
	jp	C, 00209$
;src/player.c:339: player->pos.y >= spike->pos.y - 4 &&
	ldhl	sp,#28
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#59
	ld	(hl), a
	ldhl	sp,	#24
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#42
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0004
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ld	b, a
	ld	c, e
	ldhl	sp,	#59
	ld	a, (hl)
	ldhl	sp,	#44
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl)
	sub	a, c
	inc	hl
	ld	a, (hl)
	sbc	a, b
	ld	d, (hl)
	ld	a, b
	bit	7,a
	jr	Z, 00671$
	bit	7, d
	jr	NZ, 00672$
	cp	a, a
	jr	00672$
00671$:
	bit	7, d
	jr	Z, 00672$
	scf
00672$:
	jr	C, 00209$
;src/player.c:340: player->pos.x <= spike->pos.x + 7 &&
	ldhl	sp,#46
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0007
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#50
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00673$
	bit	7, d
	jr	NZ, 00674$
	cp	a, a
	jr	00674$
00673$:
	bit	7, d
	jr	Z, 00674$
	scf
00674$:
	jr	C, 00209$
;src/player.c:341: player->pos.y <= spike->pos.y + 8 &&
	ldhl	sp,#42
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#44
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00675$
	bit	7, d
	jr	NZ, 00676$
	cp	a, a
	jr	00676$
00675$:
	bit	7, d
	jr	Z, 00676$
	scf
00676$:
	jr	C, 00209$
;src/player.c:342: !spike->entity.is_dead
	ldhl	sp,#52
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	NZ, 00209$
;src/player.c:344: player->lives--;
	ldhl	sp,#30
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/player.c:345: heatball->can_move = false;
	ldhl	sp,	#48
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:346: spike->can_move = false;
	ldhl	sp,	#57
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/player.c:347: player->is_dead = true;
	ldhl	sp,	#54
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00209$:
;src/player.c:349: }
	add	sp, #60
	ret
;src/player.c:351: void player_set_check_point(player_t *player, POINT *pos) {
;	---------------------------------
; Function player_set_check_point
; ---------------------------------
_player_set_check_point::
	add	sp, #-2
;src/player.c:352: player->check_point.x = pos->x;
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	bc
	push	bc
	inc	bc
	inc	bc
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(bc), a
;src/player.c:353: player->check_point.y = pos->y;
	pop	bc
	push	bc
	inc	bc
	inc	bc
	inc	bc
	inc	de
	ld	a, (de)
	ld	(bc), a
;src/player.c:354: }
	add	sp, #2
	ret
;src/player.c:357: void player_nextframe(player_t *player) {
;	---------------------------------
; Function player_nextframe
; ---------------------------------
_player_nextframe::
;src/player.c:358: if (global_frame % 3 != 0 || !player->entity.is_moving) return;
	ld	hl, #_global_frame
	ld	c, (hl)
	ld	b, #0x00
	ld	hl, #0x0003
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	ld	a, d
	or	a, e
	ret	NZ
	ldhl	sp,#2
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x000d
	add	hl, bc
	ld	a, (hl)
	or	a, a
	ret	Z
;src/player.c:359: if (player->entity.facing == fa_left) {
	ld	hl, #0x000b
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00105$
;src/player.c:360: min_frame = 0;
	ld	hl, #_min_frame
	ld	(hl), #0x00
;src/player.c:361: max_frame = 3;
	ld	hl, #_max_frame
	ld	(hl), #0x03
	jr	00106$
00105$:
;src/player.c:363: min_frame = 4;
	ld	hl, #_min_frame
	ld	(hl), #0x04
;src/player.c:364: max_frame = 7;
	ld	hl, #_max_frame
	ld	(hl), #0x07
00106$:
;src/player.c:366: total_frames = max_frame - min_frame;
	ld	a, (#_max_frame)
	ld	hl, #_min_frame
	sub	a, (hl)
	ld	(#_total_frames),a
;src/player.c:367: if (player->entity.sprite.frame < max_frame)
	ld	hl, #0x000a
	add	hl, bc
	ld	c, l
	ld	b, h
	ld	a, (bc)
	ld	e, a
	ld	hl, #_max_frame
	sub	a, (hl)
	jr	NC, 00108$
;src/player.c:368: player->entity.sprite.frame++;
	ld	a, e
	inc	a
	ld	(bc), a
	ret
00108$:
;src/player.c:370: player->entity.sprite.frame = min_frame;
	ld	a, (#_min_frame)
	ld	(bc), a
;src/player.c:371: }
	ret
;src/player.c:373: void player_draw(player_t *player) {
;	---------------------------------
; Function player_draw
; ---------------------------------
_player_draw::
	dec	sp
;src/player.c:374: set_sprite_tile(S_PLAYER, player->entity.sprite.frame);
	ldhl	sp,#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	bc, #_shadow_OAM+2
	ldhl	sp,	#0
	ld	a, (hl)
	ld	(bc), a
;src/player.c:375: move_sprite(S_PLAYER, player->pos.x, player->pos.y);
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #_shadow_OAM
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/player.c:375: move_sprite(S_PLAYER, player->pos.x, player->pos.y);
;src/player.c:376: }
	inc	sp
	ret
	.area _CODE
	.area _CABS (ABS)
