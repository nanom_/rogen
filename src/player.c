#include <gb/gb.h>
#include "main.h"
#include "game.h"
#include "hud.h"
#include "coin.h"
#include "bullet.h"
#include "drone.h"
#include "splitter.h"
#include "skulltron.h"
#include "turret.h"
#include "heatball.h"
#include "spike.h"
#include "item.h"
#include "player.h"

void player_spawn(player_t *player) {
    set_sprite_tile(S_PLAYER, 0);
    player_draw(player);
}

void player_input(player_t *player, bullet_t *bullet) {
    if (player->is_dead) return;
    if (joypad() & J_A) {
        if (player->ammo > 0 && player->entity.can_shoot && bullet->disabled) {
            player_shoot(player, bullet);
            player->ammo--;
            player->entity.can_shoot = false;
            hud_redraw(player->score, player->ammo, player->lives);
        }
    }
    if (!player->can_move) {
        player->entity.is_moving = false;
        return;
    }
    if (joypad() & J_LEFT) {
        if (player->pos.x > 16) {
            player->pos.x -= player->entity.spd;
            player->entity.is_moving = true;
            if (player->entity.facing == fa_right)
                player->entity.sprite.frame = 0;
            player->entity.facing = fa_left;
        }
    }
    if (joypad() & J_RIGHT) {
        if (player->pos.x < SCREENWIDTH - 8) {
            player->pos.x += player->entity.spd;
            player->entity.is_moving = true;
            if (player->entity.facing == fa_left)
                player->entity.sprite.frame = 4;
            player->entity.facing = fa_right;
        }
    }
    if (joypad() & J_DOWN) {
        if (player->pos.y < SCREENHEIGHT) {
            player->pos.y += player->entity.spd;
            player->entity.is_moving = true;
        }
    }
    if (joypad() & J_UP) {
        if (player->pos.y > 32) {
            player->pos.y -= player->entity.spd;
            player->entity.is_moving = true;
        }
    }
    if (!joypad())
        player->entity.is_moving = false;
}

void player_update(player_t *player, coin_t *coin, bullet_t *bullet, UINT8 beam_count,
    item_t *item, drone_t *drone, splitter_t *splitter, skulltron_t *skulltron,
    skulltron_t *skullperior, bullet_t *enemy_bullet, turret_t *turret, turret_t *turret2,
    heatball_t *heatball, spike_t *spike
) {
    if (player->is_dead && player->respawn_timer == 121) return;
    if (player->is_dead) {
        if (player->respawn_timer % 10 !=0 || player->entity.sprite.frame < 8) {
            player->entity.sprite.frame = (player->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);
            if (player->entity.sprite.frame == S_EXPLO2)
                BGP_REG = (BGP_REG != 0xFE ? 0xFE : 0xE4);
        }
        player_draw(player);
        player->respawn_timer++;
        if (player->respawn_timer == 120) {
            if (player->lives > 0) {
                BGP_REG = 0xE4;
                hud_redraw(player->score, player->ammo, player->lives);
                player->respawn_timer = 0;
                player->pos.x = player->check_point.x;
                player->pos.y = player->check_point.y;
                player->entity.sprite.frame = (player->entity.facing == fa_left ? 0 : 4);
                if (!drone->entity.is_dead) {
                    drone->can_move = true;
                    drone->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
                    drone->pos.y = coin->pos.y;
                }
                if (!splitter->entity.is_dead) {
                    splitter->can_move = true;
                    splitter->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
                    splitter->pos.y = coin->pos.y;
                }
                if (!skulltron->entity.is_dead) {
                    skulltron->can_move = true;
                    skulltron->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
                    skulltron->pos.y = coin->pos.y;
                }
                if (!skullperior->entity.is_dead) {
                    skullperior->can_move = true;
                    skullperior->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x - 8 : coin->pos.x + 8);
                    skullperior->pos.y = coin->pos.y;
                }
                if (!turret->entity.is_dead) {
                    turret->can_move = true;
                    turret->shoot_timer = 0;
                    turret->entity.can_shoot = true;
                    turret->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (SCREENWIDTH - 8));
                    turret->pos.y = coin->pos.y;
                }
                if (!turret2->entity.is_dead) {
                    turret2->can_move = true;
                    turret->shoot_timer = 0;
                    turret2->entity.can_shoot = true;
                    turret2->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (SCREENWIDTH - 8));
                    turret2->pos.y = coin->pos.y;
                }
                if (!heatball->entity.is_dead) {
                    heatball->can_move = true;
                    heatball->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (160 - 16));
                    heatball->pos.y = 32;
                }
                if (!spike->entity.is_dead) {
                    spike->can_move = true;
                    spike->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (160 - 16));
                    spike->pos.y = (144 - 8);
                }

                player->is_dead = false;
            } else {
                BGP_REG = 0xE4;
                player->pos.x = 0;
                player->pos.y = 0;
                player->entity.sprite.frame = S_NOTHING;
                player->respawn_timer = 121;
                player->lives = 4;
                player_draw(player);
                hud_redraw(player->score, player->ammo, player->lives);
            }
        }
        return;
    }
    player_check_collisions(player, coin, beam_count, item,
        drone, splitter, skulltron, skullperior,
        enemy_bullet, turret, turret2,
        heatball, spike);
    player_nextframe(player);
    player_draw(player);

    if (bullet->disabled && !player->entity.can_shoot) 
        player->entity.can_shoot = true;
}

void player_shoot(player_t *player, bullet_t *bullet) {
    bullet->pos.x = player->pos.x;
    bullet->pos.y = player->pos.y;
    bullet->direction = player->entity.facing;
    bullet->sprite = S_BULLET;
    bullet->spd = 6;
    bullet->disabled = false;

    bullet_spawn(bullet);
}

void player_check_collisions(player_t *player, coin_t *coin, UINT8 beam_count, item_t *item,
    drone_t *drone, splitter_t *splitter, skulltron_t *skulltron, skulltron_t *skullperior,
    bullet_t *enemy_bullet, turret_t *turret, turret_t *turret2, heatball_t *heatball,
    spike_t *spike
) {
    if (player->pos.x >= coin->pos.x - 4 &&
        player->pos.y >= coin->pos.y - 4 &&
        player->pos.x <= coin->pos.x + 8 &&
        player->pos.y <= coin->pos.y + 8
    ) {
        BGP_REG = 0xFF;
        player_set_check_point(player, &coin->pos);
        coin_destroy(coin, &player->pos);
        player->score++;
        hud_redraw(player->score, player->ammo, player->lives);

        if (!drone->entity.is_dead)
            drone_kill(drone);
        if (!splitter->entity.is_dead)
            splitter_kill(splitter);
        if (!skulltron->entity.is_dead)
            skulltron_kill(skulltron);
        if (!skullperior->entity.is_dead)
            skulltron_kill(skullperior);
        if (!enemy_bullet->disabled)
            bullet_destroy(enemy_bullet);
        if (!turret->entity.is_dead)
            turret_kill(turret);
        if (!turret2->entity.is_dead)
            turret_kill(turret2);
        if (!heatball->entity.is_dead)
            heatball_kill(heatball);
        if (!spike->entity.is_dead)
            spike_kill(spike);

        if (beam_count < 7) {
            game_new_beam();
        } else {
            if (player->score == 27)
                game_beam_speed_medium();
            else if (player->score == 124)
                game_beam_speed_hard();
        }
        
        if (item->type != i_disabled) item_destroy(item);

        game_spawn_item();
        game_next_enemy();
        BGP_REG = 0xE4;
    }

    if (player->pos.x >= item->pos.x - 4 &&
        player->pos.y >= item->pos.y - 4 &&
        player->pos.x <= item->pos.x + 12 &&
        player->pos.y <= item->pos.y + 12 &&
        item->type != i_disabled
    ) {
        if (item->type == i_extralife) {
            if (player->lives < 3) {
                player->lives++;
                item_destroy(item);
            }
        } else if (item->type == i_ammo) {
            if (player->ammo < 9) {
                player->ammo++;
                item_destroy(item);
            }
        }
        hud_redraw(player->score, player->ammo, player->lives);
    }

    if (player->pos.x >= drone->pos.x - 4 &&
        player->pos.y >= drone->pos.y - 4 &&
        player->pos.x <= drone->pos.x + 7 &&
        player->pos.y <= drone->pos.y + 8 && 
        !drone->entity.is_dead
    ) {
        player->lives--;
        drone->can_move = false;
        player->is_dead = true;
    }

    if (player->pos.x >= splitter->pos.x - 4 &&
        player->pos.y >= splitter->pos.y - 4 &&
        player->pos.x <= splitter->pos.x + 7 &&
        player->pos.y <= splitter->pos.y + 8 &&
        !splitter->entity.is_dead
    ) {
        player->lives--;
        splitter->can_move = false;
        player->is_dead = true;
    }

    if (player->pos.x >= skulltron->pos.x - 4 &&
        player->pos.y >= skulltron->pos.y - 4 &&
        player->pos.x <= skulltron->pos.x + 7 &&
        player->pos.y <= skulltron->pos.y + 8 &&
        !skulltron->entity.is_dead
    ) {
        player->lives--;
        skulltron->can_move = false;
        player->is_dead = true;
    }

    if (player->pos.x >= skullperior->pos.x - 4 &&
        player->pos.y >= skullperior->pos.y - 4 &&
        player->pos.x <= skullperior->pos.x + 7 &&
        player->pos.y <= skullperior->pos.y + 8 &&
        !skullperior->entity.is_dead
    ) {
        player->lives--;
        skullperior->can_move = false;
        player->is_dead = true;
    }

    if (!enemy_bullet->disabled &&
        player->pos.x >= enemy_bullet->pos.x - 1 &&
        player->pos.y >= enemy_bullet->pos.y - 1 &&
        player->pos.x <= enemy_bullet->pos.x + 8 &&
        player->pos.y <= enemy_bullet->pos.y + 8
    ) {
        player->lives--;
        if (!skullperior->entity.is_dead) skullperior->can_move = false;
        if (!turret->entity.is_dead) turret->entity.can_shoot = false;
        if (!turret2->entity.is_dead) turret2->entity.can_shoot = false;
        player->is_dead = true;

        bullet_destroy(enemy_bullet);
    }

    if (player->pos.x >= turret->pos.x - 4 &&
        player->pos.y >= turret->pos.y - 4 &&
        player->pos.x <= turret->pos.x + 7 &&
        player->pos.y <= turret->pos.y + 8 &&
        !turret->entity.is_dead
    ) {
        player->lives--;
        turret->can_move = false;
        turret->entity.can_shoot = false;
        player->is_dead = true;
    }

    if (player->pos.x >= turret2->pos.x - 4 &&
        player->pos.y >= turret2->pos.y - 4 &&
        player->pos.x <= turret2->pos.x + 7 &&
        player->pos.y <= turret2->pos.y + 8 &&
        !turret2->entity.is_dead
    ) {
        player->lives--;
        turret2->can_move = false;
        turret2->entity.can_shoot = false;
        player->is_dead = true;
    }

    if (player->pos.x >= heatball->pos.x - 4 &&
        player->pos.y >= heatball->pos.y - 4 &&
        player->pos.x <= heatball->pos.x + 7 &&
        player->pos.y <= heatball->pos.y + 8 &&
        !heatball->entity.is_dead
    ) {
        player->lives--;
        heatball->can_move = false;
        spike->can_move = false;
        player->is_dead = true;
    }

    if (player->pos.x >= spike->pos.x - 4 &&
        player->pos.y >= spike->pos.y - 4 &&
        player->pos.x <= spike->pos.x + 7 &&
        player->pos.y <= spike->pos.y + 8 &&
        !spike->entity.is_dead
    ) {
        player->lives--;
        heatball->can_move = false;
        spike->can_move = false;
        player->is_dead = true;
    }
}

void player_set_check_point(player_t *player, POINT *pos) {
    player->check_point.x = pos->x;
    player->check_point.y = pos->y;
}

UINT8 min_frame, max_frame, total_frames;
void player_nextframe(player_t *player) {
    if (global_frame % 3 != 0 || !player->entity.is_moving) return;
    if (player->entity.facing == fa_left) {
        min_frame = 0;
        max_frame = 3;
    } else {
        min_frame = 4;
        max_frame = 7;
    }
    total_frames = max_frame - min_frame;
    if (player->entity.sprite.frame < max_frame)
        player->entity.sprite.frame++;
    else
        player->entity.sprite.frame = min_frame;
}

void player_draw(player_t *player) {
    set_sprite_tile(S_PLAYER, player->entity.sprite.frame);
    move_sprite(S_PLAYER, player->pos.x, player->pos.y);
}
