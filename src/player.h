#include <gb/gb.h>
#include "main.h"
#include "coin.h"
#include "bullet.h"
#include "drone.h"
#include "splitter.h"
#include "turret.h"
#include "skulltron.h"
#include "heatball.h"
#include "spike.h"
#include "item.h"
#ifndef _PLAYER_H_
#define _PLAYER_H_

typedef struct player_t {
    POINT pos, check_point;
    entity_t entity;
    UINT8 lives, score, ammo;
    BOOL can_move, is_dead;
    UINT8 respawn_timer;
} player_t;

void player_spawn(player_t *player);
void player_input(player_t *player, bullet_t *bullet);
void player_update(player_t *player, coin_t *coin, bullet_t *bullet, UINT8,
    item_t *item, drone_t *drone, splitter_t *splitter, skulltron_t *skulltron,
    skulltron_t *skullperior, bullet_t *enemy_bullet, turret_t *turret, turret_t *turret2,
    heatball_t *heatball, spike_t *spike);
void player_shoot(player_t *player, bullet_t *bullet);
void player_check_collisions(player_t *player, coin_t *coin, UINT8, item_t *item,
    drone_t *drone, splitter_t *splitter, skulltron_t *skulltron, skulltron_t *skullperior,
    bullet_t *enemy_bullet, turret_t* turret, turret_t *turret2, heatball_t *heatball,
    spike_t *spike);
void player_set_check_point(player_t *player, POINT *pos);
void player_nextframe(player_t *player);
void player_draw(player_t *player);

#endif
