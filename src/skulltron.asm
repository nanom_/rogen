;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module skulltron
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _bullet_destroy
	.globl _bullet_spawn
	.globl _skulltron_spawn
	.globl _skulltron_update
	.globl _skulltron_shoot
	.globl _skulltron_check_collisions
	.globl _skulltron_nextframe
	.globl _skulltron_kill
	.globl _skulltron_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/skulltron.c:8: void skulltron_spawn(skulltron_t *skulltron, coin_t *coin) {
;	---------------------------------
; Function skulltron_spawn
; ---------------------------------
_skulltron_spawn::
	add	sp, #-4
;src/skulltron.c:9: skulltron->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x + 8 : coin->pos.x - 8);
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ldhl	sp,#8
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	a, (bc)
	ld	l, a
	sub	a, #0x50
	jr	NC, 00103$
	ld	a, l
	add	a, #0x08
	jr	00104$
00103$:
	ld	a, l
	add	a, #0xf8
00104$:
	ld	(de), a
;src/skulltron.c:10: skulltron->pos.y = coin->pos.y;
	pop	de
	push	de
	inc	de
	inc	bc
	ld	a, (bc)
	ld	(de), a
;src/skulltron.c:11: skulltron->entity.is_dead = false;
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/skulltron.c:12: skulltron->destroy_timer = 0;
	pop	de
	push	de
	ld	hl, #0x0014
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/skulltron.c:13: }
	add	sp, #4
	ret
_fps:
	.db #0x3c	; 60
;src/skulltron.c:15: void skulltron_update(skulltron_t *skulltron, bullet_t *bullet, POINT *player_pos, bullet_t *enemy_bullet) {
;	---------------------------------
; Function skulltron_update
; ---------------------------------
_skulltron_update::
	add	sp, #-10
;src/skulltron.c:16: if (skulltron->entity.is_dead && skulltron->destroy_timer >= 50) return;
	ldhl	sp,	#12
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	c, (hl)
	pop	de
	push	de
	ld	hl, #0x0014
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ld	a, c
	or	a, a
	jr	Z, 00102$
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jp	NC,00140$
00102$:
;src/skulltron.c:17: if (skulltron->entity.is_dead && skulltron->destroy_timer < 50) {
	ld	a, c
	or	a, a
	jr	Z, 00109$
	ldhl	sp,#8
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	cp	a, #0x32
	jr	NC, 00109$
;src/skulltron.c:18: skulltron->destroy_timer++;
	ld	c, a
	inc	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/skulltron.c:19: if (skulltron->destroy_timer % 10 != 0)
	ld	b, #0x00
	ld	hl, #0x000a
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	ld	c, e
	ld	a, d
	or	a, c
	jr	Z, 00105$
;src/skulltron.c:20: skulltron->entity.sprite.frame = (skulltron->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);
	pop	de
	push	de
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	sub	a, #0x16
	jr	NZ, 00142$
	ld	de, #0x0017
	jr	00143$
00142$:
	ld	de, #0x0016
00143$:
	ld	a, e
	ld	(bc), a
00105$:
;src/skulltron.c:22: if (skulltron->destroy_timer == 50) skulltron_kill(skulltron);
	ldhl	sp,#8
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jr	NZ, 00107$
	pop	hl
	push	hl
	push	hl
	call	_skulltron_kill
	add	sp, #2
00107$:
;src/skulltron.c:24: skulltron_draw(skulltron);
	pop	hl
	push	hl
	push	hl
	call	_skulltron_draw
	add	sp, #2
;src/skulltron.c:25: return;
	jp	00140$
00109$:
;src/skulltron.c:27: if (!skulltron->can_move) return;
	pop	de
	push	de
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jp	Z,00140$
;src/skulltron.c:28: skulltron_nextframe(skulltron);
	pop	hl
	push	hl
	push	hl
	call	_skulltron_nextframe
	add	sp, #2
;src/skulltron.c:31: skulltron->status_interval++;
	pop	de
	push	de
	ld	hl, #0x0012
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#3
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	inc	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/skulltron.c:32: if (skulltron->status_interval == skulltron->status_interval_max) {
	pop	de
	push	de
	ld	hl, #0x0013
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#5
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
;src/skulltron.c:34: if (skulltron->move_towards_player)
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#7
	ld	(hl+), a
	ld	(hl), d
;src/skulltron.c:32: if (skulltron->status_interval == skulltron->status_interval_max) {
	ld	a, c
	sub	a, b
	jr	NZ, 00126$
;src/skulltron.c:33: skulltron->status_interval = 0;
	ldhl	sp,	#3
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/skulltron.c:34: if (skulltron->move_towards_player)
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	inc	hl
	ld	(hl), a
;src/skulltron.c:35: if (skulltron->shoot_when_stop &&  enemy_bullet->disabled) skulltron_shoot(skulltron, enemy_bullet);
	pop	de
	push	de
	ld	hl, #0x0015
	add	hl, de
	ld	c, l
	ld	b, h
;src/skulltron.c:34: if (skulltron->move_towards_player)
	ldhl	sp,	#9
	ld	a, (hl)
	or	a, a
	jr	Z, 00117$
;src/skulltron.c:35: if (skulltron->shoot_when_stop &&  enemy_bullet->disabled) skulltron_shoot(skulltron, enemy_bullet);
	ld	a, (bc)
	or	a, a
	jr	Z, 00117$
	ldhl	sp,#18
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0005
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00117$
	push	bc
	push	de
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_skulltron_shoot
	add	sp, #4
	pop	bc
00117$:
;src/skulltron.c:34: if (skulltron->move_towards_player)
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
;src/skulltron.c:37: if (skulltron->move_towards_player && skulltron->shoot_when_stop) {
	or	a, a
	jr	Z, 00122$
	push	af
	ld	a, (bc)
	ld	e, a
	pop	af
	inc	e
	dec	e
	jr	Z, 00122$
;src/skulltron.c:38: skulltron->status_interval_max = 44;
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x2c
	jr	00123$
00122$:
;src/skulltron.c:39: } else if (!skulltron->move_towards_player && skulltron->shoot_when_stop) {
	or	a, a
	jr	NZ, 00123$
	ld	a, (bc)
	or	a, a
	jr	Z, 00123$
;src/skulltron.c:40: skulltron->status_interval_max = 27;
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x1b
00123$:
;src/skulltron.c:43: skulltron->move_towards_player = !skulltron->move_towards_player;
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a,#0x01
	ld	a, #0x00
	rla
	ld	c, a
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
00126$:
;src/skulltron.c:47: if (skulltron->move_towards_player) {
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
;src/skulltron.c:51: skulltron->entity.is_moving = true;
	pop	de
	push	de
	ld	hl, #0x000b
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
;src/skulltron.c:47: if (skulltron->move_towards_player) {
	ld	a, c
	or	a, a
	jp	Z, 00138$
;src/skulltron.c:48: if (skulltron->pos.x > player_pos->x) {
	pop	de
	push	de
	ld	a, (de)
	inc	hl
	ld	(hl), a
	ldhl	sp,	#16
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#5
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	inc	hl
	ld	(hl), a
;src/skulltron.c:49: skulltron->pos.x -= skulltron->entity.spd;
	pop	de
	push	de
	ld	hl, #0x000a
	add	hl, de
	ld	c, l
	ld	b, h
;src/skulltron.c:50: skulltron->entity.facing = fa_left;
	pop	de
	push	de
	ld	hl, #0x0009
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
;src/skulltron.c:48: if (skulltron->pos.x > player_pos->x) {
	ld	a, d
	ld	(hl-), a
	dec	hl
	ld	a, (hl)
	ldhl	sp,	#4
	sub	a, (hl)
	jr	NC, 00130$
;src/skulltron.c:49: skulltron->pos.x -= skulltron->entity.spd;
	ld	a, (bc)
	ld	e, a
	ld	a, (hl)
	sub	a, e
	pop	hl
	push	hl
	ld	(hl), a
;src/skulltron.c:50: skulltron->entity.facing = fa_left;
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/skulltron.c:51: skulltron->entity.is_moving = true;
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
	jr	00131$
00130$:
;src/skulltron.c:52: } else if (skulltron->pos.x < player_pos->x) {
	ldhl	sp,	#4
	ld	a, (hl)
	ldhl	sp,	#7
	sub	a, (hl)
	jr	NC, 00131$
;src/skulltron.c:53: skulltron->pos.x += skulltron->entity.spd;
	ld	a, (bc)
	ldhl	sp,	#4
	add	a, (hl)
	pop	hl
	push	hl
	ld	(hl), a
;src/skulltron.c:54: skulltron->entity.facing = fa_right;
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/skulltron.c:55: skulltron->entity.is_moving = true;
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
00131$:
;src/skulltron.c:58: if (skulltron->pos.y > player_pos->y) {
	pop	de
	push	de
	inc	de
	ld	a, (de)
	ldhl	sp,	#9
	ld	(hl), a
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#8
	ld	(hl), a
	ldhl	sp,	#8
	ld	a, (hl+)
	sub	a, (hl)
	jr	NC, 00135$
;src/skulltron.c:59: skulltron->pos.y -= skulltron->entity.spd;
	ld	a, (bc)
	ld	c, a
	ld	a, (hl)
	sub	a, c
	ld	(de), a
;src/skulltron.c:60: skulltron->entity.is_moving = true;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
	jr	00139$
00135$:
;src/skulltron.c:61: } else if (skulltron->pos.y < player_pos->y) {
	ldhl	sp,	#9
	ld	a, (hl-)
	sub	a, (hl)
	jr	NC, 00139$
;src/skulltron.c:62: skulltron->pos.y += skulltron->entity.spd;
	ld	a, (bc)
	inc	hl
	add	a, (hl)
	ld	(de), a
;src/skulltron.c:63: skulltron->entity.is_moving = true;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
	jr	00139$
00138$:
;src/skulltron.c:66: skulltron->entity.is_moving = false;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
00139$:
;src/skulltron.c:69: skulltron_check_collisions(skulltron, bullet);
	ldhl	sp,	#14
	ld	a, (hl+)
	ld	h, (hl)
	ld	c, a
	ld	b, h
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_skulltron_check_collisions
	add	sp, #4
;src/skulltron.c:70: skulltron_draw(skulltron);
	ldhl	sp,	#12
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_skulltron_draw
	add	sp, #2
00140$:
;src/skulltron.c:71: }
	add	sp, #10
	ret
;src/skulltron.c:73: void skulltron_shoot(skulltron_t *skulltron, bullet_t *enemy_bullet) {
;	---------------------------------
; Function skulltron_shoot
; ---------------------------------
_skulltron_shoot::
	add	sp, #-4
;src/skulltron.c:74: enemy_bullet->pos.x = skulltron->pos.x;
	ldhl	sp,#8
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	a, (de)
	ld	(bc), a
;src/skulltron.c:75: enemy_bullet->pos.y = skulltron->pos.y;
	ld	e, c
	ld	d, b
	inc	de
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ld	(de), a
;src/skulltron.c:76: enemy_bullet->direction = skulltron->entity.facing;
	ld	hl, #0x0004
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
	pop	de
	push	de
	ld	hl, #0x0009
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (de)
	ldhl	sp,	#2
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/skulltron.c:77: enemy_bullet->sprite = S_TURRETBEAM;
	ld	l, c
	ld	h, b
	inc	hl
	inc	hl
	ld	(hl), #0x1b
;src/skulltron.c:78: enemy_bullet->spd = 2;
	ld	l, c
	ld	h, b
	inc	hl
	inc	hl
	inc	hl
	ld	(hl), #0x02
;src/skulltron.c:79: enemy_bullet->disabled = false;
	ld	hl, #0x0005
	add	hl, bc
	ld	(hl), #0x00
;src/skulltron.c:81: bullet_spawn(enemy_bullet);
	push	bc
	call	_bullet_spawn
	add	sp, #2
;src/skulltron.c:82: }
	add	sp, #4
	ret
;src/skulltron.c:84: void skulltron_check_collisions(skulltron_t *skulltron, bullet_t *bullet) {
;	---------------------------------
; Function skulltron_check_collisions
; ---------------------------------
_skulltron_check_collisions::
	add	sp, #-12
;src/skulltron.c:85: if (!bullet->disabled && skulltron->pos.x >= bullet->pos.x - 1 && skulltron->pos.y >= bullet->pos.y - 1 && skulltron->pos.x <= bullet->pos.x + 10 && skulltron->pos.y <= bullet->pos.y + 10) {
	ldhl	sp,#16
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0005
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jp	NZ, 00107$
	ldhl	sp,	#14
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#9
	ld	(hl), a
	ld	a, (bc)
	ldhl	sp,	#2
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#11
	ld	(hl-), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#4
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00134$
	bit	7, d
	jr	NZ, 00135$
	cp	a, a
	jr	00135$
00134$:
	bit	7, d
	jr	Z, 00135$
	scf
00135$:
	jp	C, 00107$
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#11
	ld	(hl), a
	ld	e, c
	ld	d, b
	inc	de
	ld	a, (de)
	ldhl	sp,	#6
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#9
	ld	(hl-), a
	ld	(hl), e
	ldhl	sp,	#11
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#10
	ld	e, l
	ld	d, h
	ldhl	sp,	#8
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00136$
	bit	7, d
	jr	NZ, 00137$
	cp	a, a
	jr	00137$
00136$:
	bit	7, d
	jr	Z, 00137$
	scf
00137$:
	jp	C, 00107$
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#4
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00138$
	bit	7, d
	jr	NZ, 00139$
	cp	a, a
	jr	00139$
00138$:
	bit	7, d
	jr	Z, 00139$
	scf
00139$:
	jr	C, 00107$
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00140$
	bit	7, d
	jr	NZ, 00141$
	cp	a, a
	jr	00141$
00140$:
	bit	7, d
	jr	Z, 00141$
	scf
00141$:
	jr	C, 00107$
;src/skulltron.c:86: skulltron->entity.is_dead = true;
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	(hl), #0x01
;src/skulltron.c:87: skulltron->entity.sprite.frame = S_EXPLO1;
	pop	de
	push	de
	ld	hl, #0x0008
	add	hl, de
	ld	(hl), #0x16
;src/skulltron.c:88: skulltron->destroy_timer = 0;
	pop	de
	push	de
	ld	hl, #0x0014
	add	hl, de
	ld	e, l
	ld	d, h
	xor	a, a
	ld	(de), a
;src/skulltron.c:90: bullet_destroy(bullet);
	push	bc
	call	_bullet_destroy
	add	sp, #2
00107$:
;src/skulltron.c:92: }
	add	sp, #12
	ret
;src/skulltron.c:94: void skulltron_nextframe(skulltron_t *skulltron) {
;	---------------------------------
; Function skulltron_nextframe
; ---------------------------------
_skulltron_nextframe::
;src/skulltron.c:95: skulltron->entity.sprite.frame = (skulltron->entity.facing == fa_left ? skulltron->entity.sprite.left_anim_start : skulltron->entity.sprite.right_anim_start);
	pop	bc
	pop	de
	push	de
	push	bc
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ld	hl, #0x0009
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00103$
	inc	de
	inc	de
	inc	de
	inc	de
	ld	a, (de)
	jr	00104$
00103$:
	ld	hl, #0x0006
	add	hl, de
	ld	a, (hl)
00104$:
	ld	(bc), a
;src/skulltron.c:96: }
	ret
;src/skulltron.c:98: void skulltron_kill(skulltron_t *skulltron) {
;	---------------------------------
; Function skulltron_kill
; ---------------------------------
_skulltron_kill::
;src/skulltron.c:100: skulltron->pos.x = 200;
	pop	bc
	pop	de
	push	de
	push	bc
	ld	a, #0xc8
	ld	(de), a
;src/skulltron.c:101: skulltron->pos.y = 200;
	ld	l, e
	ld	h, d
	inc	hl
	ld	(hl), #0xc8
;src/skulltron.c:103: if (!skulltron->entity.is_dead) skulltron->entity.is_dead = true;
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00103$
	ld	(hl), #0x01
;src/skulltron.c:104: if (!skulltron->destroy_timer != 50) skulltron->destroy_timer = 50;
00103$:
	ld	hl, #0x0014
	add	hl, de
	ld	(hl), #0x32
;src/skulltron.c:105: skulltron_draw(skulltron);
	push	de
	call	_skulltron_draw
	add	sp, #2
;src/skulltron.c:106: }
	ret
;src/skulltron.c:108: void skulltron_draw(skulltron_t *skulltron) {
;	---------------------------------
; Function skulltron_draw
; ---------------------------------
_skulltron_draw::
	dec	sp
;src/skulltron.c:109: set_sprite_tile(S_SKULLTRON, skulltron->entity.sprite.frame);
	ldhl	sp,#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	bc, #_shadow_OAM+50
	ldhl	sp,	#0
	ld	a, (hl)
	ld	(bc), a
;src/skulltron.c:110: move_sprite(S_SKULLTRON, skulltron->pos.x, skulltron->pos.y);
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #(_shadow_OAM + 0x0030)
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/skulltron.c:110: move_sprite(S_SKULLTRON, skulltron->pos.x, skulltron->pos.y);
;src/skulltron.c:111: }
	inc	sp
	ret
	.area _CODE
	.area _CABS (ABS)
