#include <gb/gb.h>
#include "main.h"
#include "game.h"
#include "coin.h"
#include "bullet.h"
#include "skulltron.h"

void skulltron_spawn(skulltron_t *skulltron, coin_t *coin) {
    skulltron->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x + 8 : coin->pos.x - 8);
    skulltron->pos.y = coin->pos.y;
    skulltron->entity.is_dead = false;
    skulltron->destroy_timer = 0;
}

void skulltron_update(skulltron_t *skulltron, bullet_t *bullet, POINT *player_pos, bullet_t *enemy_bullet) {
    if (skulltron->entity.is_dead && skulltron->destroy_timer >= 50) return;
    if (skulltron->entity.is_dead && skulltron->destroy_timer < 50) {
        skulltron->destroy_timer++;
        if (skulltron->destroy_timer % 10 != 0)
            skulltron->entity.sprite.frame = (skulltron->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);

        if (skulltron->destroy_timer == 50) skulltron_kill(skulltron);

        skulltron_draw(skulltron);
        return;
    }
    if (!skulltron->can_move) return;
    skulltron_nextframe(skulltron);

    // move -> stop -> move -> stop -> ...
    skulltron->status_interval++;
    if (skulltron->status_interval == skulltron->status_interval_max) {
        skulltron->status_interval = 0;
        if (skulltron->move_towards_player)
            if (skulltron->shoot_when_stop &&  enemy_bullet->disabled) skulltron_shoot(skulltron, enemy_bullet);
        
        if (skulltron->move_towards_player && skulltron->shoot_when_stop) {
            skulltron->status_interval_max = 44;
        } else if (!skulltron->move_towards_player && skulltron->shoot_when_stop) {
            skulltron->status_interval_max = 27;
        }

        skulltron->move_towards_player = !skulltron->move_towards_player;
    }

    // if skulltron / skullperior is in move_towards_player mode
    if (skulltron->move_towards_player) {
        if (skulltron->pos.x > player_pos->x) {
            skulltron->pos.x -= skulltron->entity.spd;
            skulltron->entity.facing = fa_left;
            skulltron->entity.is_moving = true;
        } else if (skulltron->pos.x < player_pos->x) {
            skulltron->pos.x += skulltron->entity.spd;
            skulltron->entity.facing = fa_right;
            skulltron->entity.is_moving = true;
        }

        if (skulltron->pos.y > player_pos->y) {
            skulltron->pos.y -= skulltron->entity.spd;
            skulltron->entity.is_moving = true;
        } else if (skulltron->pos.y < player_pos->y) {
            skulltron->pos.y += skulltron->entity.spd;
            skulltron->entity.is_moving = true;
        }
    } else {
        skulltron->entity.is_moving = false;
    }

    skulltron_check_collisions(skulltron, bullet);
    skulltron_draw(skulltron);
}

void skulltron_shoot(skulltron_t *skulltron, bullet_t *enemy_bullet) {
    enemy_bullet->pos.x = skulltron->pos.x;
    enemy_bullet->pos.y = skulltron->pos.y;
    enemy_bullet->direction = skulltron->entity.facing;
    enemy_bullet->sprite = S_TURRETBEAM;
    enemy_bullet->spd = 2;
    enemy_bullet->disabled = false;

    bullet_spawn(enemy_bullet);
}

void skulltron_check_collisions(skulltron_t *skulltron, bullet_t *bullet) {
    if (!bullet->disabled && skulltron->pos.x >= bullet->pos.x - 1 && skulltron->pos.y >= bullet->pos.y - 1 && skulltron->pos.x <= bullet->pos.x + 10 && skulltron->pos.y <= bullet->pos.y + 10) {
        skulltron->entity.is_dead = true;
        skulltron->entity.sprite.frame = S_EXPLO1;
        skulltron->destroy_timer = 0;

        bullet_destroy(bullet);
    }
}

void skulltron_nextframe(skulltron_t *skulltron) {
    skulltron->entity.sprite.frame = (skulltron->entity.facing == fa_left ? skulltron->entity.sprite.left_anim_start : skulltron->entity.sprite.right_anim_start);
}

void skulltron_kill(skulltron_t *skulltron) {
    // we just move the enemy out of the view
    skulltron->pos.x = 200;
    skulltron->pos.y = 200;
    // and we turn off the entity's updating function by setting the "is_dead" property to "true" and the "destroy_timer" property to 50
    if (!skulltron->entity.is_dead) skulltron->entity.is_dead = true;
    if (!skulltron->destroy_timer != 50) skulltron->destroy_timer = 50;
    skulltron_draw(skulltron);
}

void skulltron_draw(skulltron_t *skulltron) {
    set_sprite_tile(S_SKULLTRON, skulltron->entity.sprite.frame);
    move_sprite(S_SKULLTRON, skulltron->pos.x, skulltron->pos.y);
}
