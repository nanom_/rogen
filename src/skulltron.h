#include <gb/gb.h>
#include "main.h"
#include "coin.h"
#include "bullet.h"
#ifndef _SKULLTRON_H_
#define _SKULLTRON_H_

typedef struct skulltron_t {
    POINT pos;
    entity_t entity;
    direction_t direction;
    BOOL can_move, move_towards_player;
    UINT8 status_interval, status_interval_max, destroy_timer;
    BOOL shoot_when_stop;
} skulltron_t;

void skulltron_spawn(skulltron_t *skulltron, coin_t *coin);
void skulltron_update(skulltron_t *skulltron, bullet_t *bullet, POINT *player_pos, bullet_t *enemy_bullet);
void skulltron_shoot(skulltron_t *skulltron, bullet_t *enemy_bullet);
void skulltron_check_collisions(skulltron_t *skulltron, bullet_t *bullet);
void skulltron_nextframe(skulltron_t *skulltron);
void skulltron_kill(skulltron_t *skulltron);
void skulltron_draw(skulltron_t *skulltron);

#endif
