;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module spike
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _bullet_destroy
	.globl _spike_spawn
	.globl _spike_update
	.globl _spike_check_collisions
	.globl _spike_kill
	.globl _spike_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/spike.c:8: void spike_spawn(spike_t *spike, coin_t *coin) {
;	---------------------------------
; Function spike_spawn
; ---------------------------------
_spike_spawn::
	add	sp, #-4
;src/spike.c:9: spike->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (160 - 16));
	ldhl	sp,#6
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	inc	sp
	inc	sp
	push	bc
	inc	hl
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#2
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00103$
	ld	de, #0x0010
	jr	00104$
00103$:
	ld	de, #0xff90
00104$:
	ld	a, e
	pop	hl
	push	hl
	ld	(hl), a
;src/spike.c:10: spike->pos.y = (144 - 8);
	ld	l, c
	ld	h, b
	inc	hl
	ld	(hl), #0x88
;src/spike.c:11: spike->entity.sprite.frame = S_SPIKE;
	ld	hl, #0x0008
	add	hl, bc
	ld	(hl), #0x14
;src/spike.c:12: spike->entity.is_dead = false;
	ld	hl, #0x000e
	add	hl, bc
	ld	(hl), #0x00
;src/spike.c:13: spike->destroy_timer = 0;
	ld	hl, #0x0011
	add	hl, bc
	ld	(hl), #0x00
;src/spike.c:14: spike->direction = d_up;
	ld	hl, #0x000f
	add	hl, bc
	ld	(hl), #0x03
;src/spike.c:15: spike->entity.facing = (coin->pos.x < (SCREENWIDTH / 2) ? fa_right : fa_left);
	ld	hl, #0x0009
	add	hl, bc
	ld	c, l
	ld	b, h
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00105$
	ld	de, #0x0001
	jr	00106$
00105$:
	ld	de, #0x0000
00106$:
	ld	a, e
	ld	(bc), a
;src/spike.c:16: }
	add	sp, #4
	ret
_fps:
	.db #0x3c	; 60
;src/spike.c:18: void spike_update(spike_t *spike, bullet_t *bullet) {
;	---------------------------------
; Function spike_update
; ---------------------------------
_spike_update::
	add	sp, #-4
;src/spike.c:19: if (spike->entity.is_dead && spike->destroy_timer >= 50) return;
	ldhl	sp,#6
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x000e
	add	hl, bc
	ld	a, (hl)
	ldhl	sp,	#1
	ld	(hl), a
	ld	hl, #0x0011
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	dec	hl
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jp	NC,00122$
00102$:
;src/spike.c:20: if (spike->entity.is_dead && spike->destroy_timer < 50) {
	ldhl	sp,	#1
	ld	a, (hl)
	or	a, a
	jr	Z, 00109$
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	cp	a, #0x32
	jr	NC, 00109$
;src/spike.c:21: spike->destroy_timer++;
	inc	a
	dec	hl
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/spike.c:22: if (spike->destroy_timer % 10 != 0)
	ld	e, a
	ld	d, #0x00
	push	bc
	ld	hl, #0x000a
	push	hl
	push	de
	call	__modsint
	add	sp, #4
	pop	bc
	ld	a, d
	or	a, e
	jr	Z, 00105$
;src/spike.c:23: spike->entity.sprite.frame = (spike->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);
	ld	hl, #0x0008
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x16
	jr	NZ, 00124$
	ld	de, #0x0017
	jr	00125$
00124$:
	ld	de, #0x0016
00125$:
	ld	(hl), e
00105$:
;src/spike.c:25: if (spike->destroy_timer == 50) spike_kill(spike);
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jr	NZ, 00107$
	push	bc
	push	bc
	call	_spike_kill
	add	sp, #2
	pop	bc
00107$:
;src/spike.c:27: spike_draw(spike);
	push	bc
	call	_spike_draw
	add	sp, #2
;src/spike.c:28: return;
	jp	00122$
00109$:
;src/spike.c:30: if (!spike->can_move) return;
	ld	hl, #0x0010
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jp	Z,00122$
;src/spike.c:32: if (spike->entity.facing == fa_left) {
	ld	hl, #0x0009
	add	hl, bc
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
;src/spike.c:33: if (spike->direction == d_down) {
	ld	hl, #0x000f
	add	hl, bc
	ld	e, l
	ld	d, h
;src/spike.c:34: spike->pos.x -= 1;
	ld	a, (bc)
	ldhl	sp,	#3
	ld	(hl), a
;src/spike.c:35: spike->pos.y += 2;
	ld	hl, #0x0001
	add	hl, bc
	push	de
	ld	a, l
	ld	d, h
	ldhl	sp,	#3
	ld	(hl+), a
	ld	(hl), d
	pop	de
;src/spike.c:33: if (spike->direction == d_down) {
	ld	a, (de)
;src/spike.c:34: spike->pos.x -= 1;
	inc	hl
	ld	e, (hl)
;src/spike.c:33: if (spike->direction == d_down) {
	sub	a, #0x02
	ld	a, #0x01
	jr	Z, 00186$
	xor	a, a
00186$:
	ldhl	sp,	#3
	ld	(hl), a
;src/spike.c:32: if (spike->entity.facing == fa_left) {
	ldhl	sp,	#0
	ld	a, (hl)
	or	a, a
	jr	NZ, 00120$
;src/spike.c:34: spike->pos.x -= 1;
	dec	e
;src/spike.c:33: if (spike->direction == d_down) {
	ldhl	sp,	#3
	ld	a, (hl)
	or	a, a
	jr	Z, 00114$
;src/spike.c:34: spike->pos.x -= 1;
	ld	a, e
	ld	(bc), a
;src/spike.c:35: spike->pos.y += 2;
	dec	hl
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, #0x02
	dec	hl
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
	jr	00121$
00114$:
;src/spike.c:37: spike->pos.x -= 1;
	ld	a, e
	ld	(bc), a
;src/spike.c:38: spike->pos.y -= 2;
	ldhl	sp,#1
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	dec	a
	dec	a
	dec	hl
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
	jr	00121$
00120$:
;src/spike.c:42: spike->pos.x += 1;
	inc	e
;src/spike.c:41: if (spike->direction == d_down) {
	ldhl	sp,	#3
	ld	a, (hl)
	or	a, a
	jr	Z, 00117$
;src/spike.c:42: spike->pos.x += 1;
	ld	a, e
	ld	(bc), a
;src/spike.c:43: spike->pos.y += 2;
	dec	hl
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, #0x02
	dec	hl
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
	jr	00121$
00117$:
;src/spike.c:45: spike->pos.x += 1;
	ld	a, e
	ld	(bc), a
;src/spike.c:46: spike->pos.y -= 2;
	ldhl	sp,#1
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	dec	a
	dec	a
	dec	hl
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
00121$:
;src/spike.c:50: spike_check_collisions(spike, bullet);
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	push	bc
	call	_spike_check_collisions
	add	sp, #4
;src/spike.c:51: spike_draw(spike);
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_spike_draw
	add	sp, #2
00122$:
;src/spike.c:52: }
	add	sp, #4
	ret
;src/spike.c:54: void spike_check_collisions(spike_t *spike, bullet_t *bullet) {
;	---------------------------------
; Function spike_check_collisions
; ---------------------------------
_spike_check_collisions::
	add	sp, #-12
;src/spike.c:55: if (spike->pos.y <= 32)
	ldhl	sp,#14
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0001
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#10
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#7
	ld	(hl), a
;src/spike.c:56: spike->direction = d_down;
	ld	hl, #0x000f
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
;src/spike.c:55: if (spike->pos.y <= 32)
	ld	a, #0x20
	dec	hl
	dec	hl
	sub	a, (hl)
	jr	C, 00102$
;src/spike.c:56: spike->direction = d_down;
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x02
00102$:
;src/spike.c:58: if (spike->pos.y >= 144)
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x90
	jr	C, 00104$
;src/spike.c:59: spike->direction = d_up;
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x03
00104$:
;src/spike.c:61: if (spike->pos.x >= (160 - 8)) {
	ld	a, (bc)
	ld	l, a
;src/spike.c:62: spike->entity.facing = fa_left;
	ld	a, c
	add	a, #0x09
	ld	e, a
	ld	a, b
	adc	a, #0x00
	ld	d, a
;src/spike.c:61: if (spike->pos.x >= (160 - 8)) {
	ld	a, l
	sub	a, #0x98
	jr	C, 00106$
;src/spike.c:62: spike->entity.facing = fa_left;
	xor	a, a
	ld	(de), a
00106$:
;src/spike.c:65: if (spike->pos.x <= 16) {
	ld	a, (bc)
	ld	l, a
	ld	a, #0x10
	sub	a, l
	jr	C, 00108$
;src/spike.c:66: spike->entity.facing = fa_right;
	ld	a, #0x01
	ld	(de), a
00108$:
;src/spike.c:69: if (!bullet->disabled && spike->pos.x >= bullet->pos.x - 1 && spike->pos.y >= bullet->pos.y - 1 && spike->pos.x <= bullet->pos.x + 10 && spike->pos.y <= bullet->pos.y + 10) {
	ldhl	sp,	#16
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	hl, #0x0005
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (de)
	or	a, a
	jp	NZ, 00115$
	ld	a, (bc)
	ldhl	sp,	#7
	ld	(hl), a
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#2
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#9
	ld	(hl-), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#4
	ld	e, l
	ld	d, h
	ldhl	sp,	#8
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00162$
	bit	7, d
	jr	NZ, 00163$
	cp	a, a
	jr	00163$
00162$:
	bit	7, d
	jr	Z, 00163$
	scf
00163$:
	jp	C, 00115$
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(hl), a
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#6
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#9
	ld	(hl-), a
	ld	(hl), e
	ldhl	sp,	#11
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#10
	ld	e, l
	ld	d, h
	ldhl	sp,	#8
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00164$
	bit	7, d
	jr	NZ, 00165$
	cp	a, a
	jr	00165$
00164$:
	bit	7, d
	jr	Z, 00165$
	scf
00165$:
	jr	C, 00115$
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#4
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00166$
	bit	7, d
	jr	NZ, 00167$
	cp	a, a
	jr	00167$
00166$:
	bit	7, d
	jr	Z, 00167$
	scf
00167$:
	jr	C, 00115$
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00168$
	bit	7, d
	jr	NZ, 00169$
	cp	a, a
	jr	00169$
00168$:
	bit	7, d
	jr	Z, 00169$
	scf
00169$:
	jr	C, 00115$
;src/spike.c:70: spike->entity.is_dead = true;
	ld	hl, #0x000e
	add	hl, bc
	ld	(hl), #0x01
;src/spike.c:71: spike->entity.sprite.frame = S_EXPLO1;
	ld	hl, #0x0008
	add	hl, bc
	ld	(hl), #0x16
;src/spike.c:72: spike->destroy_timer = 0;
	ld	hl, #0x0011
	add	hl, bc
	ld	(hl), #0x00
;src/spike.c:74: bullet_destroy(bullet);
	pop	hl
	push	hl
	push	hl
	call	_bullet_destroy
	add	sp, #2
00115$:
;src/spike.c:76: }
	add	sp, #12
	ret
;src/spike.c:78: void spike_kill(spike_t *spike) {
;	---------------------------------
; Function spike_kill
; ---------------------------------
_spike_kill::
;src/spike.c:80: spike->pos.x = 200;
	pop	bc
	pop	de
	push	de
	push	bc
	ld	a, #0xc8
	ld	(de), a
;src/spike.c:81: spike->pos.y = 200;
	ld	l, e
	ld	h, d
	inc	hl
	ld	(hl), #0xc8
;src/spike.c:83: if (!spike->entity.is_dead) spike->entity.is_dead = true;
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00103$
	ld	(hl), #0x01
;src/spike.c:84: if (!spike->destroy_timer != 50) spike->destroy_timer = 50;
00103$:
	ld	hl, #0x0011
	add	hl, de
	ld	(hl), #0x32
;src/spike.c:85: spike_draw(spike);
	push	de
	call	_spike_draw
	add	sp, #2
;src/spike.c:86: }
	ret
;src/spike.c:88: void spike_draw(spike_t *spike) {
;	---------------------------------
; Function spike_draw
; ---------------------------------
_spike_draw::
	dec	sp
;src/spike.c:89: set_sprite_tile(S_SPIKE, spike->entity.sprite.frame);
	ldhl	sp,#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	bc, #_shadow_OAM+82
	ldhl	sp,	#0
	ld	a, (hl)
	ld	(bc), a
;src/spike.c:90: move_sprite(S_SPIKE, spike->pos.x, spike->pos.y);
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #(_shadow_OAM + 0x0050)
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/spike.c:90: move_sprite(S_SPIKE, spike->pos.x, spike->pos.y);
;src/spike.c:91: }
	inc	sp
	ret
	.area _CODE
	.area _CABS (ABS)
