#include <gb/gb.h>
#include "main.h"
#include "game.h"
#include "coin.h"
#include "bullet.h"
#include "spike.h"

void spike_spawn(spike_t *spike, coin_t *coin) {
    spike->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (160 - 16));
    spike->pos.y = (144 - 8);
    spike->entity.sprite.frame = S_SPIKE;
    spike->entity.is_dead = false;
    spike->destroy_timer = 0;
    spike->direction = d_up;
    spike->entity.facing = (coin->pos.x < (SCREENWIDTH / 2) ? fa_right : fa_left);
}

void spike_update(spike_t *spike, bullet_t *bullet) {
    if (spike->entity.is_dead && spike->destroy_timer >= 50) return;
    if (spike->entity.is_dead && spike->destroy_timer < 50) {
        spike->destroy_timer++;
        if (spike->destroy_timer % 10 != 0)
            spike->entity.sprite.frame = (spike->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);

        if (spike->destroy_timer == 50) spike_kill(spike);

        spike_draw(spike);
        return;
    }
    if (!spike->can_move) return;

    if (spike->entity.facing == fa_left) {
        if (spike->direction == d_down) {
            spike->pos.x -= 1;
            spike->pos.y += 2;
        } else {
            spike->pos.x -= 1;
            spike->pos.y -= 2;
        }
    } else {
        if (spike->direction == d_down) {
            spike->pos.x += 1;
            spike->pos.y += 2;
        } else {
            spike->pos.x += 1;
            spike->pos.y -= 2;
        }
    }

    spike_check_collisions(spike, bullet);
    spike_draw(spike);
}

void spike_check_collisions(spike_t *spike, bullet_t *bullet) {
    if (spike->pos.y <= 32)
        spike->direction = d_down;
    
    if (spike->pos.y >= 144)
        spike->direction = d_up;
    
    if (spike->pos.x >= (160 - 8)) {
        spike->entity.facing = fa_left;
    }

    if (spike->pos.x <= 16) {
        spike->entity.facing = fa_right;
    }

    if (!bullet->disabled && spike->pos.x >= bullet->pos.x - 1 && spike->pos.y >= bullet->pos.y - 1 && spike->pos.x <= bullet->pos.x + 10 && spike->pos.y <= bullet->pos.y + 10) {
        spike->entity.is_dead = true;
        spike->entity.sprite.frame = S_EXPLO1;
        spike->destroy_timer = 0;

        bullet_destroy(bullet);
    }
}

void spike_kill(spike_t *spike) {
    // we just move the enemy out of the view
    spike->pos.x = 200;
    spike->pos.y = 200;
    // and we turn off the entity's updating function by setting the "is_dead" property to "true" and the "destroy_timer" property to 50
    if (!spike->entity.is_dead) spike->entity.is_dead = true;
    if (!spike->destroy_timer != 50) spike->destroy_timer = 50;
    spike_draw(spike);
}

void spike_draw(spike_t *spike) {
    set_sprite_tile(S_SPIKE, spike->entity.sprite.frame);
    move_sprite(S_SPIKE, spike->pos.x, spike->pos.y);
}
