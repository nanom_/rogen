#include <gb/gb.h>
#include "main.h"
#include "coin.h"
#include "bullet.h"
#ifndef _SPIKE_H_
#define _SPIKE_H_

typedef struct spike_t {
    POINT pos;
    entity_t entity;
    direction_t direction;
    BOOL can_move;
    UINT8 destroy_timer;
} spike_t;

void spike_spawn(spike_t *spike, coin_t *coin);
void spike_update(spike_t *spike, bullet_t *bullet);
void spike_check_collisions(spike_t *spike, bullet_t *bullet);
void spike_nextframe(spike_t *spike);
void spike_kill(spike_t *spike);
void spike_draw(spike_t *spike);

#endif
