;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module splitter
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _bullet_destroy
	.globl _abs
	.globl _arand
	.globl _initarand
	.globl _randnum
	.globl _randfactor
	.globl _splitter_spawn
	.globl _splitter_update
	.globl _splitter_check_collisions
	.globl _splitter_nextframe
	.globl _splitter_kill
	.globl _splitter_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
_randfactor::
	.ds 1
_randnum::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/splitter.c:10: void splitter_spawn(splitter_t *splitter, coin_t *coin) {
;	---------------------------------
; Function splitter_spawn
; ---------------------------------
_splitter_spawn::
	add	sp, #-6
;src/splitter.c:11: splitter->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x + 8 : coin->pos.x - 8);
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	bc
	push	bc
	ldhl	sp,#10
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	l, a
	sub	a, #0x50
	jr	NC, 00114$
	ld	a, l
	add	a, #0x08
	jr	00115$
00114$:
	ld	a, l
	add	a, #0xf8
00115$:
	ld	(bc), a
;src/splitter.c:12: splitter->pos.y = coin->pos.y;
	pop	bc
	push	bc
	inc	bc
	inc	de
	ld	a, (de)
	ld	(bc), a
;src/splitter.c:13: splitter->entity.is_dead = false;
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	e, l
	ld	d, h
	xor	a, a
	ld	(de), a
;src/splitter.c:14: splitter->destroy_timer = 0;
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	e, l
	ld	d, h
	xor	a, a
	ld	(de), a
;src/splitter.c:16: randfactor = (global_frame % 2 != 0 ? 1 : 2);
	ld	hl, #_global_frame
	bit	0, (hl)
	jr	Z, 00116$
	ld	de, #0x0001
	jr	00117$
00116$:
	ld	de, #0x0002
00117$:
	ld	hl, #_randfactor
	ld	(hl), e
;src/splitter.c:17: seed = DIV_REG;
	ldh	a, (_DIV_REG+0)
	ld	hl, #_seed
	ld	(hl), a
;src/splitter.c:18: seed |= (UINT16)DIV_REG << 8;
	ldh	a, (_DIV_REG+0)
;src/splitter.c:19: initarand(seed + rand_menukey + (rand_menuframe * randfactor) * (global_frame * randfactor) + splitter->pos.y);
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ld	hl, #_rand_menukey
	ld	e, (hl)
	ld	d, #0x00
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
	push	bc
	ld	a, (#_randfactor)
	push	af
	inc	sp
	ld	a, (#_rand_menuframe)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	ldhl	sp,	#6
	ld	a, e
	ld	(hl+), a
	ld	(hl), d
	ld	a, (#_randfactor)
	push	af
	inc	sp
	ld	a, (#_global_frame)
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	push	de
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	__mulint
	add	sp, #4
	pop	bc
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (bc)
	ld	l, a
	ld	h, #0x00
	add	hl, de
	push	hl
	call	_initarand
	add	sp, #2
;src/splitter.c:21: randnum = abs(arand());
	call	_arand
	ld	a, e
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	hl, #_randnum
	ld	(hl), e
;src/splitter.c:25: splitter->direction = d_left;
	pop	de
	push	de
	ld	hl, #0x000f
	add	hl, de
	ld	c, l
	ld	b, h
;src/splitter.c:24: if (randnum <= 31)
	ld	a, #0x1f
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00110$
;src/splitter.c:25: splitter->direction = d_left;
	xor	a, a
	ld	(bc), a
	jr	00112$
00110$:
;src/splitter.c:26: else if (randnum <= 62)
	ld	a, #0x3e
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00107$
;src/splitter.c:27: splitter->direction = d_right;
	ld	a, #0x01
	ld	(bc), a
	jr	00112$
00107$:
;src/splitter.c:28: else if (randnum <= 93)
	ld	a, #0x5d
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00104$
;src/splitter.c:29: splitter->direction = d_down;
	ld	a, #0x02
	ld	(bc), a
	jr	00112$
00104$:
;src/splitter.c:30: else if (randnum <= 127)
	ld	a, #0x7f
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00112$
;src/splitter.c:31: splitter->direction = d_up;    
	ld	a, #0x03
	ld	(bc), a
00112$:
;src/splitter.c:32: }
	add	sp, #6
	ret
_fps:
	.db #0x3c	; 60
;src/splitter.c:34: void splitter_update(splitter_t *splitter, coin_t *coin, bullet_t *bullet) {
;	---------------------------------
; Function splitter_update
; ---------------------------------
_splitter_update::
	add	sp, #-11
;src/splitter.c:35: if (splitter->entity.is_dead && splitter->destroy_timer >= 50) return;
	ldhl	sp,	#13
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	c, (hl)
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#9
	ld	(hl+), a
	ld	(hl), d
	ld	a, c
	or	a, a
	jr	Z, 00102$
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jp	NC,00152$
00102$:
;src/splitter.c:36: if (splitter->entity.is_dead && splitter->destroy_timer < 50) {
	ld	a, c
	or	a, a
	jr	Z, 00109$
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	cp	a, #0x32
	jr	NC, 00109$
;src/splitter.c:37: splitter->destroy_timer++;
	ld	c, a
	inc	c
	dec	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/splitter.c:38: if (splitter->destroy_timer % 10 != 0)
	ld	b, #0x00
	ld	hl, #0x000a
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	ld	c, e
	ld	a, d
	or	a, c
	jr	Z, 00105$
;src/splitter.c:39: splitter->entity.sprite.frame = (splitter->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);
	pop	de
	push	de
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	sub	a, #0x16
	jr	NZ, 00154$
	ld	de, #0x0017
	jr	00155$
00154$:
	ld	de, #0x0016
00155$:
	ld	a, e
	ld	(bc), a
00105$:
;src/splitter.c:41: if (splitter->destroy_timer == 50)
	ldhl	sp,#9
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jr	NZ, 00107$
;src/splitter.c:42: splitter_kill(splitter);
	pop	hl
	push	hl
	push	hl
	call	_splitter_kill
	add	sp, #2
00107$:
;src/splitter.c:44: splitter_draw(splitter);
	pop	hl
	push	hl
	push	hl
	call	_splitter_draw
	add	sp, #2
;src/splitter.c:45: return;
	jp	00152$
00109$:
;src/splitter.c:47: if (!splitter->can_move) return;
	pop	de
	push	de
	ld	hl, #0x0010
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jp	Z,00152$
;src/splitter.c:48: splitter_nextframe(splitter);
	pop	hl
	push	hl
	push	hl
	call	_splitter_nextframe
	add	sp, #2
;src/splitter.c:50: if (splitter->pos.x > SCREENWIDTH - 16)
	pop	de
	push	de
	ld	a, (de)
	ld	c, a
;src/splitter.c:51: splitter->direction = d_left;
	pop	de
	push	de
	ld	hl, #0x000f
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
;src/splitter.c:50: if (splitter->pos.x > SCREENWIDTH - 16)
	ld	a, #0x90
	sub	a, c
	jr	NC, 00114$
;src/splitter.c:51: splitter->direction = d_left;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
00114$:
;src/splitter.c:52: if (splitter->pos.x < 16)
	pop	de
	push	de
	ld	a, (de)
	sub	a, #0x10
	jr	NC, 00116$
;src/splitter.c:53: splitter->direction = d_right;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
00116$:
;src/splitter.c:54: if (splitter->pos.y > SCREENHEIGHT - 16)
	pop	de
	push	de
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#4
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	ld	a, #0x80
	sub	a, c
	jr	NC, 00118$
;src/splitter.c:55: splitter->direction = d_up;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x03
00118$:
;src/splitter.c:56: if (splitter->pos.y < 32)
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x20
	jr	NC, 00120$
;src/splitter.c:57: splitter->direction = d_down;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x02
00120$:
;src/splitter.c:59: if (abs(splitter->pos.x - coin->pos.x) > 64 || abs(splitter->pos.y - coin->pos.y) > 64) {
	pop	de
	push	de
	ld	a, (de)
	ld	c, a
	ldhl	sp,	#15
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#6
	ld	(hl+), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
	ld	a, c
	sub	a, b
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	a, #0x40
	sub	a, e
	jr	C, 00143$
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#8
	ld	(hl-), a
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#9
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	hl
	dec	hl
	ld	a, (hl)
	sub	a, c
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	a, #0x40
	sub	a, e
	jr	NC, 00144$
00143$:
;src/splitter.c:60: switch(splitter->direction) {
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	or	a, a
	jr	Z, 00121$
	cp	a, #0x01
	jr	Z, 00122$
	cp	a, #0x02
	jr	Z, 00123$
	sub	a, #0x03
	jr	Z, 00124$
	jp	00145$
;src/splitter.c:61: case d_left:
00121$:
;src/splitter.c:62: splitter->direction = d_right;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
;src/splitter.c:63: break;
	jp	00145$
;src/splitter.c:64: case d_right:
00122$:
;src/splitter.c:65: splitter->direction = d_left;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/splitter.c:66: break;
	jp	00145$
;src/splitter.c:67: case d_down:
00123$:
;src/splitter.c:68: splitter->direction = d_up;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x03
;src/splitter.c:69: break;
	jp	00145$
;src/splitter.c:70: case d_up:
00124$:
;src/splitter.c:71: splitter->direction = d_down;
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x02
;src/splitter.c:73: }
	jp	00145$
00144$:
;src/splitter.c:74: } else if (global_frame % 10 == 0) {
	ld	hl, #_global_frame
	ld	c, (hl)
	ld	b, #0x00
	push	bc
	ld	hl, #0x000a
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	pop	bc
	ld	a, d
	or	a, e
	jp	NZ, 00145$
;src/splitter.c:75: initarand(splitter->pos.x * splitter->pos.y - global_frame);
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,#4
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	push	af
	ld	a, (de)
	ld	h, a
	pop	af
	push	bc
	push	hl
	inc	sp
	push	af
	inc	sp
	call	__muluchar
	add	sp, #2
	pop	bc
	ld	a, e
	sub	a, c
	ld	c, a
	ld	a, d
	sbc	a, b
	ld	b, a
	push	bc
	call	_initarand
	add	sp, #2
;src/splitter.c:76: randnum = abs(arand());
	call	_arand
	ld	a, e
	push	af
	inc	sp
	call	_abs
	inc	sp
	ld	hl, #_randnum
	ld	(hl), e
;src/splitter.c:78: switch (splitter->direction) {
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	b, a
;src/splitter.c:87: if (randnum <= 65) splitter->direction = d_down;
	ld	a, #0x41
	ld	hl, #_randnum
	sub	a, (hl)
	ld	a, #0x00
	rla
	ld	c, a
;src/splitter.c:78: switch (splitter->direction) {
	ld	a, b
	or	a, a
	jr	Z, 00134$
	ld	a, b
	dec	a
	jr	Z, 00137$
	ld	a,b
	cp	a,#0x02
	jr	Z, 00129$
	sub	a, #0x03
	jr	NZ, 00145$
;src/splitter.c:80: if (randnum <= 10) splitter->direction = d_right;
	ld	a, #0x0a
	sub	a, (hl)
	jr	C, 00145$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
;src/splitter.c:81: break;
	jr	00145$
;src/splitter.c:82: case d_down:
00129$:
;src/splitter.c:83: if (randnum <= 55) splitter->direction = d_right;
	ld	a, #0x37
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00131$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x01
00131$:
;src/splitter.c:84: if (randnum <= 20) splitter->direction = d_left;
	ld	a, #0x14
	ld	hl, #_randnum
	sub	a, (hl)
	jr	C, 00145$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x00
;src/splitter.c:85: break;
	jr	00145$
;src/splitter.c:86: case d_left:
00134$:
;src/splitter.c:87: if (randnum <= 65) splitter->direction = d_down;
	bit	0, c
	jr	NZ, 00145$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x02
;src/splitter.c:88: break;
	jr	00145$
;src/splitter.c:89: case d_right:
00137$:
;src/splitter.c:90: if (randnum <= 65) splitter->direction = d_up;
	bit	0, c
	jr	NZ, 00145$
	pop	bc
	pop	hl
	push	hl
	push	bc
	ld	(hl), #0x03
;src/splitter.c:92: }
00145$:
;src/splitter.c:95: switch(splitter->direction) {
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
;src/splitter.c:97: splitter->pos.x -= splitter->entity.spd;
	pop	de
	push	de
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
;src/splitter.c:98: splitter->entity.facing = fa_left;
	pop	de
	push	de
	ld	hl, #0x0009
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#7
	ld	(hl+), a
	ld	(hl), d
;src/splitter.c:99: splitter->entity.is_moving = true;
	pop	de
	push	de
	ld	hl, #0x000b
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#9
	ld	(hl+), a
	ld	(hl), d
;src/splitter.c:95: switch(splitter->direction) {
	ld	a, c
	or	a, a
	jr	Z, 00147$
	ld	a, c
	dec	a
	jr	Z, 00148$
	ld	a,c
	cp	a,#0x02
	jr	Z, 00149$
	sub	a, #0x03
	jr	Z, 00150$
	jr	00151$
;src/splitter.c:96: case d_left:
00147$:
;src/splitter.c:97: splitter->pos.x -= splitter->entity.spd;
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,#2
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	push	af
	ld	a, (de)
	ld	c, a
	pop	af
	sub	a, c
	ld	c, a
	pop	hl
	push	hl
	ld	(hl), c
;src/splitter.c:98: splitter->entity.facing = fa_left;
	ldhl	sp,	#7
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/splitter.c:99: splitter->entity.is_moving = true;
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/splitter.c:100: break;
	jr	00151$
;src/splitter.c:101: case d_right:
00148$:
;src/splitter.c:102: splitter->pos.x += splitter->entity.spd;
	pop	de
	push	de
	ld	a, (de)
	ld	c, a
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, c
	ld	c, a
	pop	hl
	push	hl
	ld	(hl), c
;src/splitter.c:103: splitter->entity.facing = fa_right;
	ldhl	sp,	#7
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/splitter.c:104: splitter->entity.is_moving = true;
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/splitter.c:105: break;
	jr	00151$
;src/splitter.c:106: case d_down:
00149$:
;src/splitter.c:107: splitter->pos.y += splitter->entity.spd;
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	dec	hl
	dec	hl
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, c
	ld	c, a
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/splitter.c:108: splitter->entity.is_moving = true;
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/splitter.c:109: break;
	jr	00151$
;src/splitter.c:110: case d_up:
00150$:
;src/splitter.c:111: splitter->pos.y -= splitter->entity.spd;
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	dec	hl
	dec	hl
	dec	hl
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	push	af
	ld	a, (de)
	ld	c, a
	pop	af
	sub	a, c
	ld	c, a
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;src/splitter.c:112: splitter->entity.is_moving = true;
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/splitter.c:114: }
00151$:
;src/splitter.c:116: splitter_check_collisions(splitter, bullet);
	ldhl	sp,	#17
	ld	a, (hl+)
	ld	h, (hl)
	ld	c, a
	ld	b, h
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_splitter_check_collisions
	add	sp, #4
;src/splitter.c:117: splitter_draw(splitter);
	ldhl	sp,	#13
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_splitter_draw
	add	sp, #2
00152$:
;src/splitter.c:118: }
	add	sp, #11
	ret
;src/splitter.c:120: void splitter_check_collisions(splitter_t *splitter, bullet_t *bullet) {
;	---------------------------------
; Function splitter_check_collisions
; ---------------------------------
_splitter_check_collisions::
	add	sp, #-12
;src/splitter.c:121: if (!bullet->disabled && splitter->pos.x >= bullet->pos.x - 1 && splitter->pos.y >= bullet->pos.y - 1 && splitter->pos.x <= bullet->pos.x + 10 && splitter->pos.y <= bullet->pos.y + 10) {
	ldhl	sp,#16
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0005
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jp	NZ, 00107$
	ldhl	sp,	#14
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#9
	ld	(hl), a
	ld	a, (bc)
	ldhl	sp,	#2
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#11
	ld	(hl-), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#4
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00134$
	bit	7, d
	jr	NZ, 00135$
	cp	a, a
	jr	00135$
00134$:
	bit	7, d
	jr	Z, 00135$
	scf
00135$:
	jp	C, 00107$
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#11
	ld	(hl), a
	ld	e, c
	ld	d, b
	inc	de
	ld	a, (de)
	ldhl	sp,	#6
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#9
	ld	(hl-), a
	ld	(hl), e
	ldhl	sp,	#11
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#10
	ld	e, l
	ld	d, h
	ldhl	sp,	#8
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00136$
	bit	7, d
	jr	NZ, 00137$
	cp	a, a
	jr	00137$
00136$:
	bit	7, d
	jr	Z, 00137$
	scf
00137$:
	jp	C, 00107$
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#4
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00138$
	bit	7, d
	jr	NZ, 00139$
	cp	a, a
	jr	00139$
00138$:
	bit	7, d
	jr	Z, 00139$
	scf
00139$:
	jr	C, 00107$
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00140$
	bit	7, d
	jr	NZ, 00141$
	cp	a, a
	jr	00141$
00140$:
	bit	7, d
	jr	Z, 00141$
	scf
00141$:
	jr	C, 00107$
;src/splitter.c:122: splitter->entity.is_dead = true;
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	(hl), #0x01
;src/splitter.c:123: splitter->entity.sprite.frame = S_EXPLO1;
	pop	de
	push	de
	ld	hl, #0x0008
	add	hl, de
	ld	(hl), #0x16
;src/splitter.c:124: splitter->destroy_timer = 0;
	pop	de
	push	de
	ld	hl, #0x0011
	add	hl, de
	ld	e, l
	ld	d, h
	xor	a, a
	ld	(de), a
;src/splitter.c:126: bullet_destroy(bullet);
	push	bc
	call	_bullet_destroy
	add	sp, #2
00107$:
;src/splitter.c:128: }
	add	sp, #12
	ret
;src/splitter.c:130: void splitter_nextframe(splitter_t *splitter) {
;	---------------------------------
; Function splitter_nextframe
; ---------------------------------
_splitter_nextframe::
	add	sp, #-3
;src/splitter.c:131: if (global_frame % 15 != 0) return;
	ld	hl, #_global_frame
	ld	c, (hl)
	ld	b, #0x00
	ld	hl, #0x000f
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	ld	a, d
	or	a, e
	jr	NZ, 00106$
;src/splitter.c:132: if (splitter->entity.sprite.frame < splitter->entity.sprite.last_frame)
	ldhl	sp,#5
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	push	de
	ld	hl, #0x0002
	add	hl, de
	pop	de
	inc	sp
	inc	sp
	push	hl
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	ldhl	sp,	#2
	ld	(hl), a
	inc	de
	inc	de
	inc	de
	ld	a, (de)
	ld	e, a
;src/splitter.c:133: splitter->entity.sprite.frame++;
	ld	a,(hl)
	cp	a,e
	jr	NC, 00104$
	inc	a
	ld	(bc), a
	jr	00106$
00104$:
;src/splitter.c:135: splitter->entity.sprite.frame = splitter->entity.sprite.first_frame;
	pop	de
	push	de
	ld	a, (de)
	ld	(bc), a
00106$:
;src/splitter.c:136: }
	add	sp, #3
	ret
;src/splitter.c:138: void splitter_kill(splitter_t *splitter) {
;	---------------------------------
; Function splitter_kill
; ---------------------------------
_splitter_kill::
;src/splitter.c:140: splitter->pos.x = 200;
	pop	bc
	pop	de
	push	de
	push	bc
	ld	a, #0xc8
	ld	(de), a
;src/splitter.c:141: splitter->pos.y = 200;
	ld	l, e
	ld	h, d
	inc	hl
	ld	(hl), #0xc8
;src/splitter.c:143: if (!splitter->entity.is_dead) splitter->entity.is_dead = true;
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00103$
	ld	(hl), #0x01
;src/splitter.c:144: if (!splitter->destroy_timer != 50) splitter->destroy_timer = 50;
00103$:
	ld	hl, #0x0011
	add	hl, de
	ld	(hl), #0x32
;src/splitter.c:145: splitter_draw(splitter);
	push	de
	call	_splitter_draw
	add	sp, #2
;src/splitter.c:146: }
	ret
;src/splitter.c:148: void splitter_draw(splitter_t *splitter) {
;	---------------------------------
; Function splitter_draw
; ---------------------------------
_splitter_draw::
	dec	sp
;src/splitter.c:149: set_sprite_tile(S_SPLITTER, splitter->entity.sprite.frame);
	ldhl	sp,#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	bc, #_shadow_OAM+42
	ldhl	sp,	#0
	ld	a, (hl)
	ld	(bc), a
;src/splitter.c:150: move_sprite(S_SPLITTER, splitter->pos.x, splitter->pos.y);
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #(_shadow_OAM + 0x0028)
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/splitter.c:150: move_sprite(S_SPLITTER, splitter->pos.x, splitter->pos.y);
;src/splitter.c:151: }
	inc	sp
	ret
	.area _CODE
	.area _CABS (ABS)
