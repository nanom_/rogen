#include <gb/gb.h>
#include <rand.h>
#include "game.h"
#include "math.h"
#include "coin.h"
#include "bullet.h"
#include "splitter.h"

UINT8 randfactor, randnum;
void splitter_spawn(splitter_t *splitter, coin_t *coin) {
    splitter->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? coin->pos.x + 8 : coin->pos.x - 8);
    splitter->pos.y = coin->pos.y;
    splitter->entity.is_dead = false;
    splitter->destroy_timer = 0;

    randfactor = (global_frame % 2 != 0 ? 1 : 2);
    seed = DIV_REG;
    seed |= (UINT16)DIV_REG << 8;
    initarand(seed + rand_menukey + (rand_menuframe * randfactor) * (global_frame * randfactor) + splitter->pos.y);

    randnum = abs(arand());

    // random direction (the worst way to do this since Game Boy's RNGs are quite bad)
    if (randnum <= 31)
        splitter->direction = d_left;
    else if (randnum <= 62)
        splitter->direction = d_right;
    else if (randnum <= 93)
        splitter->direction = d_down;
    else if (randnum <= 127)
        splitter->direction = d_up;    
}

void splitter_update(splitter_t *splitter, coin_t *coin, bullet_t *bullet) {
    if (splitter->entity.is_dead && splitter->destroy_timer >= 50) return;
    if (splitter->entity.is_dead && splitter->destroy_timer < 50) {
        splitter->destroy_timer++;
        if (splitter->destroy_timer % 10 != 0)
            splitter->entity.sprite.frame = (splitter->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);

        if (splitter->destroy_timer == 50)
            splitter_kill(splitter);

        splitter_draw(splitter);
        return;
    }
    if (!splitter->can_move) return;
    splitter_nextframe(splitter);

    if (splitter->pos.x > SCREENWIDTH - 16)
        splitter->direction = d_left;
    if (splitter->pos.x < 16)
        splitter->direction = d_right;
    if (splitter->pos.y > SCREENHEIGHT - 16)
        splitter->direction = d_up;
    if (splitter->pos.y < 32)
        splitter->direction = d_down;

    if (abs(splitter->pos.x - coin->pos.x) > 64 || abs(splitter->pos.y - coin->pos.y) > 64) {
        switch(splitter->direction) {
            case d_left:
                splitter->direction = d_right;
                break;
            case d_right:
                splitter->direction = d_left;
                break;
            case d_down:
                splitter->direction = d_up;
                break;
            case d_up:
                splitter->direction = d_down;
                break;
        }
    } else if (global_frame % 10 == 0) {
        initarand(splitter->pos.x * splitter->pos.y - global_frame);
        randnum = abs(arand());
        // some sort of pseudo random movement
        switch (splitter->direction) {
            case d_up:
                if (randnum <= 10) splitter->direction = d_right;
                break;
            case d_down:
                if (randnum <= 55) splitter->direction = d_right;
                if (randnum <= 20) splitter->direction = d_left;
                break;
            case d_left:
                if (randnum <= 65) splitter->direction = d_down;
                break;
            case d_right:
                if (randnum <= 65) splitter->direction = d_up;
                break;
        }
    }

    switch(splitter->direction) {
        case d_left:
            splitter->pos.x -= splitter->entity.spd;
            splitter->entity.facing = fa_left;
            splitter->entity.is_moving = true;
            break;
        case d_right:
            splitter->pos.x += splitter->entity.spd;
            splitter->entity.facing = fa_right;
            splitter->entity.is_moving = true;
            break;
        case d_down:
            splitter->pos.y += splitter->entity.spd;
            splitter->entity.is_moving = true;
            break;
        case d_up:
            splitter->pos.y -= splitter->entity.spd;
            splitter->entity.is_moving = true;
            break;
    }

    splitter_check_collisions(splitter, bullet);
    splitter_draw(splitter);
}

void splitter_check_collisions(splitter_t *splitter, bullet_t *bullet) {
    if (!bullet->disabled && splitter->pos.x >= bullet->pos.x - 1 && splitter->pos.y >= bullet->pos.y - 1 && splitter->pos.x <= bullet->pos.x + 10 && splitter->pos.y <= bullet->pos.y + 10) {
        splitter->entity.is_dead = true;
        splitter->entity.sprite.frame = S_EXPLO1;
        splitter->destroy_timer = 0;

        bullet_destroy(bullet);
    }
}

void splitter_nextframe(splitter_t *splitter) {
    if (global_frame % 15 != 0) return;
    if (splitter->entity.sprite.frame < splitter->entity.sprite.last_frame)
        splitter->entity.sprite.frame++;
    else
        splitter->entity.sprite.frame = splitter->entity.sprite.first_frame;
}

void splitter_kill(splitter_t *splitter) {
    // we just move the enemy out of the view
    splitter->pos.x = 200;
    splitter->pos.y = 200;
    // and we turn off the entity's updating function by setting the "is_dead" property to "true" and the "destroy_timer" property to 50
    if (!splitter->entity.is_dead) splitter->entity.is_dead = true;
    if (!splitter->destroy_timer != 50) splitter->destroy_timer = 50;
    splitter_draw(splitter);
}

void splitter_draw(splitter_t *splitter) {
    set_sprite_tile(S_SPLITTER, splitter->entity.sprite.frame);
    move_sprite(S_SPLITTER, splitter->pos.x, splitter->pos.y);
}
