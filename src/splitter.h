#include <gb/gb.h>
#include "main.h"
#include "coin.h"
#include "bullet.h"
#ifndef _SPLITTER_H_
#define _SPLITTER_H_

typedef struct splitter_t {
    POINT pos;
    entity_t entity;
    direction_t direction;
    BOOL can_move;
    UINT8 destroy_timer;
} splitter_t;

void splitter_spawn(splitter_t *splitter, coin_t *coin);
void splitter_update(splitter_t *splitter, coin_t *coin, bullet_t *bullet);
void splitter_check_collisions(splitter_t *splitter, bullet_t *bullet);
void splitter_nextframe(splitter_t *splitter);
void splitter_kill(splitter_t *splitter);
void splitter_draw(splitter_t *splitter);

#endif
