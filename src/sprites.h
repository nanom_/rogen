/*
    Sprite data generated using the GameBoy Tile Designer
    (see assets/ to get the editable files.)
*/
#ifndef _SPRITES_H_
#define _SPRITES_H_

#define spritesBank 0
extern unsigned char sprites[];

#endif
