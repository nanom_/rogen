/*
    Tile data generated using the GameBoy Tile Designer
    (see assets/ to get the editable files.)
*/
#ifndef _TILES_H_
#define _TILES_H_

#define tilesBank 0
extern unsigned char tiles[];

#endif
