;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module title
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _printf
	.globl _gotoxy
	.globl _font_set
	.globl _font_load
	.globl _font_init
	.globl _set_bkg_tiles
	.globl _set_bkg_data
	.globl _joypad
	.globl _delay
	.globl _show_start_text
	.globl _rand_menukey
	.globl _rand_menuframe
	.globl _seed
	.globl _menu_frame
	.globl _font_info
	.globl _show_title
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
_font_info::
	.ds 2
_menu_frame::
	.ds 1
_seed::
	.ds 1
_rand_menuframe::
	.ds 1
_rand_menukey::
	.ds 1
_show_start_text::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;src/title.c:13: UINT8 menu_frame=0, seed, rand_menuframe, rand_menukey;
	ld	hl, #_menu_frame
	ld	(hl), #0x00
;src/title.c:14: BOOL show_start_text = false;
	ld	hl, #_show_start_text
	ld	(hl), #0x00
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/title.c:15: void show_title() {
;	---------------------------------
; Function show_title
; ---------------------------------
_show_title::
;src/title.c:16: font_init();
	call	_font_init
;src/title.c:17: font_info = font_load(font_min);
	ld	hl, #_font_min
	push	hl
	call	_font_load
	add	sp, #2
	ld	hl, #_font_info
	ld	a, e
	ld	(hl+), a
	ld	(hl), d
;src/title.c:20: set_bkg_data(44, 70, tiles);
	ld	hl, #_tiles
	push	hl
	ld	de, #0x462c
	push	de
	call	_set_bkg_data
	add	sp, #4
;src/title.c:21: set_bkg_tiles(0, 1, 20, 17, logo);
	ld	hl, #_logo
	push	hl
	ld	de, #0x1114
	push	de
	ld	a, #0x01
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_set_bkg_tiles
	add	sp, #6
;src/title.c:23: font_set(font_info);
	ld	hl, #_font_info
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_font_set
	add	sp, #2
;src/title.c:24: while(1) {
00109$:
;src/title.c:26: if (menu_frame < 59)
	ld	hl, #_menu_frame
	ld	a, (hl)
	sub	a, #0x3b
	jr	NC, 00102$
;src/title.c:27: menu_frame++;
	inc	(hl)
	jr	00103$
00102$:
;src/title.c:29: menu_frame=0;
	ld	hl, #_menu_frame
	ld	(hl), #0x00
00103$:
;src/title.c:31: gotoxy(0,0);
	xor	a, a
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_gotoxy
	add	sp, #2
;src/title.c:32: if (menu_frame % 15 == 0) {
	ld	hl, #_menu_frame
	ld	c, (hl)
	ld	b, #0x00
	ld	hl, #0x000f
	push	hl
	push	bc
	call	__modsint
	add	sp, #4
	ld	a, d
	or	a, e
	jr	NZ, 00105$
;src/title.c:34: printf(show_start_text ? "                 V%s\n\n\n\n\n\n\n\n\n\n     PRESS START    " : "                 V%s\n\n\n\n\n\n\n\n\n\n                    ", VERSION);
	ld	bc, #___str_2
	ld	a, (#_show_start_text)
	or	a, a
	jr	Z, 00113$
	ld	de, #___str_0
	jr	00114$
00113$:
	ld	de, #___str_1
00114$:
	push	bc
	push	de
	call	_printf
	add	sp, #4
;src/title.c:35: show_start_text = !show_start_text;
	ld	hl, #_show_start_text
	ld	a, (hl)
	sub	a,#0x01
	ld	a, #0x00
	rla
	ld	(hl), a
00105$:
;src/title.c:38: if (joypad()) {
	call	_joypad
	ld	a, e
	or	a, a
	jr	Z, 00107$
;src/title.c:40: rand_menukey = joypad();
	call	_joypad
	ld	hl, #_rand_menukey
	ld	(hl), e
;src/title.c:41: rand_menuframe = menu_frame;
	ld	a, (#_menu_frame)
	ld	(#_rand_menuframe),a
;src/title.c:42: break;
	ret
00107$:
;src/title.c:45: delay(1000 / fps);
	ld	hl, #_fps
	ld	c, (hl)
	ld	b, #0x00
	push	bc
	ld	hl, #0x03e8
	push	hl
	call	__divsint
	add	sp, #4
	push	de
	call	_delay
	add	sp, #2
;src/title.c:47: }
	jr	00109$
_fps:
	.db #0x3c	; 60
___str_0:
	.ascii "                 V%s"
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.ascii "     PRESS START    "
	.db 0x00
___str_1:
	.ascii "                 V%s"
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.ascii "                    "
	.db 0x00
___str_2:
	.ascii "10"
	.db 0x00
	.area _CODE
	.area _CABS (ABS)
