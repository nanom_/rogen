#include <gb/gb.h>
#include <gb/drawing.h>
#include <gb/font.h>
#include <gb/console.h>
#include <stdio.h>
#include "main.h"
#include "game.h"
#include "logo.h"
#include "tiles.h"
#include "title.h"

font_t font_info;
UINT8 menu_frame=0, seed, rand_menuframe, rand_menukey;
BOOL show_start_text = false;
void show_title() {
    font_init();
    font_info = font_load(font_min);

    // draw the logo
    set_bkg_data(44, 70, tiles);
    set_bkg_tiles(0, 1, 20, 17, logo);

    font_set(font_info);
    while(1) {
        // the title screen has its own frame counter
        if (menu_frame < 59)
            menu_frame++;
        else
            menu_frame=0;

        gotoxy(0,0);
        if (menu_frame % 15 == 0) {
            //another dirty trick :c
            printf(show_start_text ? "                 V%s\n\n\n\n\n\n\n\n\n\n     PRESS START    " : "                 V%s\n\n\n\n\n\n\n\n\n\n                    ", VERSION);
            show_start_text = !show_start_text;
        }
    
        if (joypad()) {
            //entropy generators
            rand_menukey = joypad();
            rand_menuframe = menu_frame;
            break;
        }

        delay(1000 / fps);
    }
}
