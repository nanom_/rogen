;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module turret
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _bullet_destroy
	.globl _bullet_spawn
	.globl _turret_spawn
	.globl _turret_update
	.globl _turret_shoot
	.globl _turret_check_collisions
	.globl _turret_nextframe
	.globl _turret_kill
	.globl _turret_draw
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_paused:
	.ds 1
_last_enemy:
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/game.h:7: static BOOL paused = false;
	ld	hl, #_paused
	ld	(hl), #0x00
;src/game.h:11: static enemy_t last_enemy = E_HEATBALL;
	ld	hl, #_last_enemy
	ld	(hl), #0x06
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/turret.c:8: void turret_spawn(turret_t *turret, coin_t *coin) {
;	---------------------------------
; Function turret_spawn
; ---------------------------------
_turret_spawn::
	add	sp, #-4
;src/turret.c:9: turret->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (SCREENWIDTH - 8));
	ldhl	sp,#6
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ldhl	sp,	#2
	ld	a, c
	ld	(hl+), a
	ld	(hl), b
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00103$
	ld	de, #0x0010
	jr	00104$
00103$:
	ld	de, #0xff98
00104$:
	ld	a, e
	ldhl	sp,	#2
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/turret.c:10: turret->pos.y = coin->pos.y;
	ld	e, c
	ld	d, b
	inc	de
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ld	(de), a
;src/turret.c:11: turret->entity.facing = (coin->pos.x < (SCREENWIDTH / 2) ? fa_right : fa_left);
	ld	hl, #0x0009
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#2
	ld	(hl+), a
	ld	(hl), d
	pop	de
	push	de
	ld	a, (de)
	sub	a, #0x50
	jr	NC, 00105$
	ld	de, #0x0001
	jr	00106$
00105$:
	ld	de, #0x0000
00106$:
	ld	a, e
	ldhl	sp,	#2
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/turret.c:12: turret->entity.is_dead = false;
	ld	hl, #0x000e
	add	hl, bc
	ld	(hl), #0x00
;src/turret.c:13: turret->destroy_timer = 0;    
	ld	hl, #0x0014
	add	hl, bc
	ld	(hl), #0x00
;src/turret.c:14: }
	add	sp, #4
	ret
_fps:
	.db #0x3c	; 60
;src/turret.c:16: void turret_update(turret_t *turret, bullet_t *bullet, POINT *player_pos, bullet_t *enemy_bullet) {
;	---------------------------------
; Function turret_update
; ---------------------------------
_turret_update::
	add	sp, #-7
;src/turret.c:17: if (turret->entity.is_dead && turret->destroy_timer >= 50) return;
	ldhl	sp,#9
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x000e
	add	hl, bc
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	ld	hl, #0x0014
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#5
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	dec	hl
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jp	NC,00124$
00102$:
;src/turret.c:18: if (turret->entity.is_dead && turret->destroy_timer < 50) {
	ldhl	sp,	#4
	ld	a, (hl)
	or	a, a
	jr	Z, 00109$
	inc	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	cp	a, #0x32
	jr	NC, 00109$
;src/turret.c:19: turret->destroy_timer++;
	inc	a
	dec	hl
	push	af
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	pop	af
	ld	(hl), a
;src/turret.c:20: if (turret->destroy_timer % 10 != 0)
	ld	e, a
	ld	d, #0x00
	push	bc
	ld	hl, #0x000a
	push	hl
	push	de
	call	__modsint
	add	sp, #4
	pop	bc
	ld	a, d
	or	a, e
	jr	Z, 00105$
;src/turret.c:21: turret->entity.sprite.frame = (turret->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);
	ld	hl, #0x0008
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x16
	jr	NZ, 00126$
	ld	de, #0x0017
	jr	00127$
00126$:
	ld	de, #0x0016
00127$:
	ld	(hl), e
00105$:
;src/turret.c:23: if (turret->destroy_timer == 50) turret_kill(turret);
	ldhl	sp,#5
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	sub	a, #0x32
	jr	NZ, 00107$
	push	bc
	push	bc
	call	_turret_kill
	add	sp, #2
	pop	bc
00107$:
;src/turret.c:25: turret_draw(turret);
	push	bc
	call	_turret_draw
	add	sp, #2
;src/turret.c:26: return;
	jp	00124$
00109$:
;src/turret.c:28: if (!turret->can_move) return;
	ld	hl, #0x0010
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jp	Z,00124$
;src/turret.c:29: turret_nextframe(turret);
	push	bc
	push	bc
	call	_turret_nextframe
	add	sp, #2
	pop	bc
;src/turret.c:31: turret->shoot_timer++;
	ld	hl, #0x0012
	add	hl, bc
	ld	e, l
	ld	d, h
	ld	a, (de)
	inc	a
	ldhl	sp,	#5
	ld	(hl), a
	ld	(de), a
;src/turret.c:32: if (turret->shoot_timer == turret->shoot_timer_max && turret->entity.can_shoot) {
	ld	hl, #0x0013
	add	hl, bc
	ld	a, (hl)
	ldhl	sp,	#6
	ld	(hl), a
	ldhl	sp,	#5
	ld	a, (hl+)
	sub	a, (hl)
	jr	NZ, 00116$
	ld	hl, #0x000d
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	Z, 00116$
;src/turret.c:33: turret->shoot_timer = 0;
	xor	a, a
	ld	(de), a
;src/turret.c:34: if (enemy_bullet->disabled) turret_shoot(turret, enemy_bullet);
	ldhl	sp,#15
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0005
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00116$
	push	bc
	push	de
	push	bc
	call	_turret_shoot
	add	sp, #4
	pop	bc
00116$:
;src/turret.c:37: if (turret->pos.y < player_pos->y) {
	ld	hl, #0x0001
	add	hl, bc
	inc	sp
	inc	sp
;src/turret.c:41: } else if (turret->pos.y > player_pos->y) {
	ld	e, l
	ld	d, h
	push	de
	ld	a, (de)
	ldhl	sp,	#2
	ld	(hl), a
;src/turret.c:37: if (turret->pos.y < player_pos->y) {
	ldhl	sp,#13
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	inc	de
	ld	a, (de)
	ld	e, a
;src/turret.c:42: turret->direction = d_up;
	ld	hl, #0x000f
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#3
	ld	(hl+), a
	ld	(hl), d
;src/turret.c:43: turret->entity.is_moving = true;
	ld	hl, #0x000b
	add	hl, bc
	ld	a, l
	ld	d, h
	ldhl	sp,	#5
	ld	(hl+), a
	ld	(hl), d
;src/turret.c:40: turret->pos.y += turret->entity.spd;
	ld	hl, #0x000a
	add	hl, bc
	ld	c, l
	ld	b, h
;src/turret.c:37: if (turret->pos.y < player_pos->y) {
	ldhl	sp,	#2
	ld	a, (hl)
	sub	a, e
	jr	NC, 00122$
;src/turret.c:38: turret->direction = d_down;
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x02
;src/turret.c:39: turret->entity.is_moving = true;
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/turret.c:40: turret->pos.y += turret->entity.spd;
	pop	de
	push	de
	ld	a, (de)
	push	af
	ld	a, (bc)
	ld	c, a
	pop	af
	add	a, c
	ld	c, a
	pop	hl
	push	hl
	ld	(hl), c
	jr	00123$
00122$:
;src/turret.c:41: } else if (turret->pos.y > player_pos->y) {
	ld	a, e
	ldhl	sp,	#2
	sub	a, (hl)
	jr	NC, 00119$
;src/turret.c:42: turret->direction = d_up;
	inc	hl
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x03
;src/turret.c:43: turret->entity.is_moving = true;
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x01
;src/turret.c:44: turret->pos.y -= turret->entity.spd;
	pop	de
	push	de
	ld	a, (de)
	push	af
	ld	a, (bc)
	ld	c, a
	pop	af
	sub	a, c
	ld	c, a
	pop	hl
	push	hl
	ld	(hl), c
	jr	00123$
00119$:
;src/turret.c:46: turret->entity.is_moving = false;
	ldhl	sp,	#5
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x00
;src/turret.c:47: turret->direction = d_nodir;
	ldhl	sp,	#3
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), #0x04
00123$:
;src/turret.c:50: turret_check_collisions(turret, bullet);
	ldhl	sp,	#11
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	ldhl	sp,	#11
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_turret_check_collisions
	add	sp, #4
;src/turret.c:51: turret_draw(turret);
	ldhl	sp,	#9
	ld	a, (hl+)
	ld	h, (hl)
	ld	l, a
	push	hl
	call	_turret_draw
	add	sp, #2
00124$:
;src/turret.c:52: }
	add	sp, #7
	ret
;src/turret.c:54: void turret_shoot(turret_t *turret, bullet_t *enemy_bullet) {
;	---------------------------------
; Function turret_shoot
; ---------------------------------
_turret_shoot::
	add	sp, #-2
;src/turret.c:55: enemy_bullet->pos.x = turret->pos.x;
	ldhl	sp,	#6
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	ldhl	sp,#4
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	a, (bc)
	pop	hl
	push	hl
	ld	(hl), a
;src/turret.c:56: enemy_bullet->pos.y = turret->pos.y;
	pop	de
	push	de
	inc	de
	ld	l, c
	ld	h, b
	inc	hl
	ld	a, (hl)
	ld	(de), a
;src/turret.c:57: enemy_bullet->direction = turret->entity.facing;
	pop	de
	push	de
	ld	hl, #0x0004
	add	hl, de
	ld	e, l
	ld	d, h
	ld	hl, #0x0009
	add	hl, bc
	ld	a, (hl)
	ld	(de), a
;src/turret.c:58: enemy_bullet->sprite = S_TURRETBEAM;
	pop	hl
	push	hl
	inc	hl
	inc	hl
	ld	(hl), #0x1b
;src/turret.c:59: enemy_bullet->spd = (turret->is_turret2 ? 4 : 2);
	pop	de
	push	de
	inc	de
	inc	de
	inc	de
	ld	hl, #0x0011
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	Z, 00103$
	ld	bc, #0x0004
	jr	00104$
00103$:
	ld	bc, #0x0002
00104$:
	ld	a, c
	ld	(de), a
;src/turret.c:60: enemy_bullet->disabled = false;
	pop	de
	push	de
	ld	hl, #0x0005
	add	hl, de
	ld	c, l
	ld	b, h
	xor	a, a
	ld	(bc), a
;src/turret.c:62: bullet_spawn(enemy_bullet);
	pop	hl
	push	hl
	push	hl
	call	_bullet_spawn
	add	sp, #2
;src/turret.c:63: }
	add	sp, #2
	ret
;src/turret.c:65: void turret_check_collisions(turret_t *turret, bullet_t *bullet) {
;	---------------------------------
; Function turret_check_collisions
; ---------------------------------
_turret_check_collisions::
	add	sp, #-12
;src/turret.c:66: if (!bullet->disabled && turret->pos.x >= bullet->pos.x - 1 && turret->pos.y >= bullet->pos.y - 1 && turret->pos.x <= bullet->pos.x + 10 && turret->pos.y <= bullet->pos.y + 10) {
	ldhl	sp,#16
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0005
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jp	NZ, 00107$
	ldhl	sp,	#14
	ld	a, (hl+)
	ld	e, (hl)
	ldhl	sp,	#0
	ld	(hl+), a
	ld	(hl), e
	pop	de
	push	de
	ld	a, (de)
	ldhl	sp,	#9
	ld	(hl), a
	ld	a, (bc)
	ldhl	sp,	#2
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#11
	ld	(hl-), a
	ld	a, e
	ld	(hl-), a
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#4
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00134$
	bit	7, d
	jr	NZ, 00135$
	cp	a, a
	jr	00135$
00134$:
	bit	7, d
	jr	Z, 00135$
	scf
00135$:
	jp	C, 00107$
	pop	hl
	push	hl
	inc	hl
	ld	a, (hl)
	ldhl	sp,	#11
	ld	(hl), a
	ld	e, c
	ld	d, b
	inc	de
	ld	a, (de)
	ldhl	sp,	#6
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0001
	ld	a, e
	sub	a, l
	ld	e, a
	ld	a, d
	sbc	a, h
	ldhl	sp,	#9
	ld	(hl-), a
	ld	(hl), e
	ldhl	sp,	#11
	ld	a, (hl-)
	ld	(hl), a
	xor	a, a
	inc	hl
	ld	(hl), a
	ldhl	sp,	#10
	ld	e, l
	ld	d, h
	ldhl	sp,	#8
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00136$
	bit	7, d
	jr	NZ, 00137$
	cp	a, a
	jr	00137$
00136$:
	bit	7, d
	jr	Z, 00137$
	scf
00137$:
	jp	C, 00107$
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#4
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00138$
	bit	7, d
	jr	NZ, 00139$
	cp	a, a
	jr	00139$
00138$:
	bit	7, d
	jr	Z, 00139$
	scf
00139$:
	jr	C, 00107$
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	a, l
	ld	d, h
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), d
	ldhl	sp,	#8
	ld	e, l
	ld	d, h
	ldhl	sp,	#10
	ld	a, (de)
	sub	a, (hl)
	inc	hl
	inc	de
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00140$
	bit	7, d
	jr	NZ, 00141$
	cp	a, a
	jr	00141$
00140$:
	bit	7, d
	jr	Z, 00141$
	scf
00141$:
	jr	C, 00107$
;src/turret.c:67: turret->entity.is_dead = true;
	pop	de
	push	de
	ld	hl, #0x000e
	add	hl, de
	ld	(hl), #0x01
;src/turret.c:68: turret->entity.sprite.frame = S_EXPLO1;
	pop	de
	push	de
	ld	hl, #0x0008
	add	hl, de
	ld	(hl), #0x16
;src/turret.c:69: turret->destroy_timer = 0;
	pop	de
	push	de
	ld	hl, #0x0014
	add	hl, de
	ld	e, l
	ld	d, h
	xor	a, a
	ld	(de), a
;src/turret.c:71: bullet_destroy(bullet);
	push	bc
	call	_bullet_destroy
	add	sp, #2
00107$:
;src/turret.c:73: }
	add	sp, #12
	ret
;src/turret.c:75: void turret_nextframe(turret_t *turret) {
;	---------------------------------
; Function turret_nextframe
; ---------------------------------
_turret_nextframe::
;src/turret.c:76: turret->entity.sprite.frame = (turret->entity.facing == fa_left ? turret->entity.sprite.left_anim_start : turret->entity.sprite.right_anim_start);
	pop	bc
	pop	de
	push	de
	push	bc
	ld	hl, #0x0008
	add	hl, de
	ld	c, l
	ld	b, h
	ld	hl, #0x0009
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00103$
	inc	de
	inc	de
	inc	de
	inc	de
	ld	a, (de)
	jr	00104$
00103$:
	ld	hl, #0x0006
	add	hl, de
	ld	a, (hl)
00104$:
	ld	(bc), a
;src/turret.c:77: }
	ret
;src/turret.c:79: void turret_kill(turret_t *turret) {
;	---------------------------------
; Function turret_kill
; ---------------------------------
_turret_kill::
;src/turret.c:81: turret->pos.x = 200;
	pop	bc
	pop	de
	push	de
	push	bc
	ld	a, #0xc8
	ld	(de), a
;src/turret.c:82: turret->pos.y = 200;
	ld	l, e
	ld	h, d
	inc	hl
	ld	(hl), #0xc8
;src/turret.c:84: if (!turret->entity.is_dead) turret->entity.is_dead = true;
	ld	hl, #0x000e
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00103$
	ld	(hl), #0x01
;src/turret.c:85: if (!turret->destroy_timer != 50) turret->destroy_timer = 50;
00103$:
	ld	hl, #0x0014
	add	hl, de
	ld	(hl), #0x32
;src/turret.c:86: turret_draw(turret);
	push	de
	call	_turret_draw
	add	sp, #2
;src/turret.c:87: }
	ret
;src/turret.c:89: void turret_draw(turret_t *turret) {
;	---------------------------------
; Function turret_draw
; ---------------------------------
_turret_draw::
	dec	sp
;src/turret.c:90: set_sprite_tile(S_TURRET, turret->entity.sprite.frame);
	ldhl	sp,#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0008
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
;/opt/gbdk/include/gb/gb.h:610: shadow_OAM[nb].tile=tile; 
	ld	bc, #_shadow_OAM+66
	ldhl	sp,	#0
	ld	a, (hl)
	ld	(bc), a
;src/turret.c:91: move_sprite(S_TURRET, turret->pos.x, turret->pos.y);
	ld	l, e
	ld	h, d
	inc	hl
	ld	b, (hl)
	ld	a, (de)
	ld	c, a
;/opt/gbdk/include/gb/gb.h:652: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #(_shadow_OAM + 0x0040)
;/opt/gbdk/include/gb/gb.h:653: itm->y=y, itm->x=x; 
	ld	a, b
	ld	(hl+), a
	ld	(hl), c
;src/turret.c:91: move_sprite(S_TURRET, turret->pos.x, turret->pos.y);
;src/turret.c:92: }
	inc	sp
	ret
	.area _CODE
	.area _CABS (ABS)
