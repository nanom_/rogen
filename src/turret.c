#include <gb/gb.h>
#include "main.h"
#include "game.h"
#include "coin.h"
#include "bullet.h"
#include "turret.h"

void turret_spawn(turret_t *turret, coin_t *coin) {
    turret->pos.x = (coin->pos.x < (SCREENWIDTH / 2) ? 16 : (SCREENWIDTH - 8));
    turret->pos.y = coin->pos.y;
    turret->entity.facing = (coin->pos.x < (SCREENWIDTH / 2) ? fa_right : fa_left);
    turret->entity.is_dead = false;
    turret->destroy_timer = 0;    
}

void turret_update(turret_t *turret, bullet_t *bullet, POINT *player_pos, bullet_t *enemy_bullet) {
    if (turret->entity.is_dead && turret->destroy_timer >= 50) return;
    if (turret->entity.is_dead && turret->destroy_timer < 50) {
        turret->destroy_timer++;
        if (turret->destroy_timer % 10 != 0)
            turret->entity.sprite.frame = (turret->entity.sprite.frame == S_EXPLO1 ? S_EXPLO2 : S_EXPLO1);

        if (turret->destroy_timer == 50) turret_kill(turret);

        turret_draw(turret);
        return;
    }
    if (!turret->can_move) return;
    turret_nextframe(turret);

    turret->shoot_timer++;
    if (turret->shoot_timer == turret->shoot_timer_max && turret->entity.can_shoot) {
        turret->shoot_timer = 0;
        if (enemy_bullet->disabled) turret_shoot(turret, enemy_bullet);
    }

    if (turret->pos.y < player_pos->y) {
        turret->direction = d_down;
        turret->entity.is_moving = true;
        turret->pos.y += turret->entity.spd;
    } else if (turret->pos.y > player_pos->y) {
        turret->direction = d_up;
        turret->entity.is_moving = true;
        turret->pos.y -= turret->entity.spd;
    } else {
        turret->entity.is_moving = false;
        turret->direction = d_nodir;
    }

    turret_check_collisions(turret, bullet);
    turret_draw(turret);
}

void turret_shoot(turret_t *turret, bullet_t *enemy_bullet) {
    enemy_bullet->pos.x = turret->pos.x;
    enemy_bullet->pos.y = turret->pos.y;
    enemy_bullet->direction = turret->entity.facing;
    enemy_bullet->sprite = S_TURRETBEAM;
    enemy_bullet->spd = (turret->is_turret2 ? 4 : 2);
    enemy_bullet->disabled = false;

    bullet_spawn(enemy_bullet);
}

void turret_check_collisions(turret_t *turret, bullet_t *bullet) {
    if (!bullet->disabled && turret->pos.x >= bullet->pos.x - 1 && turret->pos.y >= bullet->pos.y - 1 && turret->pos.x <= bullet->pos.x + 10 && turret->pos.y <= bullet->pos.y + 10) {
        turret->entity.is_dead = true;
        turret->entity.sprite.frame = S_EXPLO1;
        turret->destroy_timer = 0;

        bullet_destroy(bullet);
    }
}

void turret_nextframe(turret_t *turret) {
    turret->entity.sprite.frame = (turret->entity.facing == fa_left ? turret->entity.sprite.left_anim_start : turret->entity.sprite.right_anim_start);
}

void turret_kill(turret_t *turret) {
    // we just move the enemy out of the view
    turret->pos.x = 200;
    turret->pos.y = 200;
    // and we turn off the entity's updating function by setting the "is_dead" property to "true" and the "destroy_timer" property to 50
    if (!turret->entity.is_dead) turret->entity.is_dead = true;
    if (!turret->destroy_timer != 50) turret->destroy_timer = 50;
    turret_draw(turret);
}

void turret_draw(turret_t *turret) {
    set_sprite_tile(S_TURRET, turret->entity.sprite.frame);
    move_sprite(S_TURRET, turret->pos.x, turret->pos.y);
}
