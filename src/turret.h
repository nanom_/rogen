#include <gb/gb.h>
#include "main.h"
#include "coin.h"
#include "bullet.h"
#ifndef _TURRET_H_
#define _TURRET_H_

typedef struct turret_t {
    POINT pos;
    entity_t entity;
    direction_t direction;
    BOOL can_move, is_turret2;
    UINT8 shoot_timer, shoot_timer_max, destroy_timer;
} turret_t;

void turret_spawn(turret_t *turret, coin_t *coin);
void turret_update(turret_t *turret, bullet_t *bullet, POINT *player_pos, bullet_t *enemy_bullet);
void turret_shoot(turret_t *turret, bullet_t *enemy_bullet);
void turret_check_collisions(turret_t *turret, bullet_t *bullet);
void turret_nextframe(turret_t *turret);
void turret_kill(turret_t *turret);
void turret_draw(turret_t *turret);

#endif
