;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #11875 (Linux)
;--------------------------------------------------------
	.module warning
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _puts
	.globl _printf
	.globl _color
	.globl _gotoxy
	.globl _font_load
	.globl _font_init
	.globl _delay
	.globl _fadein
	.globl _f
	.globl _font_warning
	.globl _show_warning
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_font_warning::
	.ds 1
_f::
	.ds 1
_fadein::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/warning.c:10: BOOL fadein = false;
	ld	hl, #_fadein
	ld	(hl), #0x00
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/warning.c:11: void show_warning() {
;	---------------------------------
; Function show_warning
; ---------------------------------
_show_warning::
;src/warning.c:12: font_init();
	call	_font_init
;src/warning.c:13: color(WHITE, BLACK, SOLID);
	xor	a, a
	ld	d,a
	ld	e,#0x03
	push	de
	xor	a, a
	push	af
	inc	sp
	call	_color
	add	sp, #3
;src/warning.c:14: font_warning = font_load(font_min);
	ld	hl, #_font_min
	push	hl
	call	_font_load
	add	sp, #2
	ld	hl, #_font_warning
	ld	(hl), e
;src/warning.c:16: BGP_REG = 0xFF;
	ld	a, #0xff
	ldh	(_BGP_REG+0),a
;src/warning.c:17: for(f=0; f < 3; f++) {
	ld	hl, #_f
	ld	(hl), #0x00
00112$:
;src/warning.c:18: switch(BGP_REG) {
	ldh	a, (_BGP_REG+0)
	cp	a, #0xf9
	jr	Z, 00101$
	cp	a, #0xfe
	jr	Z, 00102$
	inc	a
	jr	Z, 00103$
	jr	00104$
;src/warning.c:19: case 0xF9:
00101$:
;src/warning.c:20: BGP_REG = 0xE4;
	ld	a, #0xe4
	ldh	(_BGP_REG+0),a
;src/warning.c:21: break;
	jr	00104$
;src/warning.c:22: case 0xFE:
00102$:
;src/warning.c:23: BGP_REG = 0xF9;
	ld	a, #0xf9
	ldh	(_BGP_REG+0),a
;src/warning.c:24: break;
	jr	00104$
;src/warning.c:25: case 0xFF:
00103$:
;src/warning.c:26: BGP_REG = 0xFE;
	ld	a, #0xfe
	ldh	(_BGP_REG+0),a
;src/warning.c:28: }
00104$:
;src/warning.c:29: delay(25);
	ld	hl, #0x0019
	push	hl
	call	_delay
	add	sp, #2
;src/warning.c:17: for(f=0; f < 3; f++) {
	ld	hl, #_f
	inc	(hl)
	ld	a, (hl)
	sub	a, #0x03
	jr	C, 00112$
;src/warning.c:32: gotoxy(0, 0);
	xor	a, a
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_gotoxy
	add	sp, #2
;src/warning.c:34: printf("\n\n\n\n\n\n       WARNING      \n");
	ld	hl, #___str_1
	push	hl
	call	_puts
	add	sp, #2
;src/warning.c:35: printf("  Contains flashing         images      ");
	ld	hl, #___str_2
	push	hl
	call	_printf
	add	sp, #2
;src/warning.c:36: delay(2100);
	ld	hl, #0x0834
	push	hl
	call	_delay
	add	sp, #2
;src/warning.c:37: for(f=0; f < 6; f++) {
	ld	hl, #_f
	ld	(hl), #0x00
00114$:
;src/warning.c:38: switch(BGP_REG) {
	ldh	a, (_BGP_REG+0)
	cp	a, #0xe4
	jr	Z, 00106$
	cp	a, #0xf9
	jr	Z, 00107$
	cp	a, #0xfe
	jr	Z, 00108$
	inc	a
	jr	Z, 00109$
	jr	00110$
;src/warning.c:39: case 0xE4:
00106$:
;src/warning.c:40: BGP_REG = 0xF9;
	ld	a, #0xf9
	ldh	(_BGP_REG+0),a
;src/warning.c:41: break;
	jr	00110$
;src/warning.c:42: case 0xF9:
00107$:
;src/warning.c:43: BGP_REG = (!fadein ? 0xFE : 0xE4 );
	ld	a, (#_fadein)
	or	a, a
	jr	NZ, 00118$
	ld	bc, #0x00fe
	jr	00119$
00118$:
	ld	bc, #0x00e4
00119$:
	ld	a, c
	ldh	(_BGP_REG+0),a
;src/warning.c:44: break;
	jr	00110$
;src/warning.c:45: case 0xFE:
00108$:
;src/warning.c:46: BGP_REG = (!fadein ? 0xFF : 0xF9);
	ld	a, (#_fadein)
	or	a, a
	jr	NZ, 00120$
	ld	bc, #0x00ff
	jr	00121$
00120$:
	ld	bc, #0x00f9
00121$:
	ld	a, c
	ldh	(_BGP_REG+0),a
;src/warning.c:47: break;
	jr	00110$
;src/warning.c:48: case 0xFF:
00109$:
;src/warning.c:49: BGP_REG = 0xFE;
	ld	a, #0xfe
	ldh	(_BGP_REG+0),a
;src/warning.c:50: fadein = true;
	ld	hl, #_fadein
	ld	(hl), #0x01
;src/warning.c:52: }
00110$:
;src/warning.c:53: delay((BGP_REG != 0xFF ? 25 : 120));
	ldh	a, (_BGP_REG+0)
	inc	a
	jr	Z, 00122$
	ld	bc, #0x0019
	jr	00123$
00122$:
	ld	bc, #0x0078
00123$:
	push	bc
	call	_delay
	add	sp, #2
;src/warning.c:37: for(f=0; f < 6; f++) {
	ld	hl, #_f
	inc	(hl)
	ld	a, (hl)
	sub	a, #0x06
	jr	C, 00114$
;src/warning.c:55: }
	ret
___str_1:
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.db 0x0a
	.ascii "       WARNING      "
	.db 0x00
___str_2:
	.ascii "  Contains flashing         images      "
	.db 0x00
	.area _CODE
	.area _CABS (ABS)
