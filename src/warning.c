#include <gb/gb.h>
#include <gb/font.h>
#include <gb/console.h>
#include <gb/drawing.h>
#include <stdio.h>
#include "main.h"
#include "warning.h"

UINT8 font_warning, f;
BOOL fadein = false;
void show_warning() {
    font_init();
    color(WHITE, BLACK, SOLID);
    font_warning = font_load(font_min);

    BGP_REG = 0xFF;
    for(f=0; f < 3; f++) {
        switch(BGP_REG) {
            case 0xF9:
                BGP_REG = 0xE4;
                break;
            case 0xFE:
               BGP_REG = 0xF9;
                break;
            case 0xFF:
                BGP_REG = 0xFE;
                break;
        }
        delay(25);
    }

    gotoxy(0, 0);
    // alphanumeric characters only!
    printf("\n\n\n\n\n\n       WARNING      \n");
    printf("  Contains flashing         images      ");
    delay(2100);
    for(f=0; f < 6; f++) {
        switch(BGP_REG) {
            case 0xE4:
                BGP_REG = 0xF9;
                break;
            case 0xF9:
                BGP_REG = (!fadein ? 0xFE : 0xE4 );
                break;
            case 0xFE:
               BGP_REG = (!fadein ? 0xFF : 0xF9);
                break;
            case 0xFF:
                BGP_REG = 0xFE;
                fadein = true;
                break;
        }
        delay((BGP_REG != 0xFF ? 25 : 120));
    }
}
